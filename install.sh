#!/bin/bash

echo
echo " _______  ___      ___  __   __  _______  _______  _______ "
echo "|       ||   |    |   ||  |_|  ||       ||       ||       |"
echo "|    ___||   |    |   ||       ||    _  ||  _____||    ___|"
echo "|   | __ |   |    |   ||       ||   |_| || |_____ |   |___ "
echo "|   ||  ||   |___ |   ||       ||    ___||_____  ||    ___|"
echo "|   |_| ||       ||   || ||_|| ||   |     _____| ||   |___ "
echo "|_______||_______||___||_|   |_||___|    |_______||_______|"
echo

if test -z "$CONDA_PREFIX"
then
      echo "ERROR! No CONDA_PREFIX env variabile set! Exit"
      return
else
      echo "Env Variable CONDA_PREFIX found!"
fi

#Source Conda
source ${CONDA_PREFIX}/etc/profile.d/conda.sh

#Update conda
echo "** Checking updates for conda..."
conda update conda

# Install mamba (see https://github.com/fermi-lat/Fermitools-conda/wiki/Installation-Instructions)
conda install mamba -n base -c conda-forge -y

# Install requirements
mamba create -n fermi -c conda-forge -c fermi fermitools -y
conda install -n fermi -c conda-forge --file requirements.txt -y

mamba deactivate
echo
echo "** Fermi environment created! To activate, run:"
echo "$ mamba activate fermi"
echo
echo "** To setup pyfermigamma, run:"
echo "$ source ./setup.sh"
echo
