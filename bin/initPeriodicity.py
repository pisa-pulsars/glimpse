#!/usr/bin/env python

import os
from os.path import join, exists, dirname
import sys
import platform
import argparse
import time

from astropy.io import fits
from astropy.time import Time
import numpy as np

from pyfermigamma.utilities.fermilogger import FermiLogger
from pyfermigamma.utilities.fermidataselector import FermiDataSelector
from pyfermigamma.utilities.fermiutils import run_bary_gtbary

from glimpse.sources import FermiPulsar
from glimpse.monitor import PulsarMonitor
from glimpse.models.timing import TimingModel
from glimpse.utils import MJDRef, met_to_mjd, mjd_to_met, load_parfile
from glimpse.config import PULSAR_INDEX, EVFILE, SCFILE

# General variables
os_system = platform.system()
running_python_version = int(sys.version[0])
work_dir = os.getcwd()
script_name = os.path.split(sys.argv[0])[1].split(".")[0]
script_path = join(os.getcwd(), dirname(sys.argv[0]))

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="""Reads pulsar catalog, generates data from a full-mission FT1 file and creates an index with 
                    parameters for the analysis benchmark.""",
        epilog=""" """)
    parser.add_argument("-f", "--index-filename", type=str, default=None,
                        help="path to periodicity index filename")
    parser.add_argument("--force-rerun", action="store_true",
                        help="delete old evfile and rerun gtselect")
    parser.add_argument("--skip-preparation", action="store_true",
                        help="do not run gtselect and gtbary")
    parser.add_argument("--optimize", action="store_true",
                        help="optimize weights and roi cuts")
    parser.add_argument("--estrapolate", action="store_true",
                        help="estrapolate and scan at start epoch")
    parser.add_argument("--from-scratch", action="store_true",
                        help="remove existing index and make a new one")
    parser.add_argument("-d", "--debug", action="store_true",
                        help="debug mode")
    parser.add_argument("parfile", type=str, help="input timing parfile")
    args = parser.parse_args()

    # Start the logger
    start_time = str(time.strftime("y%ym%md%d_H%HM%MS%S", time.localtime()))
    my_logger = FermiLogger(script_name + "_" + start_time + "_logger")
    if args.debug:
        my_logger.set_loglevel("DEBUG")
    else:
        my_logger.set_loglevel("INFO")

    my_logger.info("**************************************")
    my_logger.info("    " + str(script_name))
    my_logger.info("    (Running on " + os_system + " OS)")
    my_logger.info("**************************************")

    # List parameters and args
    my_logger.info("Input options:")
    for key in vars(args):
        my_logger.info(key + ": " + str(vars(args)[key]))

    # Init index
    index_filename = args.index_filename if args.index_filename is not None \
        else PULSAR_INDEX
    index = PulsarMonitor(index_filename)

    # Init pulsar
    pars_dict = load_parfile(args.parfile)
    pulsar = FermiPulsar(pars_dict)
    psr_name = pulsar.PSRJ

    # Add pulsar if not in index
    if psr_name not in index.list_pulsars():
        index.add_pulsar(pulsar)

    # Get paths for pulsar
    ft1_filename = index.get_ft1_filename(psr_name)
    subdir = os.path.dirname(ft1_filename)

    # Clean old files if required
    if args.force_rerun:
        os.remove(ft1_filename)
        index.edit_pulsar(psr_name, tstart=MJDRef, tstop=MJDRef)

    # Prepare list of files with new data to join
    if args.skip_preparation:
        my_logger.info("Skipping data selection and barycentering...")
    else:

        ft1_list = []
        if EVFILE[-5:] == ".fits":
            if met_to_mjd(fits.getval(EVFILE, "TSTOP")) >= index.get_pulsar(psr_name)["TSTOP"]:
                ft1_list.append(EVFILE)
        elif EVFILE[-4:] == ".txt":
            with open(EVFILE, "r") as lf:
                lines = [line.strip() for line in lf.readlines()]
            my_logger.debug("Files to append:")
            for line in lines:
                tstart = met_to_mjd(fits.getval(line, "TSTART"))
                tstop = met_to_mjd(fits.getval(line, "TSTOP"))
                if tstop > index.get_pulsar(psr_name)["TSTOP"]:
                    ft1_list.append(line)
                    my_logger.debug("%s    TSTART = MJD %f    TSTOP = MJD %f" % (line, tstart, tstop))

        # If new data cover a wider time range, prepare data
        if len(ft1_list) > 0:

            # Dump list to file
            list_filename = join(subdir, "ft1_list_toappend.txt")
            with open(list_filename, "w") as lf:
                lf.write("\n".join(ft1_list))

            # Run gtselect
            my_selector = FermiDataSelector(list_filename, m_ft2_filename=SCFILE, m_source_name=psr_name)
            my_selector.set_cuts("ZENITH_ANGLE", 0.0, 90.0)
            my_selector.set_cuts("ENERGY", 100, 300000)
            my_selector.set_cuts("TIME", mjd_to_met(index.get_pulsar(psr_name)["TSTOP"]), mjd_to_met(Time.now().mjd))
            my_selector.set_roi("CEL", pulsar.RAJ, pulsar.DECJ, 10.)
            my_selector.set_event_type(3)
            my_selector.set_output_dir(subdir)
            my_selector.set_output_filename(psr_name + "_new_data.fits")
            ft1_filtered = my_selector.run(run_mktime=True)

            # Run gtbary
            ft1_barycentered = run_bary_gtbary(ft1_filtered, SCFILE, pulsar.RAJ, pulsar.DECJ)

            # Store path of new file or append data to existing file
            new_header = fits.getheader(ft1_barycentered)
            if exists(ft1_filename):
                tstart = met_to_mjd(fits.getval(ft1_filename, "TSTART"))
                tstop = met_to_mjd(fits.getval(ft1_filename, "TSTOP"))
                my_logger.debug("Previous time boundaries:")
                my_logger.debug("MJD %f - %f" % (tstart, tstop))

                new_data = fits.getdata(ft1_barycentered)
                fits.append(ft1_filename, new_data)
                fits.setval(ft1_filename, "TSTOP", value=new_header["TSTOP"])
                os.remove(ft1_barycentered)
            else:
                os.rename(ft1_barycentered, ft1_filename)

            # Remove temp fits and parfiles
            os.remove("gtselect.par")
            os.remove("gtmktime.par")
            os.remove("gtbary.par")
            os.remove(ft1_filtered)
            os.remove(list_filename)

        else:
            my_logger.info("No data to append.")

    if not exists(ft1_filename):
        my_logger.fatal("FT1 file %s not found." % ft1_filename)
    else:

        index.edit_pulsar(psr_name, ft1_filename=ft1_filename)
        tstart = met_to_mjd(fits.getval(ft1_filename, "TSTART"))
        tstop = met_to_mjd(fits.getval(ft1_filename, "TSTOP"))
        index.edit_pulsar(psr_name, TSTART=tstart, TSTOP=tstop)
        index.dump()

        if args.optimize:

            my_logger.info("Starting analysis optimization procedure...")

            my_logger.info("Loading data from %s..." % ft1_filename)
            pulsar.load_data(ft1_filename)

            # Photon selection for preliminary test
            starting_range = pulsar.PEPOCH + np.array([-1., 1.]) * 30.
            pulsar.select_by_time(*starting_range)
            my_logger.info("Selected %d photons in range MJD %d-%d"
                           % (len(pulsar.get_data()), starting_range[0],
                              starting_range[1]))

            # Periodicity search at parfile epoch (mu=3.)
            my_logger.info("Scanning for periodicity at MJD %d..." %
                           pulsar.PEPOCH)
            pars = pulsar.run_h_scan(f0_trials=50, f1_trials=50,
                                     plot_results=True, compute_errors=False,
                                     mu=3.)
            my_logger.info("Scan terminated with H = %.2f" % pars["H"])
            if pars["SIGMA"] < 5.:
                my_logger.warning(
                    "Test significance at PEPOCH lower than 5 sigma.")

            pulsar.set_parameters(
                PEPOCH=pars["PEPOCH"], F0=pars["F0_BEST"], F1=pars["F1_BEST"])
            index.edit_pulsar(psr_name, PEPOCH=pars["PEPOCH"],
                              F0=pars["F0_BEST"], F1=pars["F1_BEST"])
            pulsar.reset_cuts()

            # Best parameters for simple weights (dt=60, full roi)
            my_logger.info("Scanning for weights parameters...")
            pulsar.select_by_time(*starting_range)
            mu_best = pulsar.optimize_weights(verbose=args.debug)
            index.edit_setup(psr_name, 0,
                             weights={"method": "simple", "mu_w": mu_best})
            pulsar.reset_cuts()

            # Search for optimal time window (full roi)
            my_logger.info("Estimating optimal time window...")
            dt_optimal = pulsar.optimize_time_window(mu=mu_best,
                                                  verbose=args.debug,
                                                  sigmas=8)
            index.edit_setup(psr_name, 0, time_window=dt_optimal)
            pulsar.reset_cuts()

            # Search for optimal RoI cuts at fixed time window
            my_logger.info("Estimating optimal RoI cuts...")
            pulsar.select_by_time(tmin=pulsar.PEPOCH - 0.5 * dt_optimal,
                                  tmax=pulsar.PEPOCH + 0.5 * dt_optimal)
            roi_cuts = pulsar.optimize_roi_cuts(methods=["cookie"], mu=mu_best,
                                                verbose=args.debug)
            index.edit_setup(psr_name, 0, roi_cuts=roi_cuts)
            pulsar.reset_cuts()

            # Save data
            index.dump()

        if args.estrapolate:

            # Set expected parameters at the beginning of the data
            start_epoch = tstart + 30.
            my_logger.info("Estrapolating parameters at at MJD %d..." %
                           start_epoch)
            model, x = TimingModel().from_parfile(args.parfile)
            ph, f0, f1 = model(mjd_to_met(start_epoch), x)
            pulsar.set_parameters(PEPOCH=start_epoch, F0=f0, F1=f1)

            if pulsar.get_data() is None:
                my_logger.info("Loading data from %s..." % ft1_filename)
                pulsar.load_data(ft1_filename)

            # Data selection
            setup = index.get_setup(psr_name, 0)
            trange = start_epoch + \
                             np.array([-0.5, 0.5]) * setup["time_window"]
            pulsar.select_by_time(*trange)
            pulsar.cookie_cutter(setup["roi_cuts"]["radius"],
                                 emin=setup["roi_cuts"]["emin"],
                                 emax=setup["roi_cuts"]["emax"])
            my_logger.info("Selected %d photons" % len(pulsar.get_data()))

            mu_w = None if setup["weights"]["method"] == "none" \
                else setup["weights"]["mu_w"]

            # Periodicity search at TSTART
            my_logger.info("Scanning for periodicity at MJD %d..." %
                           pulsar.PEPOCH)
            pars = pulsar.run_h_scan(f0_trials=100, f1_trials=100,
                                     plot_results=True, compute_errors=False,
                                     mu=mu_w)
            my_logger.info("Scan terminated with H = %.2f" % pars["H"])
            if pars["SIGMA"] < 5.:
                my_logger.warning(
                    "Test significance at PEPOCH lower than 5 sigma.")

            index.edit_pulsar(psr_name, PEPOCH=pars["PEPOCH"],
                              F0=pars["F0_BEST"], F1=pars["F1_BEST"])

            # Save data
            index.dump()
