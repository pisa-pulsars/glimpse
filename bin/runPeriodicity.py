#!/usr/bin/env python

import argparse
import os
from os.path import join, exists, dirname
import platform
import time
import sys

from glimpse.monitor import PulsarMonitor
from glimpse.config import PULSAR_INDEX
from pyfermigamma.utilities.fermilogger import FermiLogger

# General variables
os_system = platform.system()
running_python_version = int(sys.version[0])
work_dir = os.getcwd()
script_name = os.path.split(sys.argv[0])[1].split(".")[0]
script_path = join(os.getcwd(), dirname(sys.argv[0]))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="""Performs a pulsar analysis test on time intervals covering the whole dataset.""",
        epilog=""" """)
    parser.add_argument("-n", "--psr-name", type=str, default=None, help="psr name or list of psr names to select")
    parser.add_argument("-i", "--setup-id", type=str, default=None, help="id or list of ids to run")
    parser.add_argument("--force-rerun", action="store_true", help="rerun all intervals")
    parser.add_argument("-d", "--debug", action="store_true", help="debug mode")
    args = parser.parse_args()

    # Start the logger
    start_time = str(time.strftime("y%ym%md%d_H%HM%MS%S", time.localtime()))
    my_logger = FermiLogger(script_name + "_" + start_time + "_logger")
    if args.debug:
        my_logger.set_loglevel("DEBUG")
    else:
        my_logger.set_loglevel("INFO")

    my_logger.info("**************************************")
    my_logger.info("    " + str(script_name))
    my_logger.info("    (Running on " + os_system + " OS)")
    my_logger.info("**************************************")
    
    # List parameters and args
    my_logger.info("Input options:")
    for key in vars(args):
        my_logger.info(key + ": " + str(vars(args)[key]))

    # Read index from json file
    index = PulsarMonitor(PULSAR_INDEX)

    # Select chosen pulsars
    psr_list = []
    if args.psr_name is not None:
        if args.psr_name[0] == "[" and args.psr_name[-1] == "]":
            args.psr_name = args.psr_name[1:-1]
        psr_list = args.psr_name.split(",")
        my_logger.info("Selecting list of pulsars: " + str(psr_list))
    else:
        psr_list = index.list_pulsars()

    # Select chosen setup ids
    id_list = []
    if args.setup_id is not None:
        if args.setup_id[0] == "[" and args.setup_id[-1] == "]":
            args.setup_id = args.setup_id[1:-1]
        id_list = args.setup_id.split(",")
        my_logger.info("Selecting setup ids: " + str(id_list))

    for psr_name in psr_list:
        my_logger.info("Working on PSR " + psr_name)

        # Get list of available setups
        if len(id_list) == 0:
            id_list = [id for id in index.get_pulsar(psr_name)["setups"]]

        # iterate over setup ids
        for setup_id in id_list:
            my_logger.info("Running with setup %s." % setup_id)
            setup_id = int(setup_id)

            # Run using method
            index.run(psr_name, setup_id, force_rerun=args.force_rerun)
