#!/usr/bin/env python

import argparse
import os
from os.path import join, dirname
import sys
import platform
import time

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from cpnest import cpnest

from pyfermigamma.utilities.fermilogger import FermiLogger

from glimpse.sources import FermiPulsar
from glimpse.models.modelmanager import ModelManager

# General variables
os_system = platform.system()
running_python_version = int(sys.version[0])
work_dir = os.getcwd()
script_name = os.path.split(sys.argv[0])[1].split(".")[0]
script_path = join(os.getcwd(), dirname(sys.argv[0]))

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="""Performs a Bayesian search for glitches using outputs from analysis benchmark.""",
        epilog=""" """)
    parser.add_argument("-t", "--threads", type=int, default=1,
                        help="number of threads used by the sampler")
    parser.add_argument("--force-rerun", action="store_true",
                        help="delete old files and rerun")
    parser.add_argument("--dry-run", action="store_true",
                        help="delete old files and rerun")
    parser.add_argument("-d", "--debug", action="store_true",
                        help="debug mode")
    parser.add_argument("conf",
                        help="path to configuration file in YAML format")
    args = parser.parse_args()

    # Start the logger
    start_time = str(time.strftime("y%ym%md%d_H%HM%MS%S", time.localtime()))
    my_logger = FermiLogger(script_name + "_" + start_time + "_logger")
    if args.debug:
        my_logger.set_loglevel("DEBUG")
    else:
        my_logger.set_loglevel("INFO")

    my_logger.info("**************************************")
    my_logger.info("    " + str(script_name))
    my_logger.info("    (Running on " + os_system + " OS)")
    my_logger.info("**************************************")

    # List parameters and args
    my_logger.info("Input options:")
    for key in vars(args):
        my_logger.info(key + ": " + str(vars(args)[key]))

    # Init pulsar and load data
    pulsar = FermiPulsar().from_config(args.config)
    my_logger.info("Selected %d photons." % len(pulsar.get_data()))

    # Run H scan
    #my_logger.info("Running H test...")
    #times = pulsar.get_data()["TIME"]
    #dt = max(times) - min(times)
    #n_f0 = int(5.e-5 * dt)
    #n_f1 = int(np.abs(pulsar.F1) * dt**2)
    #print(n_f0, n_f1)
    #pars = pulsar.run_h_scan(f0_trials=n_f0, f1_trials=n_f1, mu=3.1,
    #                         plot_results=True, compute_errors=False)
    #my_logger.info("Scan terminated with H = %.2f" % pars["H"])

    # Calculate MET times relative to epoch
    t = pulsar.get_data().TIME.values
    wi = pulsar.get_weights(3.1)

    # Init BayesianSearch object
    mcmc = ModelManager().from_yaml(args.config)
    mcmc.load_photon_times(t)
    mcmc.load_weights(wi)

    mcmc.set_parameter_bounds("FRAC_00", [1., mcmc.W])
    mcmc.set_parameter_bounds("FRAC_CNST", [1., mcmc.W])

    parnames = np.array(mcmc.parameters)
    bounds = np.array(mcmc.all_bounds)

    if not args.dry_run:

        nest = cpnest.CPNest(mcmc, verbose=2, nensemble=args.threads, nnest=args.threads)
        nest.run()
        plt.clf()

    if not args.dry_run and os.path.exists("./posterior.dat"):

        # Open file and store to pandas
        with open("./header.txt", "r") as f:
            header = f.readline().split("\t")
        sample = np.loadtxt("./posterior.dat", delimiter=' ')[:, :-1]
        sample = pd.DataFrame(sample, columns=header)

        # Add default values for fixed params
        for pi in parnames[np.invert(mcmc.free)]:
            value = mcmc.default[np.where(parnames == pi)[0][0]]
            sample[pi] = np.ones(len(sample)) * value
        sample = sample[parnames]

        # Init figure
        fig = plt.figure(2, figsize=(10, 10))

        # Plot posterior frequency
        subpars = np.array(mcmc.timing_model.parameters)
        t = np.linspace(min(t), max(t), 1000)
        curve_sample = []
        for index, row in sample.iterrows():
            x = row[subpars].values
            curve = mcmc.timing_model(t, x)[1]
            curve_sample.append(curve)
        q = np.quantile(curve_sample, [0.16, 0.5, 0.84], axis=0)
        ax1 = fig.add_subplot(311)
        ax1.plot(t, q[0], ls="--", color="darkblue")
        ax1.plot(t, q[1], color="darkblue")
        ax1.plot(t, q[2], ls="--", color="darkblue")
        ax1.set_xlim(min(t), max(t))

        # Plot posterior spin-down
        subpars = np.array(mcmc.timing_model.parameters)
        curve_sample = []
        for index, row in sample.iterrows():
            x = row[subpars].values
            curve = mcmc.timing_model(t, x)[2]
            curve_sample.append(curve)
        q = np.quantile(curve_sample, [0.16, 0.5, 0.84], axis=0)
        ax2 = fig.add_subplot(312, sharex=ax1)
        ax2.plot(t, q[0], ls="--", color="darkblue")
        ax2.plot(t, q[1], color="darkblue")
        ax2.plot(t, q[2], ls="--", color="darkblue")

        # Plot posterior profile
        subpars = np.array(mcmc.profile_model.parameters)
        phi = np.linspace(0, 2, 1000)
        curve_sample = []
        for index, row in sample.iterrows():
            x = row[subpars].values
            curve = mcmc.profile_model(phi, x)
            curve_sample.append(curve)
        q = np.quantile(curve_sample, [0.16, 0.5, 0.84], axis=0)
        ax3 = fig.add_subplot(313)
        ax3.plot(phi, q[0], ls="--", color="darkblue")
        ax3.plot(phi, q[1], color="darkblue")
        ax3.plot(phi, q[2], ls="--", color="darkblue")
        ax3.set_xlim(0., 2.)

        plt.tight_layout()
        plt.savefig("./credibility_plot.png")
