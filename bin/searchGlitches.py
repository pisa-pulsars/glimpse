#!/usr/bin/env python

import argparse
import os
from os.path import join, exists
import pandas as pd
import numpy as np
import platform
import time
import sys
from scipy.signal import find_peaks
from scipy.optimize import curve_fit

from glimpse.monitor import PulsarMonitor
from glimpse.utils import timing_model, DAY_TO_SECS, fit_slope
from glimpse.analysis.filters import gaussian_derivative_filter
from glimpse.conf import *
from pyfermigamma.utilities.fermilogger import FermiLogger

import matplotlib.pyplot as plt

# General variables
os_system = platform.system()
running_python_version = int(sys.version[0])
work_dir = os.getcwd()
script_name = os.path.split(sys.argv[0])[1].split(".")[0]
script_path = join(os.getcwd(), os.path.dirname(sys.argv[0]))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="""Performs a bayesian search for glitches using outputs from analysis benchmark.""",
        epilog=""" """)
    parser.add_argument("--force-rerun", action="store_true", help="delete old files and rerun")
    parser.add_argument("-d", "--debug", action="store_true", help="debug mode")
    args = parser.parse_args()

    # Start the logger
    start_time = str(time.strftime("y%ym%md%d_H%HM%MS%S", time.localtime()))
    my_logger = FermiLogger(script_name + "_" + start_time + "_logger")
    if args.debug:
        my_logger.set_loglevel("DEBUG")
    else:
        my_logger.set_loglevel("INFO")

    my_logger.info("**************************************")
    my_logger.info("    " + str(script_name))
    my_logger.info("    (Running on " + os_system + " OS)")
    my_logger.info("**************************************")
    
    # List parameters and args
    my_logger.info("Input options:")
    for key in vars(args):
        my_logger.info(key + ": " + str(vars(args)[key]))

    # Read index from json file
    index = PulsarMonitor(INDEX_FILENAME)
    psr_list = index.list_pulsars()

    # Init directory tree
    if not os.path.exists(GLITCH_DIR):
        os.mkdir(GLITCH_DIR)

    # Init or read dataframe
    if not os.path.exists(GLITCH_FILENAME) or args.force_rerun:
        glitch_df = pd.DataFrame({
            "ID": [],
            "PSR_NAME": [],
            "T_MJD": [],
            "F0": [],
            "F1": [],
            "F2": [],
            "DELTA_F0": [],
            "DELTA_F1": [],
            "DELTA_F2": []
        })
    else:
        glitch_df = pd.read_csv(GLITCH_FILENAME)

    # Iterate over pulsar list
    for psr_name in psr_list:
        my_logger.debug("Working on PSR " + psr_name)

        # Get setup from glitch search setup
        glitch_settings = index.get_glitch_settings(psr_name)
        setup_id = int(glitch_settings["setup"])
        if str(setup_id) not in index.get_pulsar(psr_name)["setups"]:
            my_logger.info("Setup %d does not exist for PSR %s." % (setup_id, psr_name))
        else:
            setup = index.get_setup(psr_name, setup_id)

            # Read analysis results
            csv_filename = os.path.join(PULSAR_WORK_DIR, psr_name, str(setup_id), setup["results_filename"])
            if not os.path.exists(csv_filename):
                my_logger.info("%s not found" % csv_filename)
            else:
                my_logger.info("Reading %s..." % csv_filename)
                data = pd.read_csv(csv_filename)
                data = data.loc[data.SIGMA > 5.]

                # Apply Gaussian filter to F0
                t_mjd = 0.5 * (data.TMIN_MJD.values + data.TMAX_MJD.values)
                slope = fit_slope(t_mjd, data.F0_BEST.values, dy=data.F0_UNC.values)[0]
                data["F0_REL"] = data.F0_BEST - slope * t_mjd

                # TODO: apply higher order filter to F0 and/or also filter F1

                # Search peaks
                f0_filtered = gaussian_derivative_filter(data.F0_REL, sigma=5)
                locs, peaks = find_peaks(f0_filtered)

                # Create figure
                fig, axes = plt.subplots(3)
                axes[0].errorbar(t_mjd, data.F0_REL, yerr=data.F0_UNC, ls="", marker=".", ms=7)
                axes[1].errorbar(t_mjd, data.F1_BEST, yerr=data.F1_UNC, ls="", marker=".", ms=7)
                axes[2].plot(t_mjd, f0_filtered)
                for loc in locs:
                    axes[2].axvline(t_mjd[loc], color="black", ls=":")

                # Iterate over MJD times
                for tg in t_mjd[locs]:
                    label = psr_name + "_%d" % (np.round(tg / 10.) * 10.)

                    # Process ID if not in dataframe
                    if label in glitch_df.ID:
                        my_logger.debug("Skipping %s" % label)
                    else:
                        my_logger.info("Processing %s" % label)
                        subdir = join(GLITCH_DIR, label)
                        if not os.path.exists(subdir):
                            os.mkdir(subdir)
                        new_row = {"ID": label, "PSR_NAME": psr_name, "T_MJD": tg}

                        # Select data
                        mask = (t_mjd >= tg - 300.) * (t_mjd < tg + 300.)
                        x = t_mjd[mask] - tg
                        data_selected = data.loc[mask]
                        new_row["F2"] = np.mean(data_selected.F2_BEST)
                        new_row["DELTA_F2"] = 0.

                        # First, fit F1 with one jump at constant F2
                        def fit_function(t, f1, delta_f1):
                            t = t * DAY_TO_SECS
                            p0 = [0., 0., f1, new_row["F2"]]
                            delta_p = [0., 0., delta_f1, 0.]
                            output = []
                            for item in t:
                                output.append(timing_model(item, p0=p0, t_glitch=0., delta_p=delta_p)[2])
                            return np.array(output)

                        y = data_selected.F1_BEST
                        dy = data_selected.F1_UNC
                        pinit = [np.mean(y), 0.]
                        popt, _ = curve_fit(fit_function, x, y, sigma=dy, p0=pinit)
                        new_row["F1"] = popt[0]
                        new_row["DELTA_F1"] = popt[1]

                        sample = np.linspace(x[0], x[-1], 100)
                        axes[1].plot(sample + tg,
                                     fit_function(sample, *popt),
                                     color="black")

                        # Second, fit F0 with one jump at constant F1
                        def fit_function(t, f0, delta_f0):
                            t = t * DAY_TO_SECS
                            p0 = [0., f0, new_row["F1"] - slope / DAY_TO_SECS]
                            delta_p = [0., delta_f0, new_row["DELTA_F1"]]
                            output = []
                            for item in t:
                                output.append(timing_model(item, p0=p0, t_glitch=0., delta_p=delta_p)[1])
                            return np.array(output)

                        y = data_selected.F0_REL
                        dy = data_selected.F0_UNC
                        pinit = [np.mean(y), 0.]
                        popt, _ = curve_fit(fit_function, x, y, sigma=dy, p0=pinit)
                        new_row["F0"] = popt[0] + slope * tg
                        new_row["DELTA_F0"] = popt[1]

                        axes[0].plot(sample + tg,
                                     fit_function(sample, *popt),
                                     color="black")

                        glitch_df = pd.concat([glitch_df, pd.DataFrame(new_row, index=[0])], ignore_index=True, axis=0)

    glitch_df.to_csv(GLITCH_FILENAME)
    plt.show()
