#!/usr/bin/env python

import os
from os.path import join, dirname, exists
import sys
import shutil
import platform
import argparse
import time

# Custom imports
from pyfermigamma.utilities.fermilogger import FermiLogger
from glimpse.config import CATALOGS, CATALOG_DIR

# General variables
os_system = platform.system()
running_python_version = int(sys.version[0])
work_dir = os.getcwd()
script_name = os.path.split(sys.argv[0])[1].split(".")[0]
script_path = join(os.getcwd(), dirname(sys.argv[0]))

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="""Downloads the gamma-ray pulsar catalog from the Fssc. """,
        epilog=""" """)
    parser.add_argument("catalog", type=str, default="2PC", help="catalog name (only 2PC available so far)")
    parser.add_argument("-d", "--debug", action="store_true", help="debug mode")
    args = parser.parse_args()

    # Start the logger
    start_time = str(time.strftime("y%ym%md%d_H%HM%MS%S", time.localtime()))
    my_logger = FermiLogger(script_name + "_" + start_time + "_logger")
    if args.debug:
        my_logger.set_loglevel("DEBUG")
    else:
        my_logger.set_loglevel("INFO")

    my_logger.info("**************************************")
    my_logger.info("    " + str(script_name))
    my_logger.info("    (Running on " + os_system + " OS)")
    my_logger.info("**************************************")

    # List parameters and args
    my_logger.info("Input options:")
    for key in vars(args):
        my_logger.info(key + ": " + str(vars(args)[key]))

    # Download archive
    catalog = args.catalog
    download_path = CATALOGS[catalog]["download_path"]
    archive_name = os.path.basename(download_path)
    my_logger.info("Downloading " + archive_name)
    cmd = "wget --no-check-certificate " + download_path
    os.system(cmd)

    # Extract archive to a subdir inside CATALOG DIR
    subdir = os.path.join(CATALOG_DIR, catalog)
    my_logger.info("Unpacking to " + subdir)
    if exists(subdir):
        shutil.rmtree(subdir)
    os.mkdir(subdir)
    cmd = "tar zxf " + archive_name + " -C " + subdir + " --strip-components 1"
    os.system(cmd)

    # Remove archive
    my_logger.debug("Removing " + archive_name)
    os.remove(archive_name)
