#!/usr/bin/bash

echo
echo " _______  ___      ___  __   __  _______  _______  _______ "
echo "|       ||   |    |   ||  |_|  ||       ||       ||       |"
echo "|    ___||   |    |   ||       ||    _  ||  _____||    ___|"
echo "|   | __ |   |    |   ||       ||   |_| || |_____ |   |___ "
echo "|   ||  ||   |___ |   ||       ||    ___||_____  ||    ___|"
echo "|   |_| ||       ||   || ||_|| ||   |     _____| ||   |___ "
echo "|_______||_______||___||_|   |_||___|    |_______||_______|"
echo

if test -z "$CONDA_PREFIX"
then
      echo "ERROR! No CONDA_PREFIX env variabile set! Exit"
      return
else
      echo "Env Variable CONDA_PREFIX found!"
fi

# Checking pyfermigamma installation
if test -z "$PYFERMIGAMMA_DIR"
then
  echo "WARNING: PYFERMIGAMMA_DIR not set. A working pyfermigamma installation is recommended."
else
  echo "PYFERMIGAMMA_DIR is $PYFERMIGAMMA_DIR"
fi

# Setting up pyfermigamma path
export PATH=${PYFERMIGAMMA_DIR}/bin:$PATH
export PATH=${PYFERMIGAMMA_DIR}/benchmark:$PATH
export PYTHONPATH=${PYFERMIGAMMA_DIR}:$PYTHONPATH

# Set glimpse path
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
export GLIMPSE_DIR=${SCRIPT_DIR}
echo "GLIMPSE_DIR is $GLIMPSE_DIR"
export PATH=$GLIMPSE_DIR/bin:$PATH
export PYTHONPATH=${GLIMPSE_DIR}:$PYTHONPATH

# Activating conda environment
echo
echo "** Activating Fermi conda environment..."
mamba activate fermi

echo
echo "** Setup complete. Enjoy!"
echo
