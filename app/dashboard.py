import numpy as np
import pandas as pd
import json

import dash
from dash import Input, Output, State, callback, html, dcc, callback_context as ctx, ALL, MATCH
import dash_bootstrap_components as dbc
from datetime import datetime

from styles import *
from sitepage import SitePage
from modals import *
from glimpse.monitor import PulsarMonitor
from glimpse.sources import FermiPulsar
from glimpse.utils.constants import fancy_print
from glimpse.config import PULSAR_INDEX, BAYESIAN_CONFIG

#####################################
#             LAYOUTS               #
#####################################

dashboard = SitePage("/dashboard")

dashboard.description = dcc.Markdown('''
        ---
        ##### Dashboard
        
        ''')

controls = html.Div(
    [
        html.Div([
            html.Tbody("PSR"),
            html.Br(),
            dcc.Dropdown(id="psr_dropdown",
                         options=["PSR"],
                         multi=False),
            html.Div([], id="dropdown_placeholder", style={"display": "none"})
            ], style={"width": "100%", "vertical-align": "top"}
        ),

        html.Br(),
        html.Br(),

        html.Div(
            [
                dbc.Button("Add pulsar", id="add_pulsar_button", n_clicks=None, style=BUTTON_STYLE),
                add_pulsar_modal,
                html.Br(),
                dbc.Button("Add setup", id="add_setup_button", style=BUTTON_STYLE),
                add_setup_modal,
                html.Br(),
                dbc.Button("Update photons", style=BUTTON_STYLE),
                html.Br(),
                dbc.Button("Delete pulsar", style=BUTTON_STYLE),
                html.Br(),
            ], style={"width": "50%", "margin-left": "25%"},
        )
    ],
    style=SIDEBAR_TEXT_STYLE
)

dashboard.sidebar = html.Div(
    children=[
        controls,
    ], style=SIDEBAR_STYLE
)

dashboard.content = html.Div(
    html.Div(
        [], id="card_container", style={"margin-bottom": "5%"}),
    style=CONTENT_STYLE
)


#####################################
#            CALLBACKS              #
#####################################

@callback(Output("card_container", "children"),
          Input("psr_dropdown", "value"),
          Input({"type": "card", "index": ALL}, "children")
          )
def upgrade_cards(psr_name, children):

    index = PulsarMonitor(PULSAR_INDEX)

    card_list = []
    if psr_name in index.list_pulsars():
        for si in index.get_pulsar(psr_name)["setups"]:

            setup = index.get_setup(psr_name, si)

            card_header = dbc.CardHeader(
                dbc.Row(
                    [
                        dbc.Col(
                            html.Tbody(
                                si
                             ), width=1, style=CARD_HEADER_STYLE
                        ),
                        dbc.Col(
                            [
                                html.Small(
                                    "Last modified: " + setup["last_modified"],
                                    className="card-text text-muted",
                                ),
                            ], width=5, align="center"
                        ),
                        dbc.Col(
                            [
                                html.Small(
                                    "Last run: " + setup["last_run"],
                                    className="card-text text-muted",
                                ),
                            ], width=5, align="center"
                        )
                    ]
                )
            )

            card_content = html.Plaintext(fancy_print(setup)[1:-1], style=CARD_TEXT_STYLE)

            card_buttons = html.Div(
                [
                    dbc.Button("Run", style=BUTTON_STYLE),
                    html.Br(),
                    dbc.Button("Delete", style=BUTTON_STYLE, id={"type": "delete_button", "index": int(si)}),
                ],
                className="align-self-bottom",
                style={"padding": "10%"}
            )

            card = dbc.Card(
                [
                    card_header,
                    dbc.Row(
                        [
                            dbc.Col(card_content, width=10),
                            dbc.Col(card_buttons, width=2)
                        ],
                    ),
                    html.Br(),
                    dbc.Row(

                    )
                ], style=CARD_STYLE, className="g-0 mt-4 shadow"
            )

            card_list.append(dbc.Row(card, id={"type": "card", "index": int(si)}))

        return card_list

    else:
        return []


@callback(Output({"type": "card", "index": MATCH}, "children"),
          Input({"type": "delete_button", "index": MATCH}, "n_clicks"),
          State({"type": "delete_button", "index": MATCH}, "id"),
          State("psr_dropdown", "value"),
          State({"type": "card", "index": MATCH}, "children"))
def delete_setup(n_clicks, setup, psr_name, children):

    if n_clicks is None:
        raise dash.exceptions.PreventUpdate()

    index = PulsarMonitor(PULSAR_INDEX)
    index.delete_setup(psr_name, setup["index"])
    index.dump()

    return children


@callback([Output("add_pulsar_modal", "is_open"),
           Output("dropdown_placeholder", "children")],
          [Input("add_pulsar_button", "n_clicks"),
           Input("submit_pulsar_button", "n_clicks")],
          [State("psr_name_input", "value"),
           State("type_input", "value"),
           State("ra_input", "value"),
           State("de_input", "value"),
           State("glitch_setup", "value"),
           State("glitch_sample_size", "value"),
           State("adaptive_switch", "value"),
           State("gamma", "value"),
           State("scale_min", "value"),
           State("scale_max", "value")]
          )
def display_new_pulsar_modal(n_clicks_1, n_clicks_2, psr_name, type, ra, dec,
                             setup_id, sample_size, adaptive, gamma, scale_min, scale_max):

    if n_clicks_1 is None or n_clicks_2 is None:
        raise dash.exceptions.PreventUpdate()

    if ctx.triggered[0]["prop_id"] == "add_pulsar_button.n_clicks":
        return True, str(datetime.now())

    elif ctx.triggered[0]["prop_id"] == "submit_pulsar_button.n_clicks":

        pulsar = FermiPulsar(PSRJ=psr_name, Type=type, RAJ2000=ra, DEJ2000=dec)

        index = PulsarMonitor(PULSAR_INDEX)
        index.add_pulsar(pulsar)
        index.edit_glitch_settings(psr_name, setup=setup_id, n_sample=sample_size, adaptive=adaptive,
                                   gamma=gamma, scale_min=scale_min, scale_max=scale_max)

        index.dump()

        return False, str(datetime.now())


@callback([Output("add_setup_modal", "is_open"),
           Output("psr_name_drop_input", "value"),
           Output("psr_name_drop_input", "options")],
          [Input("add_setup_button", "n_clicks"),
           Input("submit_setup_button", "n_clicks")],
          [State("psr_dropdown", "value"),
           State("psr_dropdown", "options"),
           State("psr_name_drop_input", "value"),
           State("id_input", "value"),
           State("nf0_input", "value"),
           State("nf1_input", "value"),
           State("nf2_input", "value"),
           State("harmonics_input", "value"),
           State("parfile_input", "value"),
           State("window_input", "value"),
           State("sliding_input", "value"),
           State("roi_method_input", "value"),
           State("radius_input", "value"),
           State("emin_input", "value"),
           State("emax_input", "value"),
           State("a_input", "value"),
           State("b_input", "value"),
           State("mode_input", "value"),
           ]
          )
def display_new_setup_modal(n_clicks_1, n_clicks_2, psr_name_start, psr_list,
                            psr_name, setup_id, n_f0, n_f1, n_f2, n_harmonics,
                            parfile, time_window, sliding_time,
                            method, radius, emin, emax, a, b, mode):

    if n_clicks_1 is None or n_clicks_2 is None:
        raise dash.exceptions.PreventUpdate()

    if ctx.triggered[0]["prop_id"] == "add_setup_button.n_clicks":

        return True, psr_name_start, [{"label": item, "value": item} for item in psr_list]

    elif ctx.triggered[0]["prop_id"] == "submit_setup_button.n_clicks":

        index = PulsarMonitor(PULSAR_INDEX)
        index.add_setup(psr_name, setup_id)
        index.edit_setup(psr_name, setup_id=setup_id, time_window=time_window, sliding_time=sliding_time,
                         n_f0=n_f0, n_f1=n_f1, n_f2=n_f2, n_harmonics=n_harmonics, parfile=parfile,
                         method=method, radius=radius, emin=emin, emax=emax, a=a, b=b, mode=mode)

        index.dump()

        return False, psr_name, [{"label": item, "value": item} for item in psr_list]

'''
@callback([Output("psr_dropdown", "options"),
           Output("psr_dropdown", "value")],
          [Input("card_container", "children"),
          Input("dropdown_placeholder", "children")])
def update_psr_list(in1, in2):

    psr_list = PulsarIndex(INDEX_FILENAME).list_pulsars()

    return psr_list, psr_list[0]
'''
