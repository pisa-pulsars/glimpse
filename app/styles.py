#####################################
#              GENERAL              #
#####################################


COLOR_PALETTE = {
    "color1": "rgba(255,255,255,0)",  # titlebar
    "color2": "rgba(255,255,255,1)",  # plot, card, table
    "color3": "linear-gradient(180deg, rgba(47,172,210,1) 0%, rgba(255,255,255,0) 60%)",  # background
    "color4": "#06283D",  # headings, buttons
    "color5": "rgba(255,255,255,0)",  # sidebar
    "color6": "#06283D"   # text
}

GLOBAL_PAGE_STYLE = {
    #"font-family": "monospace",
    "font-size": 15,
    #"width": "1920px",
    #"height": "900px",
    "top": "0%",
    "left": "0%",
    "color": COLOR_PALETTE["color6"]
}


#####################################
#           LAYOUT STYLES           #
#####################################

TITLEBAR_STYLE = {
    "width": "100%",
    "height": "80px",
    "margin-left": "30px",
    "background-color": COLOR_PALETTE["color1"],
    "zIndex": 5,
    "border-width": "4px",
    "border-style": "none none none none",
    "border-color": COLOR_PALETTE["color5"]
}

CARD_STYLE = {
    "background-color": COLOR_PALETTE["color2"],
    "border-width": "4px",
    "border-style": "solid",
    "border-color": COLOR_PALETTE["color2"],
    "width": "800px",
}

DESCRIPTION_STYLE = {
    "margin-top": "10px",
    "margin-left": "30px",
    "width": "1450px",
    "background-color": COLOR_PALETTE["color5"]
}

SIDEBAR_STYLE = {
    "margin-top": "10px",
    "margin-left": "30px",
    "width": "400px",
    "background-color": COLOR_PALETTE["color5"],
    'display': 'inline-block',
    "vertical-align": "top",
}

CONTENT_STYLE = {
    "margin-left": "50px",
    "margin-top": "10px",
    "width": '1000px',
    'display': 'inline-block',
    "vertical-align": "top",
    "padding": "5px 5px"
}

BUTTON_STYLE = {
    "margin-top": "3%",
    "margin-bottom": "3%",
    "width": "100%",
    "color": COLOR_PALETTE["color2"],
    "background-color": COLOR_PALETTE["color4"],
    "border-style": "outset",
    "border-color": COLOR_PALETTE["color4"],
    "horizontal-align": "center",
}

GRAPH_STYLE = {
    "gridcolor": "lightgrey",
    "showline": True,
    "linewidth": 1,
    "linecolor": 'lightgrey',
    "mirror": True
}

#####################################
#            TEXT STYLES            #
#####################################

SIDEBAR_TEXT_STYLE = {
    "font-size": 15,
    "horizontal-align": "left",
}

CARD_TEXT_STYLE = {
    "margin-left": "3%",
    "padding": "1% 0%",
    "font-size": 13
}

CARD_HEADER_STYLE = {
    "font-size": 20,
    "font-weight": "bold",
    "text-align": "center",
    "vertical-align": "center",
    "width": "5%",
    "margin-left": "1%"
}

HEADING_STYLE = {
    "textAlign": "left",
    "font-size": 30,
    "color": COLOR_PALETTE["color4"],
    "text-decoration": "none",
    "font-weight": "bold",
}

LINKS_STYLE = {
    "textAlign": "left",
    "font-size": 20,
    "color": COLOR_PALETTE["color4"],
    "text-decoration": "none"
}
