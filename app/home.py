from dash import Dash, dcc, html, Input, Output, callback
import dash_bootstrap_components as dbc

from styles import *
from sitepage import SitePage

#####################################
#             LAYOUTS               #
#####################################

home = SitePage("/")

home.description = dcc.Markdown('''
        ---
        ##### Welcome to APPLpy
        The Automated Pulsar Periodicity Monitor for Python (APPLpy) provides access to a database of gamma-ray pulsars.

        ''')


#####################################
#            CALLBACKS              #
#####################################
