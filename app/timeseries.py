import json
import numpy as np
import pandas as pd

import dash
from dash import Input, Output, State, callback, html, dcc, callback_context, dash_table, ALL, MATCH
from dash.dash_table import DataTable
import dash_bootstrap_components as dbc
from plotly.subplots import make_subplots
import plotly.graph_objects as go

from styles import *
from sitepage import SitePage
from glimpse.monitor import PulsarMonitor
from glimpse.utils.constants import fit_slope, fancy_print
from glimpse.config import PULSAR_INDEX, BAYESIAN_CONFIG

index = PulsarMonitor(PULSAR_INDEX)

#####################################
#             LAYOUTS               #
#####################################

timeseries = SitePage("/timeseries")

timeseries.description = dcc.Markdown('''
        ---
        ##### Time Series
        
        ''')

controls = html.Div(
    [
        html.Div([
            html.Tbody("PSR"),
            html.Br(),
            dcc.Dropdown(id="psr_dropdown",
                         options=[{"label": psr_name, "value": psr_name}
                                  for psr_name in PulsarMonitor(PULSAR_INDEX).list_pulsars()],
                         value=PulsarMonitor(PULSAR_INDEX).list_pulsars()[0],
                         multi=False),
            html.Div(id="dropdown_placeholder", style={"display": "none"})
            ], style={"width": "50%", "display": "inline-block", "vertical-align": "top"}
        ),
        html.Div([
            html.Tbody("Setup ID"),
            html.Br(),
            dcc.Dropdown(
                id="setup_ids",
                value=None,
                style={"margin-bottom": "5%"})
            ], style={"width": "40%", "display": "inline-block", "vertical-align": "top",
                      "margin-left": "10%"}
        ),

        html.Br(),
        html.Br(),

        html.P("Setup info"),
        html.Div(id="setup_info"),

        html.P("Test probability (sigma)"),
        dcc.RangeSlider(
            id="range_slider",
            min=0,
            max=9,
            step=0.1,
            marks={
                str(h): {"label": str(h)} for h in np.arange(0, 10)},
            value=[3, 9],
            tooltip={"placement": "bottom", "always_visible": True}
        ),

        html.Br(),

        html.P("Display options", style={"textAlign": "left"}),
        html.Div(
            [
                dbc.Checklist(
                    id="options_list",
                    options=[
                        {"label": "Relative F0", "value": "relative"},
                        {"label": "Show glitches", "value": "glitches"}
                    ],
                    value=["relative"],
                    inline=False
                )
            ]
        ),
    ],
    style=SIDEBAR_TEXT_STYLE
)

timeseries.sidebar = html.Div(children=[controls], style=SIDEBAR_STYLE)

timeseries.content = html.Div(
    [
        dcc.Graph(id="errorbar")
    ],
    style=CONTENT_STYLE
)


#####################################
#            CALLBACKS              #
#####################################

@callback(Output(component_id="errorbar", component_property="figure"),
          [Input(component_id="psr_dropdown", component_property="value"),
           Input(component_id="range_slider", component_property="value"),
           Input(component_id="setup_ids", component_property="value"),
           Input(component_id="options_list", component_property="value")])
def update_graph(psr_name, sigma_range, setup_id, options_list):

    fig = make_subplots(
        rows=2,
        cols=1,
        shared_xaxes=True,
        vertical_spacing=0.1
    )
    fig.update_xaxes(tickformat="d")
    fig.update_layout(plot_bgcolor=COLOR_PALETTE["color2"],
                      paper_bgcolor="rgba(255,255,255,0)",
                      margin={"r": 10, "t": 30, "b": 40}
                      )

    df = index.get_results_df(psr_name, setup_id)
    if len(df) > 0:

        mask = (df.SIGMA.values >= sigma_range[1]) + (df.SIGMA.values < sigma_range[0])
        df = df.drop(df[mask].index)
        x = 0.5 * (df.TMIN_MJD + df.TMAX_MJD)

        # F0 plot
        y = df.F0_BEST
        dy = df.F0_UNC
        slope = 0
        mag = 0
        if "relative" in options_list:
            try:
                slope = fit_slope(x.values, y.values, dy.values)[0]
                slope = np.around(slope, 11)
                mag = np.floor(np.log10(np.abs(slope)))
            except IndexError:
                print("Not enough data to fit with a line.")
        fig.add_trace(go.Scatter(x=x, y=y - slope * x, customdata=dy,
                                 error_y=dict(array=dy, visible=True), line={"width": 0},
                                 hovertemplate="F0: %{y:.8f} " + u"\u00B1" + " %{customdata:e}<extra></extra>"),
                                row=1, col=1)
        #text = r"$k = %.1f\cdot10^{%d} Hz\ day^{-1}$" % (- slope / (10 ** mag), mag)
        #fig.add_annotation(x=min(x), y=min(y - slope * x), text=text, showarrow=False, row=1, col=1)
        if "relative" in options_list:
            y_label = r"$F0 - k \cdot MJD\ (Hz)$"
        else:
            y_label = r"$F0 \ (Hz)$"
        fig.update_yaxes(title_text=y_label, row=1)

        # F1 plot
        y = df.F1_BEST
        dy = df.F1_UNC
        fig.add_trace(go.Scatter(x=x, y=y, customdata=dy,
                                 error_y=dict(array=dy, visible=True), line={"width": 0},
                                 hovertemplate="F1: %{y:.3e} " + u"\u00B1" + " %{customdata:.2e}<extra></extra>"),
                      row=2, col=1)

        y_label = r"$F1\ (Hz\ s^{-1})$"
        fig.update_yaxes(title_text=y_label, row=2, showexponent="all", exponentformat="e")

        fig.update_yaxes(**GRAPH_STYLE)
        fig.update_xaxes(**GRAPH_STYLE)

        fig.update_layout(xaxis_showticklabels=True, xaxis2_showticklabels=True, height=800,
                          hovermode="x", showlegend=False)
        fig.update_xaxes(title_text=r"$MJD$", row=2)

    return fig


@callback(Output(component_id="setup_ids", component_property="options"),
          Output(component_id="setup_ids", component_property="value"),
          Input(component_id="psr_dropdown", component_property="value"))
def setup_list(psr_name):

    options = []
    for setup_id in index.get_pulsar(psr_name)["setups"]:
        options.append({"label": str(setup_id), "value": int(setup_id)})

    if len(options) > 0:
        value = options[0]["value"]
    else:
        value = None

    return options, value


@callback(Output(component_id="setup_info", component_property="children"),
          [Input(component_id="psr_dropdown", component_property="value"),
           Input(component_id="setup_ids", component_property="value")])
def print_info(psr_name, setup_id):

    setup = index.get_setup(psr_name, setup_id).copy()
    del setup["parfile"]
    del setup["results_filename"]
    text = fancy_print(setup, indent=0)[1:]

    return html.Plaintext(text, style={"font-size": 13})
