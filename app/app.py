import dash_bootstrap_components as dbc
from dash import Dash, dcc, html, Input, Output, callback
from datetime import datetime

from glimpse.config import HOSTNAME, DEBUG, PULSAR_INDEX
from dashboard import *
from glitches import *
from home import *
from timeseries import *
from glimpse.monitor import PulsarMonitor
from styles import GLOBAL_PAGE_STYLE

# Create a dictionary of site pages
site_dict = {
    "/": home,
    "/time-series": timeseries,
    "/dashboard": dashboard,
    "/glitches": glitches,
}

# Init web application
app = Dash(suppress_callback_exceptions=True,
           external_scripts=[
               "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-MML-AM_CHTML"
           ],
           external_stylesheets=
           [
               dbc.themes.BOOTSTRAP,
               "https://use.fontawesome.com/releases/v5.10.2/css/all.css",
           ]
           )


# Define home layout
def serve_layout():
    layout = html.Div(
        [
            dcc.Location(id="url", refresh=False),
            html.Div(
                id="page-content",
                style={
                    "height": "1000px",
                    "background": COLOR_PALETTE["color3"]
                }
            ),
        ]
    )
    return layout


app.layout = serve_layout


# Page selector
@callback(Output(component_id="page-content", component_property="children"),
          Input(component_id="url", component_property="pathname"))
def display_page(pathname):

    try:
        page = site_dict[pathname]
    except KeyError:
        page = SitePage("/")
        page.description = dcc.Markdown(
            """
            404: Page not found.
            """)

    return page.show()


if __name__ == "__main__":
    app.run_server(host=HOSTNAME, debug=True)
