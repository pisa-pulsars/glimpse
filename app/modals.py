import glob
from os.path import join, basename

from dash import callback, html, callback_context, Input, Output, State, dcc
import dash_bootstrap_components as dbc

from styles import BUTTON_STYLE
from glimpse.monitor import PulsarMonitor, GLITCH_SEARCH_DEFAULT
from glimpse.config import PULSAR_INDEX, PARFILE_DIR

psr_types = ['MSP', 'YRL', 'YRQ']


def white_space(width="10%"):

    return html.Div([], style={"display": "inline-block", "width": width})


def modal_entry(label, type_or_options, id, width="45%", **kwargs):#placeholder="", width="45%", disabled=False, value=None):

    if isinstance(type_or_options, str):
        input_obj = dbc.Input(id=id,
                              type=type_or_options,
                              **kwargs)

    elif isinstance(type_or_options, list):
        input_obj = dbc.Select(id=id,
                               value=type_or_options[0],
                               options=[{"label": item, "value": item} for item in type_or_options],
                               **kwargs)

    else:
        input_obj = dbc.Textarea(
            id=id,
        )

    entry = html.Div(
        [
            html.Div(dbc.Label(label),
                     style={"display": "inline-block", "width": "30%"}),
            html.Div(input_obj,
                     style={"display": "inline-block", "width": "70%"}),
        ],
        style={"height": "50px", "display": "inline-block", "width": width}
    )

    return entry


#####################################
#             LAYOUTS               #
#####################################

add_pulsar_modal = dbc.Modal(
    [
        dbc.ModalHeader(dbc.ModalTitle("Add a new pulsar")),
        dbc.ModalBody(
            [
                modal_entry("PSR name", "text", "psr_name_input", placeholder="J0000+0000"),
                white_space(),
                modal_entry("Type", psr_types, "type_input"),
                modal_entry("RAJ2000", "text", "ra_input", min=0, max=360, placeholder="in deg"),
                white_space(),
                modal_entry("DEJ2000", "text", "de_input", min=-90, max=90, placeholder="in deg"),
                html.Hr(),
                html.H6("Glitch search parameters", style={"height": "40px"}),
                modal_entry("Setup", [0], "glitch_setup", width="30%"),
                white_space("5%"),
                modal_entry("Sample size", "number", "glitch_sample_size", value=1000, min=1, step=1, width="30%"),
                white_space("5%"),
                html.Div(
                    dbc.Switch(
                        id="adaptive_switch",
                        label="Adaptive MCMC",
                        value=True,
                    ),
                    style={"height": "50px", "display": "inline-block"}
                ),
                modal_entry("Gamma", "number", "gamma",
                            value=GLITCH_SEARCH_DEFAULT["gamma"], min=0, disabled=False, width="30%"),
                white_space("5%"),
                modal_entry("Scale min", "number", "scale_min",
                            value=GLITCH_SEARCH_DEFAULT["scale_min"], min=0, disabled=False, width="30%"),
                white_space("5%"),
                modal_entry("Scale max", "number", "scale_max",
                            value=GLITCH_SEARCH_DEFAULT["scale_max"], min=0, disabled=False, width="30%"),
                dbc.ModalFooter(
                    [
                        white_space("80%"),
                        html.Div(
                            dbc.Button(
                                ["Submit"],
                                id="submit_pulsar_button",
                                n_clicks=0,
                                style=BUTTON_STYLE
                            ),
                            style={"display": "inline-block", "width": "20%"}
                        )
                    ]
                ),
            ],
        ),
    ],
    id="add_pulsar_modal",
    is_open=False,
    centered=True,
    size="xl"
)


add_setup_modal = dbc.Modal(
    [
        dbc.ModalHeader(dbc.ModalTitle("Add a new setup")),
        dbc.ModalBody(
            [
                modal_entry("PSR name", [""], "psr_name_drop_input"),
                white_space(),
                modal_entry("Setup ID", "number", "id_input", min=0),
                html.Hr(),
                html.H6("H test parameters", style={"height": "40px"}),
                modal_entry("N F0", "number", "nf0_input", min=0),
                white_space(),
                modal_entry("N F1", "number", "nf1_input", min=0),
                modal_entry("N F2", "number", "nf2_input", min=0, value=1),
                white_space(),
                modal_entry("Harmonics", "number", "harmonics_input", min=1, value=20),
                modal_entry("PAR file", [""], "parfile_input", width="45%", placeholder="Select..."),
                white_space("10%"),
                html.Div(dcc.Upload(dbc.Button("Upload file...", style=BUTTON_STYLE)),
                         style={"width": "20%", "display": "inline-block"}),
                html.Hr(),
                html.H6("Photon selection", style={"height": "40px"}),
                modal_entry("Time window", "number", "window_input", min=0, step=0.1, placeholder="in days"),
                white_space(),
                modal_entry("Sliding time", "number", "sliding_input", min=0, step=0.1, placeholder="in days"),
                html.Div(
                    [
                        html.Div(dbc.Label("ROI method"),
                                 style={"display": "inline-block", "width": "20%"}),
                        html.Div(
                            dbc.RadioItems(
                                options=[{"label": "Cookie", "value": "cookie"},
                                         {"label": "Power Law", "value": "power-law"},
                                         {"label": "PSF-like", "value": "psf-like"},
                                         {"label": "Auto", "value": "auto"}],
                                value="cookie",
                                id="roi_method_input",
                                inline=True,
                                style={"height": "40px"}
                            ),
                            style={"display": "inline-block", "width": "80%"}
                        ),
                    ],
                    style={"display": "inline-block", "width": "100%", "margin-top": "10px"}
                ),
                html.Div(
                    [
                        modal_entry("Radius", "number", "radius_input", min=0, step=0.01, placeholder="in degress", width="30%"),
                        white_space("5%"),
                        modal_entry("E min", "number", "emin_input", min=100., max=300000., step=1., placeholder="in MeV", width="30%"),
                        white_space("5%"),
                        modal_entry("E max", "number", "emax_input", min=0, step=0.01, placeholder="in MeV", width="30%"),
                    ],
                    style={"display": "none"},
                    id="if_cookie"
                ),
                html.Div(
                    [
                        modal_entry("Scale", "number", "a_input", min=0., max=10., step=0.01, placeholder="in degrees"),
                        white_space(),
                        modal_entry("Exponent", "number", "b_input", min=0, step=0.01),
                    ],
                    style={"display": "none"},
                    id="if_powerlaw"
                ),
                html.Div(
                    modal_entry("Mode", ["psf", "frontback"], "mode_input"),
                    style={"display": "none"},
                    id="if_psflike"
                ),
                dbc.ModalFooter(
                    [
                        white_space("80%"),
                        html.Div(
                            dbc.Button(
                                ["Submit"],
                                id="submit_setup_button",
                                n_clicks=0,
                                style=BUTTON_STYLE
                            ),
                            style={"display": "inline-block", "width": "20%"}
                        )
                    ]
                ),
            ],
        ),
    ],
    id="add_setup_modal",
    is_open=False,
    centered=True,
    size="lg"
)


#####################################
#            CALLBACKS              #
#####################################

@callback([Output("gamma", "disabled"),
           Output("scale_min", "disabled"),
           Output("scale_max", "disabled")],
          Input("adaptive_switch", "value")
          )
def adaptive_unlock(is_adaptive):

    return [not is_adaptive for i in range(3)]


@callback([Output("if_cookie", "style"),
           Output("if_powerlaw", "style"),
           Output("if_psflike", "style")],
          Input("roi_method_input", "value")
          )
def show_roi_pars(method):

    output = [{"display": "none"} for i in range(3)]
    if method == "cookie":
        output[0] = {}
    elif method == "power-law":
        output[1] = {}
    elif method == "psf-like":
        output[2] = {}

    return output


@callback(Output("parfile_input", "options"),
          [Input("psr_name_drop_input", "value"),
           Input("add_setup_button", "n_clicks")]
          )
def update_parfile_list(psr_name, n_clicks):

    parfile_list = glob.glob(join(PARFILE_DIR, "PSR" + psr_name + "*.par"))
    output = [{"label": basename(parfile), "value": basename(parfile)} for parfile in parfile_list]

    return output
