import numpy as np
import pandas as pd

import dash
from dash import Input, Output, State, callback, html, dcc, callback_context, dash_table, ALL, MATCH
from dash.dash_table import DataTable
import dash_bootstrap_components as dbc

from styles import *
from sitepage import SitePage
from glimpse.monitor import PulsarMonitor
from glimpse.config import BAYESIAN_CONFIG

#####################################
#             LAYOUTS               #
#####################################

glitches = SitePage("/glitches")

glitches.description = dcc.Markdown('''
        ---
        ##### Glitches
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. 

        ''')


controls = html.Div(
    html.Div(
        [
            dbc.Button("Add glitch", style=BUTTON_STYLE),
            html.Br(),
            dbc.Button("Run selected", style=BUTTON_STYLE),
            html.Br(),
            dbc.Button("View selected", style=BUTTON_STYLE),
            html.Br(),
            dbc.Button("Delete selected", style=BUTTON_STYLE),
            html.Br(),
        ], style={"width": "50%", "margin-left": "25%"},
    ),
    style=SIDEBAR_TEXT_STYLE
)
glitches.sidebar = html.Div(children=[controls], style=SIDEBAR_STYLE)

glitches.content = html.Div(
    [
        dbc.Card(id="glitch_table", className="g-0 mt-4 shadow")
    ],
    style=CONTENT_STYLE
)


#####################################
#            CALLBACKS              #
#####################################

@callback(Output(component_id="glitch_table", component_property="children"),
          [Input(component_id="url", component_property="pathname")]
          )
def update_table(pathname):

    df = pd.read_csv(BAYESIAN_CONFIG)
    df = df.drop(columns=["F0", "F1", "F2"])

    table = DataTable(
        id="table",
        data=df.to_dict("r"),
        row_selectable='single',
        sort_action="native",
        style_header={
            'backgroundColor': COLOR_PALETTE["color2"],
            'fontWeight': 'bold',
            'color': COLOR_PALETTE["color6"]
        },
        style_cell={
            'padding': '5px',
            'color': COLOR_PALETTE["color6"]
        },
        style_as_list_view=True,
        style_cell_conditional=[
            {
                'if': {'column_id': c},
                'textAlign': 'left'
            } for c in ["ID", "PSR_NAME"]
        ],
        style_data_conditional=[
            {
                'if': {'row_index': 'odd'},
                'backgroundColor': COLOR_PALETTE["color2"],
            }
        ],
    )

    return table
