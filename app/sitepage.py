from dash import html, dcc

import etserver.app
import etserver.components
from styles import *

titlebar = html.Div(
    [
        html.Div([etserver.components.Link("GLIMPSE", href="/", style=HEADING_STYLE, id="heading")],
                 style={"display": "inline-block", "margin-top": "20px", "width": "200px"}),
        html.Div([etserver.components.Link("Time series", href="/time-series", style=LINKS_STYLE)],
                 style={"display": "inline-block", "margin-right": "2%", "width": "120px"}),
        html.Div([etserver.components.Link("Glitches", href="/glitches", style=LINKS_STYLE)],
                 style={"display": "inline-block", "margin-right": "2%", "width": "100px"}),
        html.Div([etserver.components.Link("Dashboard", href="/dashboard", style=LINKS_STYLE)],
                 style={"display": "inline-block", "margin-right": "2%", "width": "120px"})
    ],
    style=TITLEBAR_STYLE
)


class SitePage:

    def __init__(self, pathname):

        self.pathname = pathname
        self.content = html.Div()
        self.sidebar = html.Div()
        self.description = html.Div()

    def show(self):

        page = html.Div(
            [
                titlebar,
                html.Div(self.description, style=DESCRIPTION_STYLE),
                html.Div(
                    html.Div(
                        [
                            self.sidebar,
                            self.content
                        ],
                        style={"display": "inline-block", "width": "100%"}),
                    style=GLOBAL_PAGE_STYLE),
                html.Div(id="dummy-div"),
            ]
        )

        return page
