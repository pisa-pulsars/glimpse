#!/bin/bash

echo
echo "==================================================================="
echo "=             Welcome to the GLIMPSE Docker container             ="
echo "==================================================================="
echo
echo "Type   fermi-setup   to initialize the Conda environment."
echo

exec "$@"