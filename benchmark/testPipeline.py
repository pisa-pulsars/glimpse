#!/usr/bin/env python

import argparse
import itertools
import os
import yaml
import glob

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import ray

from glimpse.models.timing import TimingModel
from glimpse.models.profile import ProfileModel
from glimpse.models.noise import TimingNoise
from glimpse.models.spectrum import SpectralModel
from glimpse.models.modelmanager import ModelManager
from glimpse.utils import mjd_to_met
from glimpse.simulators import PulsarSimulator
from glimpse.samplers import NestedSampler

work_dir = os.getcwd()

plot_filename = "benchmark_results.png"
plot_config = [
    {
        "color": "red",
        "size": 0.3,
        "marker": "v",
    },
    {
        "color": "limegreen",
        "size": 0.3,
        "marker": "s",
    },
    {
        "color": "blue",
        "size": 0.3,
        "marker": "^",
    },
    {
        "color": "orange",
        "size": 0.3,
        "marker": "D",
    }
]

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="""Inits and runs simulations of pulsar signals and perform parameter estimation.""",
        epilog=""" """)
    parser.add_argument("--template", type=str, default=None,
                        help="path to the template YAML file")
    parser.add_argument("--id", type=int, default=None,
                        help="only run this job")
    parser.add_argument("--force-rerun", action="store_true",
                        help="rerun completed jobs")
    parser.add_argument("config", type=str, default=None,
                        help="path to the test conf YAML file")
    parser.add_argument("mode",
                        help="init/sim/run/plot")
    args = parser.parse_args()

    # Load simulator data
    print("Reading conf from %s" % args.config)
    with open(args.config, "r") as stream:
        sim_data = yaml.safe_load(stream)
    work_dir = sim_data["workDir"]
    index_filename = os.path.join(work_dir, "index.csv")

    if args.mode == "init":

        # Never overwrite index
        if os.path.exists(index_filename):
            raise FileExistsError("File already exists: %s" % index_filename)

        # Create dataframe and save to index
        else:
            parameter_dict = sim_data["modelParameters"]
            parnames = list(parameter_dict.keys())
            pars_list = []
            for pi in parnames:
                entry = parameter_dict[pi]
                pars_list.append([entry] if isinstance(entry, float) else entry)
            points = np.array(
                [value for value in itertools.product(*pars_list)])
            index = pd.DataFrame(points, columns=parnames)
            index.to_csv(index_filename, index_label="ID")
            print("Index saved to %s" % index_filename)
        exit(0)

    print("Reading index from %s" % index_filename)
    index = pd.read_csv(index_filename, index_col="ID")

    # Init simulator
    my_sim = PulsarSimulator(PSRJ=sim_data["PSRJ"])
    for item in ["RAJ", "DECJ", "roiRadius", "tmin", "tmax", "emin", "emax",
                 "signalToBkg", "tnCutHigh", "nPhotons", "seed"]:
        my_sim.__setattr__(item, sim_data[item])

    # Set models
    my_tm = TimingModel.from_yaml(args.config)
    my_pm = ProfileModel.from_yaml(args.config)
    my_sed = SpectralModel.from_yaml(args.config)
    my_tn = TimingNoise.from_yaml(args.config)

    bayes_factor = []

    for idx, row in index.iterrows():

        run_job = True
        if args.id is not None:
            run_job = idx == args.id

        subdir = os.path.join(work_dir, "%s" % str(idx).zfill(3))
        if args.mode in ["sim", "run", "plot"] and run_job:

            data_filename = os.path.join(subdir, "data.csv")
            print("Working on item %d" % idx)

            # Get parameters and set simulator model
            tm_pars = np.array([row[pi] for pi in my_tm.parameters])
            my_sim.set_timing_model(my_tm, tm_pars)

            pm_pars = np.array([row[pi] for pi in my_pm.parameters])
            my_sim.set_profile_model(my_pm, pm_pars)

            sed_pars = np.array([row[pi] for pi in my_sed.parameters])
            my_sim.set_spectral_model(my_sed, sed_pars)

            tn_pars = np.array([row[pi] for pi in my_tn.parameters])
            my_sim.set_noise_model(my_tn, tn_pars)

            if args.mode == "sim":
                os.makedirs(subdir, exist_ok=True)
                print("    Running simulation...")
                my_sim.outdir = subdir
                my_sim.run()
                my_sim.plot()

                my_sim.get_dataframe().to_csv(data_filename)
                print("    Saved data to %s" % data_filename)

            if args.mode == "run" or args.mode == "plot":

                print("    Reading data from %s" % data_filename)
                data = pd.read_csv(data_filename)

                pulsar = my_sim.get_as_pulsar()
                pulsar.set_output_dir(subdir)
                pulsar.load_data(data)

                # Run H scan
                pulsar.select_by_time(my_sim.tmin, my_sim.tmin + 30.)
                opt_pars = pulsar.run_h_scan(compute_errors=False, mu=3.)
                pulsar.set_parameters(PEPOCH=opt_pars["PEPOCH"])
                pulsar.set_parameters(F0=opt_pars["F0_BEST"])
                pulsar.set_parameters(F1=opt_pars["F1_BEST"])

                # optimize weights
                opt_mu = pulsar.optimize_weights(verbose=True)
                pulsar.reset_cuts()

                # Set default template path (inside work dir)
                if not args.template:
                    args.template = os.path.join(work_dir,
                                                 "model_template.yaml")

                # Init model from conf
                my_model = ModelManager.from_yaml(args.template)

                # Load data and weights to model
                ti = pulsar.get_data()["TIME"].values - mjd_to_met(my_tm.PEPOCH)
                my_model.load_photon_times(ti)
                wi = pulsar.get_weights(opt_mu)
                my_model.load_weights(wi)

                # Test with null hypothesis
                print("    Testing null hypothesis")
                hypothesis_dir = os.path.join(subdir, "null")
                os.makedirs(hypothesis_dir, exist_ok=True)
                my_model.outDir = hypothesis_dir

                # Take note of free pars scheme
                free_pars = my_model.free

                # Fix glitch to zero
                for parname in ["GLEP_0", "GLF0D_0", "GLTD_0"]:
                    my_model.set_parameter_free(parname, False)
                my_model.set_parameter_default("GLF0D_0", 0.)

                my_model.to_yaml(os.path.join(hypothesis_dir,
                                              "model_template.yaml"))
                print(my_model)

                if ray.is_initialized():
                    ray.shutdown()
                nest = NestedSampler(my_model, verbose=2, nensemble=4, nnest=4,
                                     output=hypothesis_dir, resume=True,
                                     periodic_checkpoint_interval=60)

                if args.mode == "run":
                    if any([
                        args.force_rerun,
                        not os.path.exists(
                            os.path.join(nest.output, "cpnest.h5"))
                    ]):
                        print("    Running sampler")
                        nest.run()
                        plt.close()
                    else:
                        print("    Skipped")

                if args.mode == "plot":
                    print("    Plotting posterior")
                    nest.plot_timing()
                    nest.plot_profile()
                    nest.plot_corner()

                # Test with glitch hypothesis
                print("    Testing glitch hypothesis")
                hypothesis_dir = os.path.join(subdir, "glitch")
                os.makedirs(hypothesis_dir, exist_ok=True)
                my_model.outDir = hypothesis_dir

                my_model.free = free_pars
                for parname in ["GLEP_0", "GLF0D_0", "GLTD_0"]:
                    my_model.set_parameter_free(parname, True)
                my_model.set_parameter_default("GLF0D_0", row["GLF0D_0"])

                my_model.set_parameter_bounds("GLF0D_0",
                                              [row["GLF0D_0"] * (1 - 5.e-1),
                                               row["GLF0D_0"] * (1 + 5.e-1)])
                my_model.to_yaml(os.path.join(hypothesis_dir,
                                              "model_template.yaml"))
                print(my_model)

                if ray.is_initialized():
                    ray.shutdown()
                nest = NestedSampler(my_model, verbose=2, nensemble=4, nnest=4,
                                     output=hypothesis_dir, resume=True,
                                     periodic_checkpoint_interval=60)

                if args.mode == "run":
                    if any([
                        args.force_rerun,
                        not os.path.exists(
                            os.path.join(nest.output, "cpnest.h5"))
                    ]):
                        print("    Running sampler")
                        nest.run()
                        plt.close()
                    else:
                        print("    Skipped")

                if args.mode == "plot":
                    print("    Plotting posterior")
                    nest.plot_timing()
                    nest.plot_profile()
                    nest.plot_corner()

        elif args.mode == "out":

            Z_null = []
            evidence_files = glob.glob(
                os.path.join(subdir, "null", "*_evidence_*.txt"))
            for item in evidence_files:
                Z_null.append(np.loadtxt(item)[0])

            Z_glitch = []
            evidence_files = glob.glob(
                os.path.join(subdir, "glitch", "*_evidence_*.txt"))
            for item in evidence_files:
                Z_glitch.append(np.loadtxt(item)[0])

            bayes_factor.append(np.mean(Z_glitch) - np.mean(Z_null))

    if args.mode == "out":
        index["B"] = bayes_factor
        amplitudes = np.unique(index["AMPL"])
        indexes = np.unique(index["BETA"])

        fig, axes = plt.subplots(
            nrows=2, ncols=2, figsize=[8, 6], sharex=True, sharey=True)

        i = 0
        for beta in indexes[::-1]:
            ax = axes.ravel()[i]
            ax.axhline(5, ls="--", color="black")
            ax.text(-8, 45, r"$\beta = %d$" % beta)
            selection_1 = index.loc[index["BETA"] == beta]

            j = 0
            for ampl in amplitudes:
                selection_2 = selection_1.loc[selection_1["AMPL"] == ampl]
                ax.plot(
                    np.log10(selection_2["GLF0D_0"]), selection_2["B"],
                    marker=plot_config[j]["marker"],
                    ms=plot_config[j]["size"] * 15,
                    color=plot_config[j]["color"],
                    ls=":",
                    label=r"$\log_{10} (A\ /\ {\rm Hz}^{-1}) = %d$" % np.log10(
                        ampl))
                j += 1
            i += 1

    axes[0][0].legend(loc='lower center', bbox_to_anchor=(1., 1.05), ncol=2)
    axes[0][0].set_ylabel(r"$\log B$")
    axes[1][0].set_ylabel(r"$\log B$")
    axes[1][0].set_xlabel(r"$\log_{10} (\Delta f_t\ /\ {\rm Hz})$")
    axes[1][1].set_xlabel(r"$\log_{10} (\Delta f_t\ /\ {\rm Hz})$")

    plt.tight_layout()
    plt.subplots_adjust(hspace=0, wspace=0)
    plt.savefig(plot_filename)
