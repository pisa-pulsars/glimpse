FROM registry.gitlab.com/mrazzano/pyfermigamma:v1.0.3
SHELL ["/bin/bash", "-c"]

RUN mkdir /home/fermi/glimpse
COPY . /home/fermi/glimpse/

RUN source /fermi/bin/activate && \
    pip install -r /home/fermi/glimpse/requirements.txt --no-input && \
    source /fermi/bin/deactivate

USER fermi
WORKDIR /home/fermi

RUN echo "export PYFERMIGAMMA_DIR=/home/fermi/pyfermigamma" >> ~/.bashrc
RUN echo "export GLIMPSE_DIR=/home/fermi/applpy" >> ~/.bashrc
RUN echo "export PATH=/home/fermi/pyfermigamma/bin:/home/fermi/applpy/bin:/home/fermi/pyfermigamma/scripts:$PATH" >> ~/.bashrc
RUN echo "export PYTHONPATH=/home/fermi/applpy:/home/fermi/pyfermigamma:$PYTHONPATH" >> ~/.bashrc
RUN echo "alias fermi-setup='source /fermi/bin/activate'" >> ~/.bashrc

COPY ./docker/entrypoint.sh /home/fermi/entrypoint.sh
ENTRYPOINT ["/bin/bash", "/home/fermi/entrypoint.sh"]
CMD ["bash"]