from glimpse.simulators import PulsarSimulator

config = "./config.yaml"
outfile = "./fake-pulsar_data.csv"

my_sim = PulsarSimulator.from_yaml(config)

import psutil
print(psutil.cpu_freq())

# Python program to show time by process_time()
from time import process_time
t1_start = process_time()
my_sim.run()
t1_stop = process_time()
print("Elapsed time:", t1_stop - t1_start)

my_sim.plot()
my_sim.plot()
my_sim.get_dataframe().to_csv(outfile, index=False)
print(f"Saved to {outfile}")

