import numpy as np
import matplotlib.pyplot as plt

from glimpse.models.components import SpinDown, TransientChange
from glimpse.models.timing import TimingModel

my_model = TimingModel()
my_model.PEPOCH = 54750.0

my_model.add_component(SpinDown(3))
my_model.add_component(TransientChange(), glitch_number=0)
my_model.add_component(TransientChange(), glitch_number=1)

my_pars = {
    "PH": 0., "F0": 3.16564302, "F1": -3.5668e-12, "F2": 0.,
    "GLEP_0": 56.0, "GLF0D_0": 1.5e-5, "GLTD_0": 230.0,
    "GLEP_1": 211.0, "GLF0D_1": 2.e-5, "GLTD_1": 90.0
}
x = np.array([my_pars[parname] for parname in my_model.parameters])

my_model.plot(x, tstart=54682.60, tstop=55282.60, scale="MJD")
plt.show()
