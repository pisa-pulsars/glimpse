import numpy as np
import matplotlib.pyplot as plt

from glimpse.models.profile import MultipleGaussian

my_model = MultipleGaussian(2)
my_pars = {
    "PHI0_00": 0.4, "SIGMA_00": 0.05,
    "PHI0_01": 0.6, "SIGMA_01": 0.08,
    "FRAC_00": 0.2, "FRAC_01": 0.3, "FRAC_CNST": 0.5
}
x = np.array([my_pars[parname] for parname in my_model.parameters])
my_model.plot(x)
plt.show()
