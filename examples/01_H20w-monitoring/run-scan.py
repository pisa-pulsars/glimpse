import numpy as np
import pandas as pd
from glimpse.sources import FermiPulsar
from glimpse.models.timing import TimingModel
from glimpse.utils import met_to_mjd, DAY_TO_SECS

config = "./config.yaml"
datafile = "../00_simulate-pulsar/fake-pulsar_data.csv"
out_filename = "./fake-pulsar_monitor.csv"
plot_dir = "./plots"
time_window, sliding_days = 10., 0.
N0, N1 = 200, 100

pulsar = FermiPulsar.from_yaml(config)
data = pd.read_csv(datafile)
idx = np.argsort(data["TIME"].values)
data = data.iloc[idx]

# Find the best pars and optimize mu
pulsar.load_data(data)

'''
trange = np.array([0., time_window]) + met_to_mjd(min(pulsar.get_data().TIME))
pulsar.select_by_time(*trange, scale="MJD")
pars = pulsar.run_h_scan(
    f0_trials=100, f1_trials=50, mu=2.3,
    plot_results=True, compute_errors=False)
pulsar.set_parameters(
    PEPOCH=pars["PEPOCH"], F0=pars["F0_BEST"], F1=pars["F1_BEST"])
exit()
#mu_best = pulsar.optimize_weights(verbose=True)
'''
mu_best=2.3
pulsar.reset_cuts()
pulsar.set_output_dir(plot_dir)


# Cheating...
model_config = "../00_simulate-pulsar/config.yaml"
my_model = TimingModel.from_yaml(model_config)
print(my_model.parameters)
x = np.array([0., 3.16564302, -3.5668e-12, 0., 50.0, 30.0, 2.e-4])

# Monitor continually
out_df = pd.DataFrame()
trange = np.array([0., time_window]) + met_to_mjd(min(pulsar.get_data().TIME))
tstop = met_to_mjd(max(pulsar.get_data().TIME))
while trange[1] < tstop:
    print("Running MJD interval " + str(trange))
    pulsar.select_by_time(*trange, scale="MJD")
    print(f"    N photons: {len(pulsar.get_data())}")
    t0 = trange[0] + time_window / 2
    f0_pred, f1_pred = my_model(DAY_TO_SECS * (t0 - my_model.PEPOCH), x)[[1, 2]]
    print(f"    START: %.8f %.4e" % (f0_pred, f1_pred))
    pulsar.set_parameters(
        PEPOCH=t0, F0=f0_pred, F1=f1_pred)
    pars = pulsar.run_h_scan(
        f0_trials=N0, f1_trials=N1, mu=mu_best,
        plot_results=True, compute_errors=True)
    print(f"    F0 = {pars['F0_BEST']}\n"
          f"    F1 = {pars['F1_BEST']}\n"
          f"    H = {pars['H']}\n"
          f"    SIGMA = {pars['SIGMA']}")
    pulsar.set_parameters(
        PEPOCH=pars["PEPOCH"], F0=pars["F0_BEST"], F1=pars["F1_BEST"])
    del pars["PULSE_PHASE"]
    out_df = pd.concat([out_df, pd.DataFrame(pars, index=[0])], ignore_index=True)
    out_df.to_csv(out_filename, index=False)
    trange += time_window - sliding_days
    pulsar.reset_cuts()
