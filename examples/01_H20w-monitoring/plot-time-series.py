import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from glimpse.utils import fit_slope, DAY_TO_SECS, taylor, mjd_to_met
from glimpse.models.components import SpinDown, TransientChange
from glimpse.models.timing import TimingModel

in_filename = "./fake-pulsar_monitor.csv"
df = pd.read_csv(in_filename)

fig = plt.figure(figsize=(8, 6))
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212, sharex=ax1)
axes = [ax1, ax2]

idx = np.argsort(df.PEPOCH)
df = df.iloc[idx]
t = df.PEPOCH.values
f0, df0 = df["F0_BEST"].values, df["F0_UNC"].values
f1, df1 = df["F1_BEST"].values, df["F1_UNC"].values

my_model = TimingModel()
my_model.PEPOCH = 55050.0
my_model.add_component(SpinDown(3))
my_model.add_component(TransientChange(), glitch_number=0)
my_pars = {
    "PH": 0., "F0": 3.16564302, "F1": -3.5668e-12, "F2": 0.,
    "GLEP_0": 50., "GLF0D_0": 2.e-4, "GLTD_0": 30.
}
p = np.array([my_pars[parname] for parname in my_model.parameters])
x = np.linspace(df.TMIN_MJD.values[0], df.TMAX_MJD.values[-1], 1000)
y = my_model(DAY_TO_SECS * (x - my_model.PEPOCH), p)

t0 = my_model.PEPOCH
axes[0].plot(x, y[1] - p[2] * mjd_to_met(x - t0), color="darkblue")
axes[0].errorbar(t, f0 - p[2] * mjd_to_met(t - t0), yerr=df0, ls="", ms=5, marker=".", color="black")
axes[0].set_ylabel(r"$f - \dot f(t_0) \times (t - t_0)$ [Hz] ")

axes[1].plot(x, y[2], color="darkblue")
axes[1].errorbar(t, f1, yerr=df1, ls="", ms=5, marker=".", color="black")
axes[1].set_ylabel(r"$\dot f$ [Hz/s]")
axes[1].set_xlabel(r"MJD")
axes[1].set_xlim(df.TMIN_MJD.values[0], df.TMAX_MJD.values[-1])

axes[0].axvline(my_model.PEPOCH, color="black", ls="--")
axes[1].axvline(my_model.PEPOCH, color="black", ls="--")

plt.subplots_adjust(right=0.995, left=0.12, bottom=0.08, top=0.95)
plt.savefig("./results_plot.png")
