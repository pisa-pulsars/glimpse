import ray

from glimpse.sources import FermiPulsar
from glimpse.models.modelmanager import ModelManager
from glimpse.utils import mjd_to_met
from glimpse.samplers import NestedSampler

conf = "./sampler.yaml"

pulsar = FermiPulsar.from_yaml(conf)
my_model = ModelManager.from_yaml(conf)
print(my_model)

ti = pulsar.get_data()["TIME"].values - mjd_to_met(my_model.timing_model.PEPOCH)
my_model.load_photon_times(ti)

wi = pulsar.get_weights(2.3)
my_model.load_weights(wi)

if ray.is_initialized():
    ray.shutdown()
nest = NestedSampler(my_model, verbose=2, nensemble=4, nnest=4, output=my_model.outDir, )
nest.run()
