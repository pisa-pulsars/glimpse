import ray

from glimpse.sources import FermiPulsar
from glimpse.models.modelmanager import ModelManager
from glimpse.utils import mjd_to_met
from glimpse.samplers import NestedSampler

conf = "./sampler.yaml"
datafile = "../00_simulate-pulsar/fake-pulsar_data.csv"
outdir = "./out"

pulsar = FermiPulsar.from_yaml(conf)
my_model = ModelManager.from_yaml(conf)
print(my_model)

ti = pulsar.get_data()["TIME"].values - mjd_to_met(my_model.timing_model.PEPOCH)
wi = pulsar.get_weights(2.3)
my_model.load_photon_times(ti)
my_model.load_weights(wi)

if ray.is_initialized():
    ray.shutdown()
nest = NestedSampler(my_model, verbose=2, nensemble=6, nnest=6, output=outdir, )

nest.plot_timing(outfile="posterior_timing.png")
nest.plot_profile(n_bins=30, outfile="posterior_profile.png")
nest.plot_corner(outfile="posterior_corner_FULL.png")
nest.plot_corner(parameters=["F0", "F1", "F2", "GLEP_0", "GLF0D_0", "GLTD_0"],
                 outfile="posterior_corner_TIMING.png")
nest.plot_corner(parameters=["PHI0_00", "SIGMA_00", "FRAC_00", "PHI0_01", "SIGMA_01", "FRAC_01", "FRAC_CNST"],
                 outfile="posterior_corner_PROFILE.png")
