class BaseModel:
    """Base class for models.

    Attributes:
        parameters (list): list of parameter names

    All child models must implement the `__call__` method to compute the models.
    The `__call__` method must be a function of an array with `dtype`=`float`
    including the points for which the models is computed; a list containing the
    values of the models parameters. The shape must match the length of `self.parameters`.
    """

    def __init__(self):

        self.parameters = []
        self.id = 0

    def __repr__(self):

        output = self.class_name + "()"

        return output

    @property
    def class_name(self):

        return self.__class__.__name__

    def check_input_pars(self, x):
        """Checks that the length of the input array matches the number of parameters.

        Args:
            x (np.ndarray): models parameters

        Raises:
            ValueError: When `len(x)` is not equal to `len(self.parameters)`
        """

        if len(x) != len(self.parameters):
            raise ValueError("Invalid input: an array of length %d is expected."
                             % len(self.parameters))

    def check_parname(self, parname):
        """Checks that the input parameter name is in `self.parameters`.

        Args:
            parname (str): models parameter name

        Raises:
            ValueError: When `parname` is not in `self.parameters`
        """

        if parname not in self.parameters:
            raise ValueError("Invalid input: %s not in available parameters."
                             % parname)
