import numpy as np
from scipy.integrate import quad
import yaml

from glimpse.models.base import BaseModel


class SpectralModel(BaseModel):

    def __init__(self, emin=100., emax=300000.):

        super().__init__()
        self.emin = emin
        self.emax = emax
        self.parameters = []

    def integral(self, E, x):

        def my_func(point):
            return self.function(point, x)

        F = quad(my_func, self.emin, E)[0]

        return F

    def norm(self, x):

        return self.integral(self.emax, x) - self.integral(self.emin, x)

    def __call__(self, E, x):

        return self.function(E, x) / self.norm(x)

    @classmethod
    def from_yaml(cls, filename):

        with open(filename, "r") as stream:
            data = yaml.safe_load(stream)

        my_model = eval(data["spectralModel"]["name"])

        return my_model


class PowerLawSuperExpCutoff2(SpectralModel):

    def __init__(self, emin=100., emax=300000.):

        super().__init__(emin, emax)
        self.parameters = ["INDEXS", "EXPFACTOR", "INDEX2"]

    def function(self, E, x):

        self.check_input_pars(x)
        return (E / 1000.)**x[0] * np.exp(-x[1] * E**x[2])

    #def integral(self, E, x):
    #
    #    return self.function(E, x) * E / (x[0] + 1) * (1 - x[1] * x[2] * E**(x[2] - 1))


class PowerLawSuperExpCutoff4(SpectralModel):

    def __init__(self, emin=100., emax=300000., scale=1000.):

        super().__init__(emin, emax)
        self.parameters = ["INDEXS", "EXPFACTORS", "INDEX2"]
        self.scale = scale

    def function(self, E, x):

        self.check_input_pars(x)

        E = E / self.scale
        gamma, d, b = x

        mask = b * np.log(E) < np.exp(-2)
        flux_1 = E**(gamma - d/2 * np.log(E) -
                     d * b / 6 * np.log(E)**2 -
                     d * b**2 / 24 * np.log(E)**3)
        flux_2 = E**(gamma + d / b) * \
                 np.exp(d / b**2 * (1 - E**b))

        return np.where(mask, flux_1, flux_2)


class PowerLaw(SpectralModel):

    def __init__(self, emin=100., emax=300000., scale=1000.):

        super().__init__(emin, emax)
        self.parameters = ["INDEX"]
        self.scale = scale

    def function(self, E, x):

        self.check_input_pars(x)

        return (E / self.scale)**x[0]


class BlackBody(SpectralModel):

    def __init__(self, emin=0.1, emax=20.):

        super().__init__(emin, emax)
        self.parameters = ["TEMPERATURE"]

    def function(self, E, x):

        self.check_input_pars(x)

        return (E / x[0]) ** 3 / (np.exp(E / x[0]) - 1)
