import numpy as np
import matplotlib.pyplot as plt
import yaml
import os

from glimpse.models.components import Component, SpinDown, \
    PermanentChange, TransientChange
from glimpse.utils import mjd_to_met, met_to_mjd, load_parfile, DAY_TO_SECS


class TimingModel(Component):
    """
    Class for timing models definition. A models is defined by adding spin-down
    and/or glitch components, which may share the same glitch epoch.

    Attributes:
        parameters (list): list of parameter names
        components (list): list of glimpse.models.components.Component objects
        PEPOCH (float): the MJD reference epoch of the models

    The `__call__` method is a function of `t` (float, np.ndarray), the time in
    seconds measured with respect to PEPOCH, and `x` (`list`), the values of the
    models parameters. The `__call__` method iteratively calls each component and
    computes the sum. It returns an array of shape (3, n) with the values of
    phase, frequency and frequency derivative at the n input times.
    """

    def __init__(self):

        super().__init__()
        self.components = []
        self.PEPOCH = 0.

    def __call__(self, t, x):

        self.check_input_pars(x)
        #t = t - mjd_to_met(self.PEPOCH)

        pt = 0.
        for item in self.components:
            mask = [parname in item.parameters for parname in self.parameters]
            x_sub = x[mask]
            pt = pt + item(t, x_sub)

        return pt

    def add_component(self, item, glitch_number=0):
        """
        Add a timing component to the models.

        Args:
            item (glimpse.mcmc.components.Component):
                the timing component to add.
            glitch_number (int, optional):
                set a glitch number for the component. Glitches with the same glitch number share the same GLEP.
        """

        if not isinstance(item, Component):
            raise TypeError("the input must be an object of class Component.")

        if not isinstance(item, SpinDown):
            for pi in range(len(item.parameters)):
                item.parameters[pi] += "_%d" % glitch_number
                item.id = glitch_number

        self.components.append(item)
        self.parameters = np.concatenate([self.parameters, item.parameters])
        _, idx = np.unique(self.parameters, return_index=True)
        self.parameters = self.parameters[np.sort(idx)]

    def remove_component(self, i):
        """
        Remove timing component from the models.

        Args:
            id (int): the list index of the component to remove.
        """
        del self.components[i]
        self._refresh()

    def from_parfile(self, filename):
        """
        Initialize models from TEMPO2 parfile.

        Args:
            filename (str): path to the input parfile.
        """

        pars_dict = load_parfile(filename)

        self.components = []
        self._refresh()
        self.PEPOCH = float(pars_dict["PEPOCH"])

        x = [0.]
        derivatives = ["F%d" % ti for ti in range(10)]
        for item in derivatives:
            if item in pars_dict:
                x.append(pars_dict[item])
        self.add_component(SpinDown(len(x) - 1))

        for gi in range(10):
            if "GLEP_%d" % gi in pars_dict:
                x.append(float(pars_dict["GLEP_%d" % gi]) - self.PEPOCH)
                if "GLF0_%d" % gi in pars_dict:
                    self.add_component(PermanentChange(), glitch_number=gi)
                    for prefix in ["GLPH", "GLF0", "GLF1"]:
                        x.append(pars_dict[prefix + "_%d" % gi])
                if "GLTD_%d" % gi in pars_dict:
                    self.add_component(TransientChange(), glitch_number=gi)
                    for prefix in ["GLTD", "GLF0D"]:
                        x.append(pars_dict[prefix + "_%d" % gi])

        self._refresh()

        return self, np.array(x, dtype=float)

    @classmethod
    def from_yaml(cls, filename):

        with open(filename, "r") as stream:
            data = yaml.safe_load(stream)

        my_model = TimingModel()
        my_model.PEPOCH = data["timingModel"]["PEPOCH"]

        for item in data["timingModel"]["components"]:
            cstring = item["name"]
            my_model.add_component(eval(cstring), int(item["gid"]))

        return my_model

    def to_yaml(self, filename, append=True):

        clist = []
        for item in self.components:
            clist.append(
                {
                    "name": str(item),
                    "gid": item.id
                }
            )

        data = {
            "timingModel": {
                "PEPOCH": self.PEPOCH,
                "components": clist
            }
        }

        if append and os.path.exists(filename):
            with open(filename, "r") as stream:
                old = yaml.safe_load(stream)
            if old:
                data.update(old)

        with open(filename, "w") as stream:
            yaml.safe_dump(data, stream)

    def plot(self, x, tstart, tstop, scale="MJD"):
        """
        Plots the component in a chosen time range.

        Args:
            x (np.ndarray): the parameters of the models
            tstart (float): lower boundary of the x axis
            tstop (float): upper boundary of the x-axis
            scale (str): time scale (choose MJD or MET)

        Returns:
            fig (matplotlib.pyplot.Figure): Instance of the Figure containing the plot.
        """

        self.check_input_pars(x)

        fig, axes = plt.subplots(3, 1, sharex="all", figsize=(10, 8))
        axes[0].set_ylabel("phi")
        axes[1].set_ylabel("f (Hz)")
        axes[2].set_ylabel("f_dot (Hz/s)")

        t = np.linspace(tstart, tstop, 1000)
        if scale == "MJD":
            t = mjd_to_met(t) - mjd_to_met(self.PEPOCH)
            axes[2].set_xlabel("MJD - %.2f" % self.PEPOCH)
        elif scale == "MET":
            t = t - mjd_to_met(self.PEPOCH)
            axes[2].set_xlabel("seconds since MJD %.2f" % self.PEPOCH)
        else:
            axes[2].set_xlabel("seconds since PEPOCH" % self.PEPOCH)

        fig.suptitle(self.__repr__()[:-2] + str(x), size="medium")

        y = self.__call__(t, x)
        for pi in range(len(axes)):
            if scale == "MJD":
                axes[pi].plot(t / DAY_TO_SECS, y[pi])
            else:
                axes[pi].plot(t, y[pi])
            #axes[pi].axvline(self.PEPOCH, ls="--", color="black")
        return fig

    def _refresh(self):
        """Hidden method that performs a cleanup of the parameter dictionary
        after the removal of a component.
        """

        self.parameters = []
        for item in self.components:
            self.parameters = np.concatenate([self.parameters, item.parameters])
