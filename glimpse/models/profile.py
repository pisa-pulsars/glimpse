import matplotlib.pyplot as plt
import yaml
import os

from glimpse.distributions import *
from glimpse.models.base import BaseModel
from glimpse.utils import fold_phases

from scipy.optimize import minimize


class ProfileModel(BaseModel):
    """
    Base class for pulse profile models definition.

    Attributes:
        parameters (list): list of parameter names
        M (int): number of components

    All child classes must implement the `__call__` and `integrate` methods.
    The `__call__` method is a function of `phi` (float, np.ndarray), the
    fractional rotational phase, and `x` (`list`), the values of the
    models parameters. The `__call__` method returns an array of shape (n) with
    the values of the models at the n input phases.
    The `integrate` method is a function of `x` and returns the integral of the
    models in the phase range [0, 1].
    """

    def __init__(self):

        super().__init__()
        self.parameters = []
        self.M = 0

    def __call__(self, phi, x):

        self.check_input_pars(x)

        phi = np.array([phi]) if isinstance(phi, float) else phi

        return np.ones(len(phi))

    def __repr__(self):

        output = self.class_name + "(%d)" % self.M

        return output

    def integrate(self, x):

        return 0.

    def maximize(self, x):

        def my_func(phi):

            return -self.__call__(phi, x)[0]

        opt = minimize(my_func, 0.5, bounds=[(0, 1)])

        return opt.x, -opt.fun

    @classmethod
    def from_yaml(cls, filename):

        with open(filename, "r") as stream:
            data = yaml.safe_load(stream)

        my_model = eval(data["profileModel"]["name"])

        return my_model

    def to_yaml(self, filename, append=True):

        data = {
            "profileModel": {
                "name": str(self)
            }
        }

        if append and os.path.exists(filename):
            with open(filename, "r") as stream:
                old = yaml.safe_load(stream)
            if old:
                data.update(old)

        with open(filename, "w") as stream:
            yaml.safe_dump(data, stream)

    def plot(self, x):

        self.check_input_pars(x)

        fig, axes = plt.subplots(1, 1, figsize=(10, 8))
        axes.set_xlabel("phi")
        axes.set_ylabel("normalized rate")

        phi = np.linspace(0, 2, 1000)[:-1]
        y = self.__call__(phi, x)
        axes.plot(phi, y)
        axes.set_xlim(0, 2)
        axes.set_ylim(0, )

        return fig, axes


class Stepwise(ProfileModel):
    """
    A models for a pulse profile parameterized as a stepwise function with M
    bins.

    Attributes:
        parameters (list): names of the glitch parameters
        M (int): number of components

    Parameters:
        FRAC (float): bin counts
    """

    def __init__(self, M):

        super().__init__()
        self.M = M
        for ri in range(M):
            self.parameters.append("FRAC_%02d" % ri)

    def __call__(self, phi, x):

        phi = np.array([phi]) if isinstance(phi, float) else phi
        self.check_input_pars(x)

        phi = fold_phases(phi)
        bi = np.array((phi * self.M) // 1, dtype=int)
        pp = x[bi].ravel()

        return pp

    def integrate(self, x):

        self.check_input_pars(x)

        return np.sum(x)


class MultipleGaussian(ProfileModel):
    """
    A models for a pulse profile parameterized as the sum of M Gaussian peaks and
    a constant unpulsed component.

    Attributes:
        parameters (list): names of the glitch parameters
        M (int): number of Gaussian peaks

    Parameters:
        PHI0 (float): position of the Gaussian peak
        SIGMA (float): half width of the Gaussian peak
        FRAC (float): counts from one component
    """

    def __init__(self, M):
        super().__init__()
        self.M = M
        for ri in range(M):
            self.parameters.append("PHI0_%02d" % (ri))
            self.parameters.append("SIGMA_%02d" % (ri))
        for ri in range(M):
            self.parameters.append("FRAC_%02d" % (ri))
        self.parameters.append("FRAC_CNST")

    def __call__(self, phi, x):
        phi = np.array([phi]) if isinstance(phi, float) else phi
        self.check_input_pars(x)

        phi = fold_phases(phi) - np.arange(-3, 5, 1).reshape((1, 8, 1))

        ci = np.arange(self.M)
        x0 = x[2 * ci].reshape((self.M, 1, 1))
        norm = (x[-1 - self.M + ci] / np.sqrt(2 * np.pi) / x[
            2 * ci + 1]).reshape((self.M, 1, 1))
        sigma = x[2 * ci + 1].reshape((self.M, 1, 1))

        pp = norm * np.exp(-0.5 * (phi - x0) ** 2 / sigma ** 2)
        pp = np.sum(pp, axis=1)
        pp = np.sum(pp, axis=0)
        pp += x[-1]

        '''
        phi = fold_phases(phi)
        pp = 0. * phi + x[-1]
        for ci in range(self.M):
            norm = x[-1-self.M+ci] / np.sqrt(2 * np.pi) / x[2*ci+1]
            component = norm * np.exp(-0.5 *
                            (phi - np.arange(-3, 5, 1).reshape((8, 1)) - x[2*ci])**2 /
                            x[2*ci+1]**2)
            pp += component.sum(axis=0)
        '''

        return pp

    def integrate(self, x):

        self.check_input_pars(x)

        return np.sum(x[-self.M - 1:])


class MultipleGaussian2(MultipleGaussian):
    """
    A models for a pulse profile parameterized as the sum of M Gaussian peaks and
    a constant unpulsed component.

    Attributes:
        parameters (list): names of the glitch parameters
        M (int): number of Gaussian peaks

    Parameters:
        PHI0 (float): position of the first Gaussian peak, or positions of the
                      following Gaussians relative to the first peak
        SIGMA (float): half width of the Gaussian peak
        FRAC (float): counts from one component
    """

    def __call__(self, phi, x):

        x2 = x.copy()
        x2[2 * np.arange(1, self.M)] += x[0]

        return super().__call__(phi, x2)


'''
class MultipleGaussian2(ProfileModel):
    """
    A models for a pulse profile parameterized as the sum of M Gaussian peaks and
    a constant unpulsed component.

    Attributes:
        parameters (list): names of the glitch parameters
        M (int): number of Gaussian peaks

    Parameters:
        PHI0 (float): position of the first Gaussian peak, or positions of the
                      following Gaussians relative to the first peak
        SIGMA (float): half width of the Gaussian peak
        FRAC (float): counts from one component
    """

    def __init__(self, M):
        super().__init__()
        self.M = M
        for ri in range(M):
            self.parameters.append("PHI0_%02d" % (ri))
            self.parameters.append("SIGMA_%02d" % (ri))
        for ri in range(M):
            self.parameters.append("FRAC_%02d" % (ri))
        self.parameters.append("FRAC_CNST")

    def __call__(self, phi, x):

        phi = np.array([phi]) if isinstance(phi, float) else phi
        self.check_input_pars(x)

        phi = fold_phases(phi) - np.arange(-3, 5, 1).reshape((1, 8, 1))

        ci = np.arange(self.M)
        x0 = x[0] + x[2 * ci]
        x0[0] = x[0]
        x0 = x0.reshape((self.M, 1, 1))
        norm = (x[-1-self.M+ci] / np.sqrt(2 * np.pi) / x[2*ci+1]).reshape((self.M, 1, 1))
        sigma = x[2*ci+1].reshape((self.M, 1, 1))

        pp = norm * np.exp(-0.5 * (phi - x0)**2 / sigma**2)
        pp = np.sum(pp, axis=1)
        pp = np.sum(pp, axis=0)
        pp += x[-1]

        
        pp = 0. * phi + x[-1]
        for ci in range(self.M):
            delta = 0. if ci == 0 else x[2 * ci]
            norm = x[-1-self.M+ci] / np.sqrt(2 * np.pi) / x[2*ci+1]
            component = norm * np.exp(-0.5 *
                (phi - np.arange(-3, 5, 1).reshape((8, 1)) - (x[0] + delta))**2 /
                x[2*ci+1]**2)
            pp += component.sum(axis=0)
        

        return pp

    def integrate(self, x):

        self.check_input_pars(x)

        return np.sum(x[-self.M - 1:])
'''