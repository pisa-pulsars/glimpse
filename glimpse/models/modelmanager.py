from abc import ABC
import os

import yaml
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import comb
from cpnest.model import Model as CPNestModel
from cpnest.parameter import LivePoint

from glimpse.utils import fold_phases
from glimpse.models.base import BaseModel
from glimpse.models.timing import TimingModel
from glimpse.models.profile import ProfileModel, MultipleGaussian
from glimpse.models.components import SpinDown, PermanentChange, TransientChange


DEFAULT_BOUNDS = {
    "PH": [-1.e-6, 1.],
    "F0": [1., 1000.],
    "F1": [-1.e-8, 1.e-8],
    "F2": [-1.e-20, 1.e-20],
    "GLEP": [-100., 100.],
    "GLPH": [-1., 1],
    "GLF0": [-1.e-2, 1.e-2],
    "GLF1": [-1.e-9, 1.e-9],
    "GLTD": [0., 5000.],
    "NORM": [0., 1.e4],
    "SIGM": [1e-6, 2.],
    "PHI0": [0., 1.],
    "FRAC": [-1.e-6, 10000.]
}

DEFAULT_VALUES = {
    "PH": 0.,
    "F0": 1.,
    "F1": 0.,
    "F2": 0.,
    "GLEP": 0.,
    "GLPH": 0.,
    "GLF0": 0.,
    "GLF1": 0.,
    "GLTD": 4999.,
    "NORM": 1000.,
    "SIGM": .1,
    "PHI0": .5,
    "FRAC": 10.
}


class ModelManager(CPNestModel, BaseModel, ABC):
    """
    This class contains photon arrival times and a joint models for pulsar
    timing and profile. It inherits from `glimpse.models.base.BaseModel` to handle
    parameters. The class also inherits from `cpnest.models.CPNestModel` to allow
    parameter estimation via nested sampling.

    Attributes:
        T (float): the total time span of the observation
        dt (float): the bin size for unbinned likelihood
        timing_model (glimpse.model.timing.TimingModel): an instance of pulsar timing models
        profile_model (glimpse.models.profileProfileModel): an instance of pulse profile models
        outDir (str): the path of the output folder (for plots)
        free (list): boolean mask indicating free parameters
        all_bounds (): list of 2-elements tuples indicating the boudaries of each parameter
        parameters (list): list of parameter names
        default (list): default values for models parameters
    """

    def __init__(self):

        super(BaseModel, self).__init__()
        self._photon_times = None
        self._weights = None
        self._sorted_idx = None
        self.T = 0.
        self.dt = 0.
        self.timing_model = TimingModel()
        self.profile_model = ProfileModel()
        self.outDir = "./"

        self.free = []
        self.all_bounds = []
        self.parameters = []
        self.default = []

        self._refresh()

    def __str__(self):

        outstr = ""
        for i in range(len(self.parameters)):
            outstr += "%s %s %s %s\n" % \
                      (self.parameters[i], self.all_bounds[i],
                       self.free[i], self.default[i])

        return outstr

    def load_photon_times(self, photon_times):
        """
        Loads a list of photon arrival time into the cache.

        Args:
            photon_times (np.array): list of photon arrival times
        """

        self._sorted_idx = np.argsort(photon_times)
        self._photon_times = photon_times.copy()[self._sorted_idx]
        self.T = np.max(self._photon_times) - np.min(self._photon_times)
        self.dt = np.min(self._photon_times[1:] - self._photon_times[:-1])

    def get_photon_times(self):
        """
        Returns the photon times.
        """

        return self._photon_times

    def load_weights(self, weights):
        """
        Loads a list of photon statistical weights into the cache.

        Args:
            weights (np.array): list of photon weights
        """

        self._weights = weights.copy()[self._sorted_idx]

    def get_weights(self):
        """
        Returns the weights.
        """

        return self._weights

    def set_timing_model(self, model):
        """
        Set the input timing models and updates parameter list.

        Args:
            model (glimpse.model.timing.TimingModel): an instance of pulsar timing models
        """

        if not isinstance(model, TimingModel):
            raise TypeError(
                "The input must be an object of class TimingModel.")

        self.timing_model = model
        self._refresh()
    
    def set_pulse_profile(self, model):
        """
        Set the input profile models and updates parameter list.

        Args:
            model (glimpse.model.profile.ProfileModel): an instance of pulse profile models
        """

        if not isinstance(model, ProfileModel):
            raise TypeError(
                "The input must be an object of class ProfileModel.")

        self.profile_model = model
        self._refresh()

    def set_parameter_bounds(self, parname, bounds):
        """
        Sets the bounds of a chosen parameter.

        Args:
            parname (str): parameter name for which the boundaries will be updated
            bounds (tuple): new parameter boundaries
        """

        self.check_parname(parname)
        idx = np.where(np.array(self.parameters) == parname)[0][0]
        self.all_bounds[idx] = bounds

    def set_parameter_free(self, parname, free):
        """
        Sets a chosen parameter free or fixed.

        Args:
            parname (str): name of the parameter to set free or fixed
            free (bool): set free (True) or fixed (False)
        """

        self.check_parname(parname)
        idx = np.where(np.array(self.parameters) == parname)[0][0]
        self.free[idx] = free

    def set_parameter_default(self, parname, value):
        """
        Sets the default value of a chosen parameter.

        Args:
            parname (str): parameter name for which the default value will be updated
            value (float): the value of the parameter
        """

        self.check_parname(parname)
        idx = np.where(np.array(self.parameters) == parname)[0][0]
        self.default[idx] = value

    @classmethod
    def from_yaml(cls, filename):
        """
        This method allows to define a joint models using a YAML conf file. For
        an example YAML file see templates/modelManager.yaml.

        Args:
            filename (str): path to the YAML conf file

        Returns:
            glimpse.models.modelmanager.Modelmanager
        """

        # Load models
        my_model = ModelManager()
        my_model.set_timing_model(
            TimingModel.from_yaml(filename)
        )
        my_model.set_pulse_profile(
            ProfileModel.from_yaml(filename)
        )

        # Open conf file
        with open(filename, "r") as stream:
            try:
                config = yaml.safe_load(stream)["modelManager"]
            except KeyError:
                config = {}

        # Update outdir
        if "outDir" in config:
            my_model.outDir = config["outDir"]

        # Update parameter specs (default, free, bounds)
        spec_list = []
        if "parameterSpec" in config:
            spec_list = np.append(spec_list, config["parameterSpec"])
        for item in spec_list:
            parname = item["name"]
            if "default" in item:
                my_model.set_parameter_default(parname, item["default"])
            if "free" in item:
                my_model.set_parameter_free(parname, item["free"])
            if "bounds" in item:
                my_model.set_parameter_bounds(parname, item["bounds"])

        return my_model

    def to_yaml(self, filename):
        """

        Args:
            filename (str): path to the YAML conf file
        """

        self.timing_model.to_yaml(filename, append=False)
        self.profile_model.to_yaml(filename)

        data = {
            "modelManager": {
                "parameterSpec": [],
                "outDir": self.outDir
            }
        }

        for parname in self.parameters:
            idx = np.where(np.array(self.parameters) == parname)[0][0]
            entry = {
                "name": parname,
                "default": float(self.default[idx]),
                "free": self.free[idx],
                "bounds": [
                    float(self.all_bounds[idx][0]),
                    float(self.all_bounds[idx][1])
                ]
            }
            data["modelManager"]["parameterSpec"].append(entry)

        with open(filename, "r") as stream:
            old = yaml.safe_load(stream)
            if old:
                data.update(old)

        with open(filename, "w") as stream:
            yaml.safe_dump(dict(data), stream)

    def _refresh(self):
        """
        Hidden methods that updates the parameter list and attributes.
        """

        # Copy old parameters and init new parameter list
        old_parameters = self.parameters.copy()
        new_parameters = np.concatenate([self.timing_model.parameters,
             self.profile_model.parameters]).tolist()

        # Drop obsolete parameters
        if len(new_parameters) < len(old_parameters):
            diff = set(old_parameters).difference(new_parameters)
            for parname in diff:
                print("Dropped parameter %s" % parname)

        # Keep common parameters or add new ones
        new_free, new_all_bounds, new_default = [], [], []
        for parname in new_parameters:
            if parname in old_parameters:
                idx = np.where(
                    np.array(old_parameters) == parname)[0][0]
                new_free.append(self.free[idx])
                new_all_bounds.append(self.all_bounds[idx])
                new_default.append(self.default[idx])
            else:
                print("Added new parameter %s" % parname)
                new_free.append(True)
                new_all_bounds.append(DEFAULT_BOUNDS[parname[:4]])
                new_default.append(DEFAULT_VALUES[parname[:4]])

        # Set attributes
        self.parameters = new_parameters
        self.free = new_free
        self.all_bounds = new_all_bounds
        self.default = new_default

    def check_input_pars(self, x):
        """
        Method that checks the input type and converts CPNEst LivePoints to
        numpy arrays.

        Args:
            x: (LivePoint, np.ndarray): parameter values

        Returns:
            np.ndarray: array of parameter values
        """

        if isinstance(x, LivePoint):
            y = []
            for parname in self.parameters:
                if parname in self.names:
                    idx = np.where(np.array(self.names) == parname)[0][0]
                    y.append(x.values[idx])
                else:
                    idx = np.where(np.array(self.parameters) == parname)[0][0]
                    y.append(self.default[idx])
            y = np.array(y)
        else:
            y = x.copy()

        return y

    def get_phases(self, x):
        """
        Uses the cached data and the timing models to compute rotational phases.

        Args:
            x: (np.ndarray): parameter values

        Returns:
            np.ndarray: rotational phasaes
        """

        self.check_input_pars(x)
        t = self._photon_times.copy()
        x_sub = x[:len(self.timing_model.parameters)]
        phi = self.timing_model(t, x_sub)[0]
        phi = fold_phases(phi)

        return phi

    def log_prior(self, x):
        """
        Implements the logarithm of the prior as required by CPNest.

        Args:
            x (LivePoint, np.ndarray): parameter values

        Returns:
            float: the logarithm of the prior computed at the chosen point

        So far the prior is always uniform.
        """

        y = self.check_input_pars(x)
        in_bounds = [self.all_bounds[bi][0] < y[bi] < self.all_bounds[bi][1]
                     for bi in range(len(self.all_bounds))]
        logP = 0. if np.all(in_bounds) else -np.inf

        return logP

    def log_likelihood(self, x):
        """
        Implements the logarithm of the unbinned likelihood as required by
        CPNest.

        Args:
            x (LivePoint, np.ndarray): parameter values

        Returns:
            float: the logarithm of the likelihood computed at the chosen point
        """

        y = self.check_input_pars(x)

        phi = self.get_phases(y)
        fi = self.profile_model(phi, y[-self.profile_pars:])
        loglike = np.sum(np.log(fi) * self._weights) \
            - self.profile_model.integrate(y[-self.profile_pars:])

        return loglike

    @property
    def names(self):
        """
        Returns the names of the free parameters.
        """

        return np.array(self.parameters)[self.free].tolist()

    @property
    def bounds(self):
        """
        Returns the boundaries of the free parameters.
        """

        return np.array(self.all_bounds)[self.free].tolist()

    @property
    def N(self):
        """
        Returns the length of the data.
        """

        return len(self._photon_times)

    @property
    def W(self):
        """
        Returns the total weighted counts.
        """

        return np.sum(self._weights) if self._weights is not None else self.N

    @property
    def profile_pars(self):
        """
        Returns the number of profile parameters.
        """

        return len(self.profile_model.parameters)

    @property
    def timing_pars(self):
        """
        Returns the number of timing parameters.
        """

        return len(self.timing_model.parameters)

    def ockham(self, n_bins, x):
        """
        Estimate the Ockham factor as in Gregory and Loredo, 1992.

        Args:
            n_bins (int): number of bins in the stepwise models
            x (LivePoint, np.ndarray): parameter values

        Returns:

        """

        self.check_input_pars(x)
        phi = self.get_phases(x)

        n_j, _ = np.histogram(phi, bins=n_bins, weights=self._weights)

        log_w = np.log(np.arange(self.W) + 1).sum()
        for n in n_j:
            log_w -= np.log(np.arange(n) + 1).sum()
        ockham = self.W * np.log(n_bins) - log_w - np.log(
            comb(self.W + n_bins + 1, self.W))

        return np.exp(ockham)

    def estimate_optimal_bins(self, x, plot=True):
        """
        Searches the optimal number of bins m that maximizes the Ockham factor.

        Args:
            x (LivePoint, np.ndarray): parameter values
            plot (bool): if True, plot the results of the search

        Returns:
            np.ndarray: the tested values of m
            np.ndarray: the Ocham factor at each test point
        """

        ockham = 0.
        values = []
        trials = np.arange(2, 31)
        for m in trials:
            ockham_new = self.ockham(m, x)
            values.append(ockham_new)
            if ockham_new > ockham:
                ockham = ockham_new
            m += 1

        if plot:
            phi = self.get_phases(x)
            fig, axes = plt.subplots(2, 1)
            axes[0].plot(trials, values, color="darkblue", lw=1)
            axes[0].set_ylabel(r"$O_{m1}$")
            axes[0].set_xlabel("m")
            axes[0].set_xticks(trials)

            m_opt = trials[np.argmax(values)]
            wi = self._weights if self._weights is not None \
                else np.ones(len(self._photon_times))
            counts, edges, _ = axes[1].hist(np.append(phi, phi + 1.),
                                            bins= m_opt * 2, histtype="step",
                                            color="darkblue",
                                            weights=np.repeat(wi, 2))
            axes[1].errorbar((edges[1:] + edges[:-1]) * 0.5, counts,
                             yerr=np.sqrt(counts), marker="", ls="",
                             color="darkblue")
            axes[1].set_ylabel("counts / bin")
            axes[1].set_xlabel("rotational phase")
            axes[1].set_xlim(0., 2.)
            plt.tight_layout()
            png_filename = os.path.join(self.outDir, "optimal_phaseogram.png")
            fig.savefig(png_filename)
            plt.clf()

        return trials, np.array(values)
