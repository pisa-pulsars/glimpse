import numpy as np
import matplotlib.pyplot as plt
import yaml

from glimpse.models.base import BaseModel
from glimpse.utils import DAY_TO_SECS, F_1YEAR


class TimingNoise(BaseModel):

    def __init__(self):

        super().__init__()
        self.parameters = []

    def __call__(self, f, x):

        return np.zeros(len(f))

    def sample(self, x, dt_secs, fcut_lo=None, fcut_hi=None, seed=None):

        # We assume a sampling frequency Fs of 1 Hz (ok for long-timescale noise)
        # and a Nyquist frequency of 1 rotation per observation
        # The number of samples in the observation is
        N = int(dt_secs)

        # N must be even, because it must be distributed evenly
        # between positive and negative frequencies
        odd = N % 2 != 0
        if odd:
            N += 1

        # We generate N/2 positive frequencies in [0., Fs-Fnyq]
        # and N/2 negative frequencies in [-Fs, -Fnyq]
        freqs = np.fft.fftfreq(N)
        freqs[0] = 1e-10  # The power law is not defined in 0

        # We compute the PSD at the N frequencies and
        Pk = self(freqs, x)

        # The Fourier transform of the noise is
        # the square root of PSD * N * Fs
        #Fk = (Pk * N)**0.5

        # We apply cuts by setting Fk=0 outside the boundaries
        if fcut_lo is not None:
            Pk[np.abs(freqs) <= fcut_lo] = 0
        if fcut_hi is not None:
            Pk[np.abs(freqs) > fcut_hi] = 0

        # The coefficients of the Fourier transform are sqrt(Pk/2)
        # (we divide by two to normalize the two-sided PSD)
        ak = (0.5 * Pk)**0.5

        # We generate N random phases in [0, 2pi]
        rng = np.random.default_rng(seed)
        phi = 2 * np.pi * rng.random(N)

        # The phases are added as exp(-j phi) so that
        # the real part of the inverse transform is cos(wt + phi)
        Fk = ak * np.exp(-1j * phi)

        # We apply the inverse discrete Fourier transform
        x = np.fft.ifft(Fk)

        # We return the N coefficients of cos(wt + phi)
        if odd:
            N -= 1
        return x[:N]

    def plot(self, x, dt_secs, fmin=None, fmax=None):

        if fmin is None:
            fmin = F_1YEAR
        if fmax is None:
            fmax = 1.

        fig, axes = plt.subplots(2, 1, figsize=(8, 6))
        fig.suptitle(self.__repr__()[:-2] + str(x), size="medium")

        n_bins = int(dt_secs)
        bin_size = 1. / dt_secs

        odd = n_bins % 2 != 0
        if odd:
            n_bins += 1
        f = np.fft.fftfreq(n_bins) #* bin_size
        f[0] = 1e-10
        f = f[:len(f)//2]

        psd = self.__call__(f, x)
        axes[0].plot(f, psd)
        axes[0].set_xlim(fmin, fmax)
        axes[0].set_xlabel("f (Hz)")
        axes[0].set_ylabel("PSD")
        axes[0].set_xscale("log")
        axes[0].set_yscale("log")

        phases = self.sample(x, dt_secs)
        axes[1].plot(phases)
        axes[1].set_xlabel(r"seconds since t$_0$")
        axes[1].set_ylabel(r"$\Delta \phi$")

        return fig

    @classmethod
    def from_yaml(cls, filename):

        with open(filename, "r") as stream:
            data = yaml.safe_load(stream)

        my_model = eval(data["noiseModel"]["name"])

        return my_model


class PowerLawTN(TimingNoise):

    def __init__(self):

        super().__init__()
        self.parameters = ["AMPL", "BETA"]

    def __call__(self, f, x):

        self.check_input_pars(x)
        psd = x[0] * np.abs(np.abs(f) / F_1YEAR) ** x[1]
        psd[0] = 0

        return psd


class CornerPowerLawTN(TimingNoise):

    def __init__(self):

        super().__init__()
        self.parameters = ["AMPL", "BETA", "FCORN"]

    def __call__(self, f, x):

        self.check_input_pars(x)

        # rescaling so that x[0] = PSD(F_1YEAR)
        #scale = x[0] / (1 + (x[2] / F_1YEAR)**(0.5 * x[1]))**2
        scale = x[0] * (1 + (F_1YEAR / x[2]) ** (-0.5 * x[1])) ** 2
        #scale = x[0] * (x[2] / F_1YEAR)**(-x[1])
        psd = scale / (1. + (np.abs(f) / x[2]) ** (-0.5 * x[1]))**2

        return psd

