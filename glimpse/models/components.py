import numpy as np

from glimpse.models.base import BaseModel
from glimpse.utils import DAY_TO_SECS, mjd_to_met, taylor


class Component(BaseModel):
    """
    Class for timing models components. This should be inherited by child
    component classes.

    Attributes:
        parameters (list): list of parameter names

    The `__call__` method is a function of `t` (`float`, `np.ndarray`), the time in
    seconds measured with respect to PEPOCH, and `x` (`list`), the values of the
    models parameters. The `__call__` method returns an array of shape (3, n) with
    the values of phase, frequency and frequency derivative at the n input times.
    """

    def __call__(self, t, x):

        t = np.array([t]) if isinstance(t, float) else t

        return np.zeros(len(t))


class SpinDown(Component):
    """
    Class for the pulsar SpinDown component. This component is defined as a
    Taylor series of the rotational phase around t=0:

    phi(t) = PH + F0 * t + 1/2! * F1 * t^2 + ...

    Args:
        n_terms (int): order of the Taylor series

    Attributes:
        parameters (list): names of the Taylor coefficients

    Parameters:
        PH (float): absolute rotational phase offset
        F0 (float): rotational frequency at t=0 in Hz
        F1 (float): derivative of the frequency at t=0 in Hz/s
        F2 (float): 2-nd derivative of the frequency at t=0 in Hz/s^2
        ...
    """

    def __init__(self, n_terms=2):

        super().__init__()
        self.parameters = ["PH"]
        for ti in range(n_terms):
            self.parameters.append("F%d" % ti)

    def __call__(self, t, x):

        self.check_input_pars(x)
        pt = taylor(t, x)

        return pt[:3]

    def __repr__(self):

        output = self.class_name + "(%d)" % (len(self.parameters) - 1)

        return output


class PermanentChange(Component):
    """
    Class for a rotational glitch modeled as a permanent change. This component
    adds a constant to phase, frequency and frequency derivative at t > GLEP.

    Attributes:
        parameters (list): names of the glitch parameters

    Parameters:
        GLEP (float): glitch epoch in days since PEPOCH
        GLPH (float): amplitude of the phase shift at GLEP
        GLF0 (float): amplitude of the frequency change at GLEP in Hz
        GLF1 (float): amplitude of the frequency derivative change at GLEP in Hz/s
    """

    def __init__(self):

        super().__init__()
        self.parameters = ["GLEP", "GLPH", "GLF0", "GLF1"]

    def __call__(self, t, x):

        self.check_input_pars(x)

        t_glitch = x[0] * DAY_TO_SECS
        t = t - t_glitch
        apply = t >= 0.
        pt = taylor(t, np.array(x[1:]))
        pt = np.where(apply, pt, 0.)

        return pt


class TransientChange(Component):
    """
    Class for a rotational glitch modeled as a transient change in F0. This
    component adds a term to the frequency at t > GLEP, which decays
    exponentially.

    Attributes:
        parameters (list): names of the glitch parameters

    Parameters:
        GLEP (float): glitch epoch in days since PEPOCH
        GLTD (float): glitch decay time in days
        GLF0D (float): amplitude of the frequency change at GLEP in Hz
    """

    def __init__(self):

        super().__init__()
        self.parameters = ["GLEP", "GLTD", "GLF0D"]

    def __call__(self, t, x):

        self.check_input_pars(x)

        t_glitch = x[0] * DAY_TO_SECS
        t_decay = x[1] * DAY_TO_SECS
        t = (t - t_glitch) / t_decay
        apply = t >= 0.

        pt = [
            t_decay * (1 - np.exp(-t)),
            np.exp(-t),
            - 1. / t_decay * np.exp(-t)
            ]
        pt = np.where(apply, x[2] * np.array(pt), 0.)

        return pt
