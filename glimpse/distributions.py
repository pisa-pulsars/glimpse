import numpy as np
from numpy.linalg import inv, det
from scipy.special import gamma
import matplotlib.pyplot as plt


class Distribution:

    def __init__(self):

        self._name = "Distribution"

    def __repr__(self):

        return self._name


class UnivariateDistribution(Distribution):

    def __init__(self):

        super().__init__()
        self._name = "UnivariateDistribution"

    def random(self, n=1):

        sample = np.array([])
        m = min(10000, n*10)
        while len(sample) < n:
            x = np.random.random(m) * (self.suplim - self.inflim) + self.inflim
            f = self.__call__(x)
            r = np.random.random(m)
            accepted = x[r < f]
            sample = np.concatenate([sample, accepted])

        if n == 1:
            return sample[0]
        else:
            return sample[:n]

    def plot(self, xrange=None):

        if xrange is None:
            xrange = [self.inflim, self.suplim]
        x = np.linspace(*xrange, 100)
        y = self.__call__(x)
        ax = plt.plot(x, y)
        return ax


class Uniform(UnivariateDistribution):

    def __init__(self, inflim=0., suplim=1.):

        super().__init__()
        self.inflim = inflim
        self.suplim = suplim
        self._name = "Uniform(%.2e, %.2e)" % (self.inflim, self.suplim)
        self.scale = 1. / self.suplim - self.inflim

    def __call__(self, x):

        p = np.where((x >= self.inflim) * (x < self.suplim), np.ones(len([x])), np.zeros(len([x])))
        p *= self.scale

        return p

    def random(self, n=1):

        sample = np.random.uniform(low=self.inflim, high=self.suplim, size=n)

        return sample


class Inverse(UnivariateDistribution):

    def __init__(self, inflim=0., suplim=1.):

        super().__init__()
        self.inflim = inflim
        self.suplim = suplim
        self._name = "Inverse(%.2e, %.2e)" % (self.inflim, self.suplim)
        self.scale = np.log(self.suplim / self.inflim)

    def __call__(self, x):

        p = np.where((x >= self.inflim) * (x < self.suplim), np.ones(len([x])) / x, np.zeros(len([x])))
        p *= self.scale

        return p


class Gaussian(UnivariateDistribution):

    def __init__(self, mu=0., sigma=1.):

        super().__init__()
        self.mu = mu
        self.sigma = sigma
        self._name = "Gaussian(%.2e, %.2e)" % (self.mu, self.sigma)
        self.scale = 1 / (sigma * np.sqrt(2 * np.pi))
        self.suplim = mu + sigma * 10.
        self.inflim = mu - sigma * 10.

    def __call__(self, x):

        p = self.scale * np.exp(-0.5 * (x - self.mu)**2 / self.sigma**2)

        return p

    def random(self, n=1):

        sample = np.random.normal(loc=self.mu, scale=self.sigma, size=n)

        return sample


class Triangular(UnivariateDistribution):

    def __init__(self, a=0., b=1., c=0.5):

        super().__init__()
        self.a = a
        self.b = b
        self.c = c
        self.name = "Triangular(%.2e, %.2e, %.2e)" % (self.a, self.b, self.c)
        self.scale = 2 / (self.b - self.a)
        self.suplim = b
        self.inflim = a

    def __call__(self, x):

        p = np.zeros(len([x]))
        if self.c > self.a:
            p = np.where((x >= self.a) * (x <= self.c), (x - self.a) / (self.c - self.a), p)
        if self.c < self.b:
            p = np.where((x >= self.c) * (x <= self.b), (self.b - x) / (self.b - self.c), p)
        p *= self.scale

        if len(p) == 1:
            return p[0]
        else:
            return p

    def random(self, n=1):

        sample = np.random.triangular(left=self.a, mode=self.c, right=self.b, size=n)

        return sample


class Gamma(UnivariateDistribution):

    def __init__(self, k=1., theta=1.):

        super().__init__()
        self.k = k
        self.theta = theta
        self._name = "Gamma(%.2e, %.2e)" % (self.k, self.theta)
        self.scale = 1. / (gamma(self.k) * self.theta ** self.k)
        self.suplim = self.k * self.theta * (1. + 10 * self.theta)
        self.inflim = 0.

    def __call__(self, x):

        p = np.where(x > 0, (x ** (self.k - 1) * np.exp(-x / self.theta)), np.zeros(len([x])))
        p *= self.scale

        if len(p) == 1:
            return p[0]
        else:
            return p

    def random(self, n=1):

        sample = np.random.gamma(shape=self.k, scale=self.theta, size=n)

        return sample


class Moffat(UnivariateDistribution):

    def __init__(self, alpha=1., beta=2.):

        super().__init__()
        self.alpha = alpha
        self.beta = beta
        self._name = "Moffat(%s, %s)" % (self.alpha, self.beta)
        self.suplim = 100.
        self.inflim = 0.
        self.scale = 1.

    def __call__(self, x):

        p = (1 + x**2/self.alpha**2)**(-self.beta)

        return p


class MultivariateDistribution(Distribution):

    def __init__(self):

        super().__init__()
        self._name = "MultivariateDistribution"


class Dirichlet(MultivariateDistribution):

    def __init__(self, alpha=[1., 1., 1.]):

        super().__init__()
        self.alpha = np.array(alpha)
        self._name = "Dirichlet(%s)" % self.alpha
        self.K = len(alpha)
        self.scale = gamma(self.alpha.sum()) / np.prod(gamma(self.alpha))
        self.suplim = 1.
        self.inflim = 0.

    def __call__(self, x):

        if len(x.shape) == 1:
            x = np.array([x])

        p = np.zeros(x.shape[0])
        for j in range(x.shape[0]):
            mask = (x[j] < 0) + (x[j] > 1)
            if np.sum(mask) == 0:
                p[j] = self.scale * np.prod(x[j] ** (np.array(self.alpha) - 1))

        return p

    def random(self, n=1):

        sample = []
        while len(sample) < n:
            y = np.zeros(self.K)
            for i in range(self.K):
                gamma_distr = Gamma(k=self.alpha[i], theta=1)
                y[i] = gamma_distr.random()
            x = y / y.sum()
            f = self.__call__(x)
            r = np.random.random() * self.scale
            if r < f:
                sample.append(x)
        sample = np.array(sample)

        return sample


class SymmetricDirichlet(Dirichlet):

    def __init__(self, K=10, alpha=1.):

        super().__init__(alpha=np.ones(K) * alpha)
        self._name = "SymmetricDirichlet(%d, %.2e)" % (self.K, alpha)


class MultivariateGaussian(MultivariateDistribution):

    def __init__(self, mu=[0., 0.], sigma=[[1., 0.], [0., 1.]]):

        super().__init__()
        self.mu = np.array(mu)
        self.sigma = np.array(sigma)
        self._name = "MultivariateGaussian(%s, %s)" % (self.mu, self.sigma)
        self.K = len(mu)
        self.scale = 1. / np.sqrt((2 * np.pi)**self.K * det(sigma))
        self.suplim = self.mu + 10 * np.sqrt(np.sum(self.sigma, axis=1))
        self.inflim = self.mu - 10 * np.sqrt(np.sum(self.sigma, axis=1))

    def __call__(self, x):

        if len(x.shape) == 1:
            x = np.array([x])

        p = np.zeros(x.shape[0])
        for j in range(x.shape[0]):
            delta_x = x[j] - self.mu
            p[j] = self.scale * np.exp(-0.5 * np.sum(delta_x * np.matmul(inv(self.sigma), delta_x)))

        return p

    def random(self, n=1):

        sample = np.random.multivariate_normal(mean=self.mu, cov=self.sigma, size=n)

        return sample
