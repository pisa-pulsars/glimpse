import json
import os
from os.path import join, exists
import shutil
from datetime import datetime, timezone

import numpy as np
import pandas as pd

from glimpse.config import DATETIME_FORMAT, GLIMPSE_PERIODICITY_DIR
from glimpse.sources import FermiPulsar
from glimpse.utils import LAT_StartMissionMJD, met_to_mjd

PERIODICITY_DEFAULT = {
    "timeWindow": 30.,
    "slidingTime": 0.,
    "nF0": 500.,
    "nF1": 200.,
    "resultsFilename": "",
    "roiCuts": {
        "method": "cookie",
        "radius": 5.,
        "emin": 100.,
        "emax": 300000.
    },
    "weights": {
        "method": "simple",
        "mu_w": 3.
    }
}

GLITCH_SEARCH_DEFAULT = {
    "setup": 0,
    "n_sample": 50000,
    "gamma": 0.1,
    "scale_min": 0.01,
    "scale_max": 100.,
    "skip": 10
}


class PulsarMonitor:

    def __init__(self, index_filename="./pulsar_index.json"):
        self.index_filename = index_filename
        self.index_dir = os.path.dirname(index_filename)
        if exists(index_filename):
            print("Reading from " + index_filename + "...")
            with open(self.index_filename, "r") as jf:
                index = json.from_json(jf)
            self.psr_dict = index
        else:
            print("Creating from scratch...")
            self.psr_dict = {}

    def get_pulsar(self, psr_name):

        if psr_name not in self.psr_dict:
            print("PSR %s not in index." % psr_name)
            return {}
        else:
            return self.psr_dict[psr_name]

    def add_pulsar(self, pulsar, tmin_mjd=0., tmax_mjd=0, ft1_filename=None, overwrite=False):

        if isinstance(pulsar, FermiPulsar):
            if pulsar.PSRJ in self.list_pulsars() and not overwrite:
                print(pulsar.PSRJ + " already exists in the catalog. Pulsar not added.")
            else:
                print("Adding PSR %s." % pulsar.PSRJ)
                info_dict = {
                    "F0": pulsar.F0,
                    "F1": pulsar.F1,
                    "F2": pulsar.F2,
                    "PEPOCH": pulsar.PEPOCH
                }
                for key in ["RAJ", "DECJ"]:
                    info_dict[key] = float(getattr(pulsar, key))

                # not sure if the following lines are required
                if tmin_mjd == 0.:
                    tmin_mjd = LAT_StartMissionMJD + .001
                if tmax_mjd == 0.:
                    tmax_mjd = LAT_StartMissionMJD + .001
                info_dict["TSTART"] = tmin_mjd
                info_dict["TSTOP"] = tmax_mjd

                if ft1_filename is None:
                    ft1_filename = os.path.join(GLIMPSE_PERIODICITY_DIR,
                        pulsar.PSRJ, pulsar.PSRJ + "_data_mktime_bary.fits")
                info_dict["ft1_filename"] = ft1_filename

                for key in info_dict:
                    print("  %s set to " % key + str(info_dict[key]) + ".")
                info_dict["setups"] = {}
                self.psr_dict[pulsar.PSRJ] = info_dict

                subdir = join(self.index_dir, pulsar.PSRJ)
                if not exists(subdir):
                    os.mkdir(subdir)

                self.add_setup(pulsar.PSRJ)

        else:
            raise TypeError("Input is not a FermiPulsar instance.")

    def edit_pulsar(self, psr_name, **kwargs):

        if psr_name not in self.psr_dict:
            print("PSR %s not in index." % psr_name)
        else:
            for key in kwargs:
                if key in self.psr_dict[psr_name]:
                    self.psr_dict[psr_name][key] = kwargs[key]
                    print("%s of PSR %s set to " % (key, psr_name) + str(kwargs[key]) + ".")
                else:
                    print("%s is not a valid parameter." % key)

    def delete_pulsar(self, psr_name, remove_dir=True):

        if psr_name not in self.psr_dict:
            print("PSR %s not in index." % psr_name)
        else:
            print("Deleting PSR %s." % psr_name)
            del self.psr_dict[psr_name]
            if remove_dir:
                shutil.rmtree(join(self.index_dir, psr_name), ignore_errors=True)

    def get_setup(self, psr_name, setup_id):

        if psr_name not in self.psr_dict:
            print("PSR %s not in index." % psr_name)
        else:
            if str(setup_id) not in self.get_pulsar(psr_name)["setups"]:
                print("No setup with id %d in PSR %s." % (setup_id, psr_name))
            else:
                return self.get_pulsar(psr_name)["setups"][str(setup_id)]

    def add_setup(self, psr_name, setup_id=None, results_filename=None, overwrite=False, **kwargs):

        if setup_id is None:
            setup_id = 0
            while str(setup_id) in self.get_pulsar(psr_name)["setups"]:
                setup_id += 1

        if str(setup_id) in self.get_pulsar(psr_name)["setups"] and not overwrite:
            print("Setup with id %d already existing for PSR %s. Skipping..." % (setup_id, psr_name))
            return
        else:
            print("Adding new setup with id %d to PSR %s" % (setup_id, psr_name))
            self.psr_dict[psr_name]["setups"][str(setup_id)] = PERIODICITY_DEFAULT.copy()

            subdir = join(self.index_dir, psr_name, str(setup_id))
            if not exists(subdir):
                os.mkdir(subdir)

            if results_filename is None:
                results_filename = join(subdir, "PSR_%s_setup_%d_results.csv" % (psr_name, setup_id))
            self.edit_setup(psr_name, setup_id, results_filename=results_filename)

            self.edit_setup(psr_name, setup_id, **kwargs)

    def edit_setup(self, psr_name, setup_id, **kwargs):

        if psr_name not in self.psr_dict:
            print("PSR %s not in index." % psr_name)
        else:
            if str(setup_id) not in self.get_pulsar(psr_name)["setups"]:
                print("No setup with id %d in PSR %s." % (setup_id, psr_name))
            else:
                for key in kwargs:
                    if key in self.get_setup(psr_name, setup_id):
                        self.psr_dict[psr_name]["setups"][str(setup_id)][key] = kwargs[key]
                        print("%s in setup %d of PSR %s set to " % (key, setup_id, psr_name) + str(kwargs[key]) + ".")
                    elif key in ["method", "radius", "a", "b", "mode", "emin", "emax"]:
                        self.psr_dict[psr_name]["setups"][str(setup_id)]["roiCuts"][key] = kwargs[key]
                        print("%s in setup %d of PSR %s set to " % (key, setup_id, psr_name) + str(kwargs[key]) + ".")
                    else:
                        print("%s is not a valid parameter." % key)

                rm_list = []
                if self.psr_dict[psr_name]["setups"][str(setup_id)]["roiCuts"]["method"] == "cookie":
                    rm_list = ["a", "b", "mode"]
                elif self.psr_dict[psr_name]["setups"][str(setup_id)]["roiCuts"]["method"] == "power_law":
                    rm_list = ["radius", "mode", "emin", "emax"]
                elif self.psr_dict[psr_name]["setups"][str(setup_id)]["roiCuts"]["method"] == "psf_like":
                    rm_list = ["radius", "a", "b", "emin", "emax"]

                for key in rm_list:
                    if key in self.psr_dict[psr_name]["setups"][str(setup_id)]["roiCuts"]:
                        del self.psr_dict[psr_name]["setups"][str(setup_id)]["roiCuts"][key]

                self.psr_dict[psr_name]["setups"][str(setup_id)]["last_modified"] = datetime.now(
                    timezone.utc).strftime(DATETIME_FORMAT)

    def delete_setup(self, psr_name, setup_id, remove_dir=True):

        if psr_name not in self.psr_dict:
            print("PSR %s not in index." % psr_name)
        else:
            if str(setup_id) not in self.get_pulsar(psr_name)["setups"]:
                print("No setup with id %d in PSR %s." % (setup_id, psr_name))
            else:
                print("Deleting setup %d in PSR %s." % (setup_id, psr_name))
                del self.psr_dict[psr_name]["setups"][str(setup_id)]
                if remove_dir:
                    shutil.rmtree(join(self.index_dir, psr_name, str(setup_id)), ignore_errors=True)

    def get_results_df(self, psr_name, setup_id):

        csv_filename = join(self.index_dir, psr_name, str(setup_id),
                            self.get_setup(psr_name, setup_id)["results_filename"])
        print("Loading " + csv_filename)
        if exists(csv_filename):
            df = pd.read_csv(csv_filename)
        else:
            print("No results csv file found.")
            df = pd.DataFrame()

        return df

    def get_ft1_filename(self, psr_name):

        ft1_filename = join(self.get_pulsar(psr_name)["ft1_filename"])

        return ft1_filename

    def list_pulsars(self):
        name_list = []
        for psr_name in self.psr_dict:
            name_list.append(psr_name)
        return name_list

    def print(self, psr_name):
        if psr_name not in self.psr_dict:
            print("PSR %s not in index." % psr_name)
        else:
            print("PSR %s" % psr_name)
            for key in self.get_pulsar(psr_name):
                if key != "setups":
                    print("  %s: " % key + str(self.get_pulsar(psr_name)[key]))
            print("  setups:")
            for setup_id in self.get_pulsar(psr_name)["setups"]:
                print("    %s:" % setup_id)
                for key in self.get_setup(psr_name, int(setup_id)):
                    print("      %s: " % key + str(self.get_setup(psr_name, setup_id)[key]))

    def num_setups(self, psr_name):
        if psr_name not in self.psr_dict:
            print("PSR %s not in index." % psr_name)
        else:
            return len(self.get_pulsar(psr_name)["setups"])

    def dump(self):
        with open(self.index_filename, "w") as jf:
            print("Writing to %s" % self.index_filename)
            json.to_json(self.psr_dict, jf, indent=4)

    def run(self, psr_name, setup_id, force_rerun=False, debug=False):

        if psr_name not in self.psr_dict:
            raise ValueError("PSR %s not in index." % psr_name)

        if str(setup_id) not in self.get_pulsar(psr_name)["setups"]:
            raise ValueError("No setup with id %d in PSR %s."
                             % (setup_id, psr_name))

        # Get info
        psr_dict = self.get_pulsar(psr_name)
        if debug:
            print(psr_dict)

        # Get ft1_filename
        ft1_filename = self.get_ft1_filename(psr_name)
        if not exists(ft1_filename):
            raise ValueError("Evfile %s not found." % ft1_filename)

        # Get setup and paths
        setup = self.get_setup(psr_name, setup_id)
        setup_subdir = join(self.index_dir, psr_name, str(setup_id))
        plots_dir = join(setup_subdir, "test_out")
        results_filename = join(setup_subdir, setup["results_filename"])

        # Get weights parameters
        if setup["weights"]["method"] == "none":
            mu = None
        elif setup["weights"]["method"] == "simple":
            mu = setup["weights"]["mu_w"]
        else:
            raise ValueError("Invalid weights method.")

        # Init pulsar and load data
        pulsar = FermiPulsar(psr_dict)
        print("Loading data from %s" % ft1_filename)
        pulsar.load_data(ft1_filename)
        print("Setting plots directory to %s" % plots_dir)
        pulsar.set_output_dir(plots_dir)
        if not exists(plots_dir):
            print("Creating plots directory")
            os.mkdir(plots_dir)

        # Init or load csv file
        if force_rerun or not os.path.exists(results_filename):
            print("Init empty results table")
            results_df = pd.DataFrame()
            check_list = []
            if exists(plots_dir):
                print("Removing old plots")
                shutil.rmtree(plots_dir, ignore_errors=True)
        else:
            print("Reading previous results file")
            results_df = self.get_results_df(psr_name, setup_id)
            check_list = results_df.TMIN_MJD.values[:-1]

        # Calculate number of time intervals
        start_mjd = met_to_mjd(min(pulsar.get_data().TIME))
        stop_mjd = met_to_mjd(max(pulsar.get_data().TIME))

        # Load number of values to test
        f0_trials = setup["n_f0"]
        f1_trials = setup["n_f1"]
        f2_trials = setup["n_f2"]

        tmin_mjd = np.round(start_mjd, decimals=6)
        tmax_mjd = np.round(tmin_mjd + setup["time_window"], decimals=6)
        ii = 0

        # Iterate over intervals
        while tmin_mjd < stop_mjd:

            # Run only the first time
            if tmin_mjd in check_list:
                print("Skipping MJD %d-%d" % (tmin_mjd, tmax_mjd))
            else:
                print("Running MJD %d-%d" % (tmin_mjd, tmax_mjd))
                pulsar.select_by_time(tmin_mjd, tmax_mjd)

                # Apply RoI cuts
                if setup["roiCuts"]["method"] == "cookie":
                    pulsar.cookie_cutter(setup["roiCuts"]["radius"],
                                         setup["roiCuts"]["emin"],
                                         setup["roiCuts"]["emax"])
                elif setup["roiCuts"]["method"] == "power_law":
                    pulsar.power_law_cutter(setup["roiCuts"]["a"],
                                            setup["roiCuts"]["b"])
                elif setup["roiCuts"]["method"] == "psf_like":
                    pulsar.psf_like_cutter(setup["roiCuts"]["mode"])
                else:
                    print("Invalid roi cuts. Taking full dataset ...")

                # Check data size
                print("Selected %d photons" % len(pulsar.get_data()))
                if len(pulsar.get_data()) < 100:
                    print("Low photon count. Skipping ...")
                else:

                    # Run H test and append results
                    try:
                        results_dict = pulsar.run_h_scan(f0_trials=f0_trials,
                                                         f1_trials=f1_trials,
                                                         f2_trials=f2_trials,
                                                         mu=mu)
                        print("F0=%.6f    F1=%.2e    H=%.2f"
                              % (results_dict["F0_BEST"],
                                 results_dict["F1_BEST"],
                                 results_dict["H"],))
                        results_dict["TMIN_MJD"] = tmin_mjd
                        results_dict["TMAX_MJD"] = tmax_mjd
                        del results_dict["PULSE_PHASE"]

                        df_line = pd.DataFrame(results_dict, index=[ii])
                        results_df = results_df.append(df_line)
                        with open(results_filename, "w") as rf:
                            results_df.to_csv(rf, index=False)

                        check_list = results_df.TMIN_MJD.values

                    except ValueError:
                        print("Unable to run the test on the current selection.")

                    pulsar.reset_cuts()

            # Update pulsar parameters to latest epoch
            pulsar.set_parameters(
                PEPOCH=results_df.PEPOCH.values[ii],
                F0=results_df.F0_BEST.values[ii],
                F1=results_df.F1_BEST.values[ii]
            )

            # Update time interval
            ii += 1
            tmin_mjd = np.round(tmax_mjd - setup["sliding_time"], decimals=6)
            tmax_mjd = np.round(tmin_mjd + setup["time_window"], decimals=6)

        #self.edit_setup(psr_name, setup_id,
        #    last_run=datetime.now(timezone.utc).strftime(DATETIME_FORMAT))
        #self.dump()
