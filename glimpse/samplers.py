import os
import h5py

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from skimage.filters import gaussian as gaussian_filter
from cpnest.cpnest import CPNest

from glimpse.utils import get_credibility_levels, Estimator, confidence_levels, \
    met_to_mjd, DAY_TO_SECS, get_quantiles


class NestedSampler(CPNest):

    quantiles = [0.5 * (1. - confidence_levels[0]),
                 0.5,
                 0.5 * (1. + confidence_levels[0])]
    data_color = "black"
    contour_colors = ["lightsteelblue", "cornflowerblue", "darkblue"]
    estimate_color = "salmon"
    cmap = "Greys"

    def get_posterior_data(self):

        cpnest_filename = os.path.join(self.output, "cpnest.h5")

        with h5py.File(cpnest_filename, 'r') as f:
            posterior_sample = pd.DataFrame(
                np.array(f["combined"]["posterior_samples"])
            )

        # Add default values for fixed params
        parnames = np.array(self.user.parameters)
        fixed_parnames = parnames[np.invert(self.user.free)]
        for pi in fixed_parnames:
            value = self.user.default[np.where(parnames == pi)[0][0]]
            posterior_sample[pi] = np.ones(len(posterior_sample)) * value

        return posterior_sample

    def plot_profile(self, n_bins=20, outfile="posterior_profile.png", transparent=False):

        posterior_sample = self.get_posterior_data()
        parnames = self.user.parameters
        subpars = np.array(self.user.profile_model.parameters)

        fig = plt.figure(figsize=(9, 3))
        if transparent:
            fig.patch.set_alpha(0)
        ax = fig.add_subplot(111)

        # Plot contours
        phi = np.linspace(0, 2, 1000)
        curve_sample = []
        for ri, x in posterior_sample[subpars].iterrows():
            curve = self.user.profile_model(phi, x.values.flatten())
            curve_sample.append(curve)
        q1 = np.quantile(curve_sample, self.quantiles, axis=0)
        ax.fill_between(phi, q1[0] / n_bins, q1[2] / n_bins,
                        ls="", lw=0, facecolor=self.contour_colors[0])

        # Plot hitogram and error bars at the most probable parameters
        max_idx = np.argmax(posterior_sample["logL"].values)
        opt_pars = posterior_sample.loc[max_idx][parnames].values.flatten()
        pi = self.user.get_phases(opt_pars)
        wi = self.user.get_weights()
        counts, edges, _ = plt.hist(np.concatenate([pi, pi+1.]),
                                bins=np.linspace(0., 2., 2 * n_bins + 1),
                                weights=np.concatenate([wi, wi]),
                                color=self.data_color, histtype="step")
        centers = 0.5 * (edges[1:] + edges[:-1])
        uncs_sup = np.sqrt(counts)
        uncs_inf = uncs_sup.copy()
        ax.errorbar(centers, counts, yerr=[uncs_inf, uncs_sup], ls="",
                marker="", ms=5, color=self.data_color)

        # Plot most probable curve
        ax.plot(phi, q1[1] / n_bins, color=self.contour_colors[-1])

        # Labels
        ax.set_ylim(0.,)
        ax.set_xlim(0., 2.)
        ax.set_xlabel("phase")
        ax.set_ylabel("Weighted counts")
        plt.tight_layout()

        out_filename = os.path.join(self.output, outfile)
        plt.savefig(out_filename)
        plt.close("all")

    def plot_timing(self, outfile="posterior_timing.png", transparent=False):

        posterior_sample = self.get_posterior_data()
        subpars = np.array(self.user.timing_model.parameters)

        fig = plt.figure(figsize=(9, 6))
        if transparent:
            fig.patch.set_alpha(0)

        # Get time in secs since PEPOCH and in MJD
        ti = self.user.get_photon_times()
        time_secs = np.linspace(np.min(ti), np.max(ti), 1000)
        time_mjd = time_secs / DAY_TO_SECS + self.user.timing_model.PEPOCH

        # Generate a sample of curves
        frequency_sample, fdot_sample = [], []
        for ri, x in posterior_sample[subpars].iterrows():
            curve = self.user.timing_model(time_secs, x.values.flatten())
            frequency_sample.append(curve[1])
            fdot_sample.append(curve[2])

        # Plot frequency contours
        ax = fig.add_subplot(211)
        q = np.quantile(frequency_sample, self.quantiles, axis=0)
        slope = (q[1][-1] - q[1][0]) / (time_secs[-1] - time_secs[0])
        ax.fill_between(time_mjd,
                        q[0] - slope * (time_secs - time_secs[0]),
                        q[2] - slope * (time_secs - time_secs[0]),
                        ls="", lw=0, facecolor=self.contour_colors[0])
        ax.plot(time_mjd, q[1] - slope * (time_secs - time_secs[0]), color=self.contour_colors[-1])
        ax.set_ylabel("$f$ (Hz)")

        # Plot fdot contours
        ax = fig.add_subplot(212, sharex=ax)
        q = np.quantile(fdot_sample, self.quantiles, axis=0)
        ax.fill_between(time_mjd, q[0], q[2],
                        ls="", lw=0, facecolor=self.contour_colors[0])
        ax.plot(time_mjd, q[1], color=self.contour_colors[-1])
        ax.set_xlim(time_mjd[0], time_mjd[-1])
        ax.set_xlabel("MJD")
        ax.set_ylabel("$\dot f$ (Hz s$^{-1}$)")

        fig.tight_layout()
        plt.subplots_adjust(hspace=0.)

        out_filename = os.path.join(self.output, outfile)
        plt.savefig(out_filename)
        plt.close("all")

    def plot_corner(self, only_free=True, parameters=None, outfile="posterior-corner.png", transparent=False):

        posterior_sample = self.get_posterior_data()[self.user.parameters]
        if parameters is not None:
            posterior_sample = posterior_sample[parameters]
        else:
            if only_free:
                free_parnames = np.array(self.user.parameters)[self.user.free]
                posterior_sample = posterior_sample[free_parnames]

        parnames = np.sort(posterior_sample.columns.values.tolist())
        n_pars = len(parnames)
        n_bins = int(np.sqrt(len(posterior_sample)))

        fig = plt.figure(figsize=(2 * n_pars, 2 * n_pars))
        if transparent:
            fig.patch.set_alpha(0)

        i = 0
        for p1 in parnames:

            # First element in column is a 1D histogram
            axes = fig.add_subplot(n_pars, n_pars, n_pars * i + i + 1)
            axes.hist(posterior_sample[p1], histtype="step", bins=n_bins, color=self.data_color)
            axes.get_xaxis().set_visible(False)
            axes.get_yaxis().set_visible(False)

            # Print measure with uncertainties
            measure = Estimator.from_sample(posterior_sample[p1].values).to_latex()
            axes.set_title("%s = " % p1 + measure)

            # Overplot quantiles
            q1 = get_quantiles(posterior_sample[p1].values)
            axes.axvline(q1[1], color=self.estimate_color)
            for k in [0, -1]:
                axes.axvline(q1[k], ls="--", color=self.contour_colors[-1])

            # Iterate over other parameters and make 2D histogram
            j = 0
            for p2 in parnames[(i + 1):]:
                axes = fig.add_subplot(n_pars, n_pars,
                                       n_pars * (i + j + 1) + i + 1,
                                       sharex=axes)
                axes.hist2d(posterior_sample[p1], posterior_sample[p2],
                            bins=[n_bins, n_bins], cmap=self.cmap)
                counts, x_edges, y_edges = np.histogram2d(posterior_sample[p1],
                                                          posterior_sample[p2],
                                                          bins=[n_bins, n_bins])

                # Plot 1-2-3 sigma contours of smoothed histogram
                x_locs = 0.5 * (x_edges[1:] + x_edges[:-1])
                y_locs = 0.5 * (y_edges[1:] + y_edges[:-1])
                filtered_counts = gaussian_filter(np.transpose(counts), sigma=2,
                                                  mode="nearest")
                levels = np.flip(get_credibility_levels(filtered_counts))
                axes.contour(x_locs, y_locs, filtered_counts, levels,
                             colors=self.contour_colors)

                # Overplot lines indicating median value
                q2 = get_quantiles(posterior_sample[p2].values)
                axes.axvline(q1[1], color=self.estimate_color)
                axes.axhline(q2[1], color=self.estimate_color)

                # Hide labels in subplot spaces
                axes.get_xaxis().set_visible(False)
                axes.get_yaxis().set_visible(False)
                if i == 0:
                    axes.set_ylabel(p2)
                    axes.get_yaxis().set_visible(True)
                j += 1


            axes.get_xaxis().set_visible(True)
            axes.set_xlabel(p1, labelpad=20)
            i += 1

        fig.tight_layout()
        plt.subplots_adjust(wspace=0.1, hspace=0.1)

        out_filename = os.path.join(self.output, outfile)
        plt.savefig(out_filename)
        plt.close("all")
