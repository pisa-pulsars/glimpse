import yaml

import numpy as np
import pandas as pd
from astropy.table import Table
from math import factorial

from glimpse.htest import h_scan, Hmw_test
from glimpse.utils import DAY_TO_SECS, LAT_StartMissionMJD, \
    met_to_mjd, mjd_to_met, sigma_psf, angular_distance, dms_to_decimal, \
    hours_to_degrees, psf_scaling, taylor, fold_phases


class BaseSource:

    col_names = ["ENERGY", "RA", "DEC", "TIME", "FRONT/BACK", "PSF"]

    def __init__(self):

        self.RAJ = 180.
        self.DECJ = 0.

    def set_parameters(self, **kwargs):
        """
        Set the value of a parameter.

        Args:
            kwargs: parameter name and value in `key=value` format
        """

        for key, value in kwargs.items():
            if key not in self.__dict__:
                print("Invalid attribute %s for object FermiPulsar." % key)
            else:
                if key == "RAJ":
                    value = hours_to_degrees(dms_to_decimal(value)) \
                        if isinstance(value, str) else float(value)
                elif key == "DECJ":
                    value = dms_to_decimal(value) \
                        if isinstance(value, str) else float(value)
                else:
                    try:
                        value = float(value)
                    except ValueError:
                        value = value

                setattr(self, key, value)

    def drop_parameters(self, *args):
        """
        Drop a parameter from class attributes.

        Args:
            args (str, list): parameter name or list of parameter names
        """
        for par_name in args:
            if par_name in self.__dict__:
                del self.__dict__[par_name]
            else:
                print(par_name + " not in pulsar parameters.")


class FermiPulsar(BaseSource):
    """This class handles info about Fermi-LAT pulsars and provides methods
    for photon data reduction.

    Attributes:
        PSRJ (str): pulsar name in J2000 coordinates
        PEPOCH (float): MJD reference of the timing parameters
        RAJ (float): right ascension in degrees
        DECJ (float): declination in degrees
        F0 (float): spin frequency at the reference epoch in Hz
        F1 (float): derivative of the frequency at the reference epoch in Hz / s
        F1 (float): second derivative of the frequency at the reference epoch in Hz / s^2
        outDir (str): path to the output directory (for H-test diagrams)
    """

    def __init__(self, *data, **kwargs):

        super().__init__()

        self.PSRJ = "my-pulsar"
        self.PEPOCH = LAT_StartMissionMJD
        self.F0 = 1.
        self.F1 = 0.
        self.F2 = 0.
        self._data_full = None
        self._data_selected = None
        self.outDir = "./"

        for dictionary in data:
            self.set_parameters(**dictionary)
        self.set_parameters(**kwargs)

    def _check_data(self):
        """
        Hidden method to check cached data.

        Raises:
            AttributeError: If no data is present in the cache.
        """
        if self._data_selected is None:
            raise AttributeError("No photon times loaded.")

    def _check_epochs(self):
        """
        Hidden method to check time consistency.

        Raises:
            ValueError: When PEPOCH is outside the data time range
        """

        self._check_data()
        tmin_mjd = met_to_mjd(min(self._data_selected.TIME))
        tmax_mjd = met_to_mjd(max(self._data_selected.TIME))
        t0_mjd = self.PEPOCH
        if tmin_mjd > t0_mjd or tmax_mjd < t0_mjd:
            raise ValueError("Epoch is outside of data time range.")

    def set_output_dir(self, dirname):
        """
        Set the output directory.

        Args:
            dirname: path to the output directory.
        """
        self.outDir = dirname
    
    def load_data(self, source):
        """
        Load barycentered photon data from a LAT FT1 file into a pandas DataFrame.

        Args:
            source: path to the FT1 file or pandas DataFrame instance
        """

        if isinstance(source, str):

            ext = source[-5:].split(".")[-1]
            if ext == "fits":
                data_table = Table.read(source, format="fits", hdu=1)
            elif ext == "csv":
                data_table = Table.read(source, format="csv")
            else:
                data_table = Table.read(source)
            self._data_full = data_table["ENERGY", "RA", "DEC", "TIME"].to_pandas()

            '''
            if ext == "fits":
                type_matrix = data_table["EVENT_TYPE"]
                self._data_full["FRONT/BACK"] = np.where(type_matrix[:, -1],
                                                         "FRONT", "BACK")
                type_list = np.where(type_matrix[:, -3], "PSF1", "NONE")
                type_list = np.where(type_matrix[:, -4], "PSF2", type_list)
                type_list = np.where(type_matrix[:, -5], "PSF3", type_list)
                type_list = np.where(type_matrix[:, -6], "PSF4", type_list)
                self._data_full["PSF"] = type_list
            '''

            center = np.array([1., self.RAJ, self.DECJ])
            ra = self._data_full.RA
            dec = self._data_full.DEC
            distance = [angular_distance(center, np.array([1., ra[i], dec[i]]))
                        for i in range(len(ra))]
            self._data_full["ROI_DIST"] = np.array(distance)

        elif isinstance(source, pd.DataFrame):

            checklist = [key in source.columns for key in self.col_names]
            if not all(checklist):
                raise KeyError("Input DataFrame must contain the following "
                               "columns: %s" % str(self.col_names))
            self._data_full = source

        else:
            raise TypeError("Input must be of type string or pandas.DataFrame.")

        self._data_selected = self._data_full.copy()

    def get_data(self):
        """
        Return the current data selection in pandas DataFrame format.
        """

        return self._data_selected

    def get_phases(self):
        """
        Returns rotational phases computed using a 2-nd order Taylor series.
        """
        deltat = self._data_selected.TIME - mjd_to_met(self.PEPOCH)
        phi = [taylor(x, np.array([0., self.F0, self.F1, self.F2]))[0]
               for x in deltat]
        phi = fold_phases(phi)
        return phi

    def get_weights(self, mu):
        """
        Compute simple weights as in Bruel 2019.

        Args:
            mu (float): weights parameter

        Returns:
            np.ndarray: simple weights
        """
        E = self._data_selected["ENERGY"]
        x = self._data_selected["ROI_DIST"]
        w1 = np.exp(-2 * (np.log10(E) - mu) ** 2)
        w2 = (1 + (9 * x ** 2) / (4 * sigma_psf(E) * 2)) ** (-2)

        return (w1 * w2).values

    def select_by_time(self, tmin=0., tmax=0., scale="MJD"):
        """
        Select data in a chosen time range.

        Args:
            tmin (float): lower boundary
            tmax (float): upper boundary
            scale (str): scale of the time boundaries (choose 'MJD' or 'MET')
        """
        self._check_data()
        if tmax == 0.:
            tmax = np.inf
        else:
            if scale == "MJD":
                tmax = mjd_to_met(tmax)
        if scale == "MJD":
            tmin = mjd_to_met(tmin)
        mask = (self._data_selected.TIME >= tmin) * \
               (self._data_selected.TIME < tmax)
        self._data_selected = self._data_selected.loc[mask]

    def select_by_energy(self, emin=0., emax=0.):
        """
        Select data in a chosen energy rage.

        Args:
            emin (float): lower boundary in MeV
            emax: upper boundary in MeV
        """
        self._check_data()
        if emax == 0.:
            emax = np.inf
        mask = (self._data_selected.ENERGY >= emin) * \
               (self._data_selected.ENERGY < emax)
        self._data_selected = self._data_selected.loc[mask]

    def cookie_cutter(self, radius, emin=0., emax=500000.):
        """
        Select data in a circle of chosen radius and optionally in a chosen energy range.

        Args:
            radius (float): radius of the selection in degrees
            emin (float): lower boundary of the energy range in MeV
            emax (float): upper boundary of the energy range in MeV
        """
        self._check_data()
        distance = self._data_selected["ROI_DIST"].values
        self._data_selected = self._data_selected.loc[distance < radius]
        self.select_by_energy(emin, emax)

    def power_law_cutter(self, a, b):
        """
        Select data within an energy-dependent radius defined as a power law.
        The function is defined as follows:
        > r(E) = a * (E / 100 GeV)^(-b)

        Args:
            a (float): Scale factor
            b (float): Power-law index
        """
        self._check_data()
        distance = self._data_selected["ROI_DIST"].values
        energy = self._data_selected.ENERGY
        mask = distance < (a * (energy / 100000.)**(-b))
        self._data_selected = self._data_selected.loc[mask]

    def psf_like_cutter(self, mode="psf"):
        """
        Select data within an energy-dependent radius defined as the 68%
        containment radius of the LAT PSF. the function is defined as follows:
        > r(E) = sqrt{[ a * (E / 100 MeV)^(-b) ]^2 + c^2}
        The parameters depend on the chosen data partition method.

        Args:
            mode (str): LAT partition method (choose between 'frontback' and 'psf')
        """
        self._check_data()
        distance = self._data_selected["ROI_DIST"].values
        energy = self._data_selected.ENERGY

        if mode == "psf":
            evtype_list = ["PSF0", "PSF1", "PSF2", "PSF3"]
        elif mode == "frontback":
            evtype_list = ["FRONT", "BACK"]
        else:
            raise ValueError("Invalid mode. Choose among psf or frontback.")

        filtered_df = pd.DataFrame()
        for evtype in evtype_list:
            radius = psf_scaling(energy, evtype) * 180. / np.pi
            mask = (self.get_data().PSF == evtype) * (distance < radius)
            filtered_df = pd.concat(
                [filtered_df, self.get_data().copy().loc[mask]])

        self._data_selected = filtered_df.copy()

    def reset_cuts(self):
        """
        Restore data selection to the original data set.
        """
        self._data_selected = self._data_full.copy()

    @classmethod
    def from_yaml(cls, filename):

        with open(filename, "r") as stream:
            config = yaml.safe_load(stream)["fermiPulsar"]

        my_psr = cls(config)
        try:
            my_psr.load_data(config["dataFilename"])
        except KeyError as e:
            print("No data filename provided.")

        try:
            data_selection = config["dataSelection"]
        except KeyError as e:
            data_selection = None
            print("No photon cuts specified.")

        if data_selection is not None:
            try:
                my_psr.select_by_time(
                    data_selection["tminMJD"],
                    data_selection["tmaxMJD"])
            except KeyError:
                print("No time cuts specified.")

            try:
                roi_cuts = data_selection["roiCuts"]
                if roi_cuts["method"] == "cookie":
                    my_psr.cookie_cutter(roi_cuts["radius"],
                                       roi_cuts["emin"],
                                       roi_cuts["emax"])
                elif roi_cuts["method"] == "power_law":
                    my_psr.power_law_cutter(roi_cuts["a"], roi_cuts["b"])
                elif roi_cuts["method"] == "psf_like":
                    my_psr.psf_like_cutter(roi_cuts["mode"])
                else:
                    print("Invalid attribute dataSelection.roiCuts.method")
            except KeyError:
                print("No RoI cuts specified.")

        return my_psr

    def from_config(self, filename):
        """
        Init from a YAML conf file.

        Args:
            filename (str): path to the YAML conf file

        Returns:
            glimpse.sources.FermiPulsar: a LAT pulsar instance
        """

        with open(filename, "r") as stream:
            print("Reading %s" % filename)
            config =yaml.safe_load(stream, yaml.Loader)["fermiPulsar"]

        self.__init__(config)
        self.load_data(config["dataFilename"])
        self.select_by_time(config["dataSelection"]["tminMJD"],
                            config["dataSelection"]["tmaxMJD"])

        roi_cuts = config["dataSelection"]["roiCuts"]
        if roi_cuts["method"] == "cookie":
            self.cookie_cutter(roi_cuts["radius"],
                               roi_cuts["emin"],
                               roi_cuts["emax"])
        elif roi_cuts["method"] == "power_law":
            self.power_law_cutter(roi_cuts["a"], roi_cuts["b"])
        elif roi_cuts["method"] == "psf_like":
            self.psf_like_cutter(roi_cuts["mode"])
        else:
            print("Invalid attribute dataSelection.roiCuts.method")

        return self

    def optimize_time_window(self, sigmas=3, mu=3., verbose=False):
        """
        Optimize the selection time window by computing the Hmw test
        significance and taking the shortest window above threshold.

        Args:
            sigmas (float): threshold significance in sigmas
            mu (float): simple weight parameter
            verbose (bool): if `True`, print the search trail

        Returns:
            float: optimal time window in days
        """

        t0_mjd = self.PEPOCH
        old_data = self._data_selected.copy()

        pmw = 0.
        dt_days = 0.
        while dt_days < 365 and pmw < sigmas:
            dt_days += 7.
            self.select_by_time(tmin=t0_mjd - 0.5 * dt_days,
                                tmax=t0_mjd + 0.5 * dt_days, scale="MJD")
            phi = self.get_phases()
            wi = self.get_weights(mu)
            hmw, pmw = Hmw_test(phi, wi)
            if verbose:
                print("days = %d    Hmw = %.1f    Ps = %.2f " %
                      (dt_days, hmw, pmw))
            self._data_selected = old_data.copy()

        return dt_days

    def optimize_roi_cuts(self, methods=['cookie', 'power_law'],
                          mu=3., verbose=False):
        """
        Optimize the RoI cuts  by maximizing the Hmw test statistics.

        Args:
            methods (list): list of strings representing the selection methods to test (avalable options: `cookie`, 'power-law', 'psf-like')
            mu: simple weight parameter
            verbose: if `True`, print the search trail

        Returns:
            dict: optimal cuts settings
        """

        self._check_epochs()
        old_data = self._data_selected.copy()

        pmw_max = 0.

        if 'psf-like' in methods:
            self.psf_like_cutter(mode="psf")
            phi = self.get_phases()
            wi = self.get_weights(mu)
            hmw, pmw = Hmw_test(phi, wi)
            if verbose:
                print("method = psf_like    mode = psf    Hw = %.1f"
                      "    Ps = %.2f" % (hmw, pmw))
            pmw_max = hmw
            output = {"method": "psf_like", "mode": "psf"}
            self._data_selected = old_data.copy()

        if 'cookie' in methods:
            radii = np.arange(0.8, 10.1, 0.1)
            energies = np.logspace(2, 3, 5)
            for radius in radii:
                for emin in energies:
                    self.cookie_cutter(radius=radius, emin=emin)
                    phi = self.get_phases()
                    wi = self.get_weights(mu)
                    hmw, pmw = Hmw_test(phi, wi)
                    if verbose:
                        print("method = cookie    radius = %.1f    emin = %.1f"
                              "    Hw = %.1f    Ps = %.2f" %
                              (radius, emin, hmw, pmw))
                    if pmw > pmw_max:
                        pmw_max = pmw
                        output = {"method": "cookie", "radius": radius,
                                  "emin": emin, "emax": 300000.}
                    self._data_selected = old_data.copy()

        if 'power_law' in methods:
            a_list = np.arange(0.5, 2.1, 0.1)
            b_list = np.arange(0.5, 2.1, 0.1)
            for A in a_list:
                for B in b_list:
                    self.power_law_cutter(A, B)
                    phi = self.get_phases()
                    wi = self.get_weights(mu)
                    hmw, pmw = Hmw_test(phi, wi)
                    print("mu_w = %.2f    Ps = %.2f" % (mu, pmw))
                    if verbose:
                        print("method = power_law    A = %.1f    B = %.1f"
                              "    Hw = %.1f    Ps = %.2f" % (A, B, hmw, pmw))
                    if pmw > pmw_max:
                        pmw_max = pmw
                        output = {"method": "psf_like", "a": A, "b": B}
                    self._data_selected = old_data.copy()

        return output

    def optimize_weights(self, verbose=False):
        """
        Optimize the simple weight parameter maximizing the Hmw statistics.

        Args:
            verbose (bool): if `True`, print the search trail

        """
        self._check_data()
        phi = self.get_phases()

        mu_test = np.linspace(2, 4, 21)
        hmw_max = 0.
        output = 3.
        for mu in mu_test:
            wi = self.get_weights(mu)
            hmw, pmw = Hmw_test(phi, wi)
            if verbose:
                print("mu = %.2f    Hw = %.2f    Ps = %.2f" % (mu, hmw, pmw))
            if hmw > hmw_max:
                hmw_max = hmw
                output = mu

        return output

    def run_h_scan(self, m=20, c=4, mu=None, compute_errors=True,
                   f0_trials=200, f1_trials=50, f2_trials=1, plot_results=True):
        """
        Scan the F0-F1 parameter space to maximize the Hmw statistics.

        Args:
            m (int): number of harmonics used to compute the H-statistics
            c (int): H-statistics factor
            mu (float): simple weights parameter
            compute_errors (bool): if `True`, estimate uncertainties via sample bootstrapping
            f0_trials (int): number of F0 trials
            f1_trials (int): number of F1 trials
            f2_trials (int): number of F2 trials
            plot_results (bool): if `True`, produce a diagnostic plot

        Returns:
            dict: output parameters of the scan
        """
        if self._data_selected is None:
            raise AttributeError("No photon times loaded.")

        pars_epoch = []
        fi = 0
        while "F"+str(fi) in self.__dict__:
            pars_epoch.append(self.__dict__["F"+str(fi)])
            fi += 1

        # calculate central time t0 and time distance
        tmin_met = np.min(self.get_data().TIME)
        tmax_met = np.max(self.get_data().TIME)
        t0_mjd = met_to_mjd(0.5 * (tmin_met + tmax_met))
        dt_sec = (t0_mjd - self.PEPOCH) * DAY_TO_SECS

        # TODO: use taylor() function
        # calculate expected values at t0
        while len(pars_epoch) < 3:
            pars_epoch.append(0)
        pars_start = np.array(pars_epoch)
        for fi in range(len(pars_start) - 1):
            fj = 1
            while fj < len(pars_start) - fi:
                pars_start[fi] += pars_epoch[fi+fj] * \
                                  dt_sec ** fj / factorial(fj)
                fj += 1
        if f2_trials == 1:
            pars_start[2] = pars_epoch[2]

        ti = self.get_data().TIME
        if mu is None:
            wi = np.ones(len(ti))
        else:
            wi = self.get_weights(mu)
        results = h_scan(photon_times=self.get_data().TIME,
                         pars_start=pars_start, f0_trials=f0_trials,
                         f1_trials=f1_trials, m=m, c=c, weights=wi,
                         outdir=self.outDir, compute_errors=compute_errors,
                         plot_results=plot_results, psr_name=self.PSRJ)

        self._data_selected["PULSE_PHASE"] = results["PULSE_PHASE"]

        return results
