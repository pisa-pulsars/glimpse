import os
from datetime import datetime, timezone

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import yaml

from glimpse.distributions import MultivariateGaussian, Moffat, Uniform
from glimpse.models.noise import TimingNoise
from glimpse.models.profile import ProfileModel
from glimpse.models.spectrum import SpectralModel, PowerLawSuperExpCutoff2, \
    PowerLaw
from glimpse.models.timing import TimingModel
from glimpse.sources import FermiPulsar
from glimpse.utils import DAY_TO_SECS, LAT_StartMissionMJD, \
    met_to_mjd, mjd_to_met, sigma_psf, angular_distance, fold_phases


def sample_energy(spectrum, pars, size, bin_size=1.):
    e_band = spectrum.emax - spectrum.emin
    n_bins = int(np.ceil(e_band / bin_size))
    edges = np.linspace(spectrum.emin, spectrum.emax + bin_size, n_bins + 1)

    probs = 0.5 * e_band / n_bins * (
                spectrum(edges[:-1], pars) + spectrum(edges[1:], pars))
    probs = probs / np.sum(probs)

    sample = spectrum.emin + e_band / n_bins * \
             (np.random.choice(np.arange(0, n_bins, 1), size, p=probs) +
              np.random.random(size))

    return sample


class PulsarSimulator:

    my_cmap = "Blues"
    my_datacolor = "darkblue"
    my_modelcolor = "red"

    def __init__(self, PSRJ="my-pulsar"):

        base_keys = dir(self)

        self.PSRJ = PSRJ
        self.RAJ = 180.  # degrees
        self.DECJ = 0.  # degrees
        self.roiRadius = 10.  # degrees
        self.emin = 100.  # MeV
        self.emax = 300000.  # MeV
        self.tmin = LAT_StartMissionMJD  # MJD
        self.tmax = LAT_StartMissionMJD + 365.  # MJD
        self.signalToBkg = 1.e6  # pulsar photons vs. bkg photons
        self.tnCutLow = None  # Hz
        self.tnCutHigh = None  # Hz
        self.outdir = "./"  # directory where the plot will be saved
        self.nPhotons = 1000  # number of photons in the sample
        self.bkgModel = PowerLaw(100., 300000.)
        self.bkgParameters = np.array([-2.])
        self.seed = datetime.now(timezone.utc).timestamp()

        self._keys = dir(self)
        for key in base_keys:
            self._keys.remove(key)

        self.timing_model = None
        self.timing_pars = None
        self.tn_model = None
        self.tn_pars = None
        self.profile_model = None
        self.profile_pars = None
        self.spectral_model = PowerLawSuperExpCutoff2(100., 300000.)
        self.spectral_pars = np.array([-1.4, 0.001, 2. / 3.])

        self._data = None
        self._figure = None
        self._rng = None

    @classmethod
    def from_yaml(cls, filename):

        # Load simulator data
        with open(filename, "r") as stream:
            config = yaml.safe_load(stream)["pulsarSimulator"]

        # Init simulator
        my_sim = PulsarSimulator()
        sim_pars = list(config.keys())
        if "bkgModel" in sim_pars:
            my_sim.set_background_model(
                eval(config["bkgModel"]),
                np.array(config["bkgParameters"])
            )
            sim_pars.remove("bkgModel")
            sim_pars.remove("bkgParameters")

        sim_pars.remove("modelParameters")
        for key in sim_pars:
            if key not in my_sim._keys:
                raise KeyError(f"Invalid key {key}")

            print(f"Setting {key} to {config[key]}")
            if key in ["PSRJ", "outdir"]:
                setattr(my_sim, key, config[key])
            else:
                setattr(my_sim, key, float(config[key]))

        my_tm = TimingModel.from_yaml(filename)
        tm_pars = np.array(
            [float(config["modelParameters"][pi]) for pi in my_tm.parameters])
        my_sim.set_timing_model(my_tm, tm_pars)

        my_pm = ProfileModel.from_yaml(filename)
        pm_pars = np.array(
            [float(config["modelParameters"][pi]) for pi in my_pm.parameters])
        my_sim.set_profile_model(my_pm, pm_pars)

        try:
            my_tn = TimingNoise.from_yaml(filename)
            tn_pars = np.array(
                [float(config["modelParameters"][pi]) for pi in my_tn.parameters])
            my_sim.set_noise_model(my_tn, tn_pars)
        except KeyError:
            print("Timing noise model not set.")

        try:
            my_sed = SpectralModel.from_yaml(filename)
            sed_pars = np.array(
                [float(config["modelParameters"][pi]) for pi in my_sed.parameters])
            my_sim.set_spectral_model(my_sed, sed_pars)
        except KeyError:
            print("Using default spectral model.")

        return my_sim

    def set_timing_model(self, model, pars):

        if not isinstance(model, TimingModel):
            raise ValueError(
                "Invalid models. Please provide a TimingManager object.")

        self.timing_model = model
        self.timing_pars = pars

    def set_profile_model(self, model, pars):

        if not isinstance(model, ProfileModel):
            raise ValueError(
                "Invalid models. Please provide a ProfileModel object.")

        self.profile_model = model
        self.profile_pars = pars

    def set_noise_model(self, model, pars):

        if not isinstance(model, TimingNoise):
            raise ValueError(
                "Invalid models. Please provide a TimingNoise object.")

        self.tn_model = model
        self.tn_pars = pars

    def set_spectral_model(self, model, pars):

        self.spectral_model = model
        self.spectral_pars = pars

    def set_background_model(self, model, pars):

        self.bkgModel = model
        self.bkgParameters = pars

    def run(self, n_photons=None, energy_bin_size=1.):

        if not n_photons:
            n_photons = int(self.nPhotons)

        signal = int(n_photons * self.signalToBkg / (self.signalToBkg + 1))
        bkg = n_photons - signal

        self._init_dataset(signal)
        self._rng = np.random.default_rng(int(self.seed))
        if signal > 0:
            self._random_energy(energy_bin_size)
            self._random_position()
            self._random_time()
        if bkg > 0:
            self._add_bkg(bkg)

        idx = np.argsort(self._data["TIME"].values)
        self._data = self._data.iloc[idx]

    def plot(self):

        self._init_figure()
        self._plot_map()
        self._plot_timing()
        self._plot_phase()
        self._plot_spectrum()

        filename = "pulsarsim_" + self.PSRJ + "_plots.png"
        self._figure.savefig(os.path.join(self.outdir, filename))
        plt.close()

    def get_dataframe(self):

        return self._data

    def get_as_pulsar(self):

        pulsar = FermiPulsar(
            PSRJ=self.PSRJ,
            RAJ=self.RAJ, DECJ=self.DECJ,
            PEPOCH=self.timing_model.PEPOCH,
            F0=0., F1=0., F2=0.,
        )
        f0, f1 = self.timing_model(
            mjd_to_met(self.timing_model.PEPOCH), self.timing_pars)[1:]
        pulsar.F0 = f0
        pulsar.F1 = f1
        if self._data is not None:
            pulsar.load_data(self._data)

        return pulsar

    def _init_dataset(self, size):

        one = np.ones(size)
        self._data = pd.DataFrame(
            {"ENERGY": one * self.emin,
             "RA": one * self.RAJ,
             "DEC": one * self.DECJ,
             "ROI_DIST": one * 0.,
             "TIME": one * self.tmin,
             "FRONT/BACK": np.random.choice(["FRONT", "BACK"], size),
             "PSF": np.random.choice(["PSF0", "PSF1", "PSF2", "PSF3"], size)
             }
        )

    def _random_energy(self, bin_size=1.):

        self._data["ENERGY"] = sample_energy(
            self.spectral_model, self.spectral_pars, len(self._data),
            bin_size)

    def _random_position(self):

        center = np.array([self.RAJ, self.DECJ])

        '''
        #distr = MultivariateGaussian(np.zeros(2), np.identity(2))
        sample = []
        distances = []
        for E in self._data["ENERGY"]:

            roi_dist = self.roiRadius + 1
            while roi_dist > self.roiRadius:
                point = distr.random(1)[0] * sigma_psf(E) + center
                roi_dist = angular_distance(
                    np.array([1., point[0], point[1]]),
                    np.array([1., center[0], center[1]])
                )
            sample.append(point)
            distances.append(roi_dist)
         
        self._data["ROI_DIST"] = distances
        self._data["RA"] = np.array(sample)[:, 0]
        self._data["DEC"] = np.array(sample)[:, 1]
        '''

        distr = Moffat(2. / 3, 2)
        r = np.array([])
        while len(r) < len(self._data):
            E = self._data["ENERGY"].values
            sample = distr.random(len(self._data)) * sigma_psf(E)
            accepted = sample[sample < self.roiRadius]
            r = np.concatenate([r, accepted])

        r = r[:len(self._data)]
        phi = Uniform(0., 2*np.pi).random(len(r))

        self._data["ROI_DIST"] = r
        self._data["RA"] = r * np.cos(phi) + self.RAJ
        self._data["DEC"] = r * np.sin(phi) + self.DECJ

    def _random_time(self):

        t0 = mjd_to_met(self.tmin)
        window = mjd_to_met(self.tmax) - t0

        if self.tn_model is not None:
            noise = self.tn_model.sample(self.tn_pars, window,
                fcut_lo=self.tnCutLow, fcut_hi=self.tnCutHigh, seed=int(self.seed)).real
        else:
            noise = np.zeros(int(window))
        self._noise = noise

        sample = []
        pmax = self.profile_model.maximize(self.profile_pars)[1]
        m = min(10000, 10*len(self._data))
        while len(sample) < len(self._data):

            rand_time = self._rng.random(m) * window + t0
            rand_phase = self.timing_model(rand_time - mjd_to_met(self.timing_model.PEPOCH),
                                           self.timing_pars)[0]
            if self.tn_model is not None:
                rand_j = (rand_time - t0).astype(int)
                rand_phase = rand_phase + noise[rand_j]
            rand_phase = fold_phases(rand_phase)

            r = self._rng.random(m) * pmax
            mask = r < self.profile_model(rand_phase, self.profile_pars)
            rand_time = rand_time[mask]
            sample = np.concatenate([sample, rand_time])

        sample = sample[:len(self._data)]
        self._data["TIME"] = sample[:len(self._data)]

    def _add_bkg(self, size):

        t0 = mjd_to_met(self.tmin)
        window = mjd_to_met(self.tmax) - t0
        center = np.array([self.RAJ, self.DECJ])

        rand_time = self._rng.random(size) * window + t0
        rand_energy = sample_energy(self.bkgModel, self.bkgParameters, size,
                                    bin_size=0.1)

        rand_position = []
        while len(rand_position) < size:

            point = self._rng.random(2) * 2 * self.roiRadius + \
                    center - self.roiRadius
            roi_dist = angular_distance(
                np.array([1., point[0], point[1]]),
                np.array([1., center[0], center[1]])
            )
            if roi_dist < self.roiRadius:
                rand_position.append(np.array([point[0], point[1], roi_dist]))
        rand_position = np.array(rand_position).T

        bkg_data = pd.DataFrame(
            {"ENERGY": rand_energy,
             "RA": rand_position[0],
             "DEC": rand_position[1],
             "ROI_DIST": rand_position[2],
             "TIME": rand_time
             }
        )
        self._data = pd.concat([self._data, bkg_data])

    def _init_figure(self):

        padbottom = 0.12
        padleft = 0.2
        padright = 0.95
        padtop = 0.99

        fig = plt.figure(figsize=(13.5, 8.5))
        gs = fig.add_gridspec(6, 9)

        txt = ""
        row_len = 0
        for key, value in self.__dict__.items():
            if key[0] != "_":
                new_item = key + ": " + str(value) + "    "
                if row_len + len(new_item) > 180:
                    txt += "\n"
                    row_len = 0
                txt += new_item
                row_len += len(new_item)

        fig.suptitle(txt, x=padleft / 3, y=padtop, ha="left", size=10)

        # Subfigure for spectrum
        subfig = fig.add_subfigure(gs[1:3, :3])
        subgs = subfig.add_gridspec(1, 1)
        ratio = 3 / 2
        subfig.subplots_adjust(bottom=1.3 * padbottom * ratio, right=padright,
                               left=padleft, hspace=0,
                               top=1 - (1 - padtop) * ratio)

        spectrum = subfig.add_subplot(subgs[0])
        spectrum.set_xlabel("Energy [MeV]")
        spectrum.set_ylabel(r"$E^2 \times dN/dE$ [MeV]")
        plt.xscale("log")
        plt.yscale("log")
        plt.xlim(self.emin, self.emax)

        # Subfigure for count map and histograms
        subfig = fig.add_subfigure(gs[3:6, :3])
        subgs = subfig.add_gridspec(3, 3)
        ratio = 1.
        ratio = 1.
        subfig.subplots_adjust(bottom=padbottom * ratio, left=padleft,
                               right=padright,
                               wspace=0., hspace=0.,
                               top=1 - (1 - padtop) * ratio)

        map = subfig.add_subplot(subgs[1:, :2])
        map.set_xlabel(r"RA [$^\circ$]")
        map.set_ylabel(r"Dec [$^\circ$]")
        map.grid()

        ra = subfig.add_subplot(subgs[0, :2], sharex=map)
        ra.tick_params(which="both", bottom=False, left=False,
                       labelbottom=False, labelleft=False)

        dec = subfig.add_subplot(subgs[1:, 2], sharey=map)
        dec.tick_params(which="both", bottom=False, left=False,
                        labelbottom=False, labelleft=False)

        # Subfigure for timing models
        subfig = fig.add_subfigure(gs[1:4, 3:6])
        subgs = subfig.add_gridspec(2, 1)
        ratio = 1.
        subfig.subplots_adjust(bottom=padbottom * ratio,
                               top=1 - (1 - padtop) * ratio, hspace=0.15)

        f0 = subfig.add_subplot(subgs[0])
        f0.tick_params(which="both", bottom=True, labelbottom=False)
        f0.set_ylabel(r"$f\ -\ \dot f$(t$_0$) $\times$ (t - t$_0$)  [Hz]")

        f1 = subfig.add_subplot(subgs[1], sharex=f0)
        f1.set_ylabel(r"$\dot f$ [Hz/s]")
        f1.set_xlabel("MJD - %.2f" % self.tmin)
        f1.set_xlim(0., self.tmax - self.tmin)

        # Subfigure for timing noise
        subfig = fig.add_subfigure(gs[4:6, 3:6])
        subgs = subfig.add_gridspec(1, 1)
        ratio = 3. / 2.
        subfig.subplots_adjust(bottom=padbottom * ratio,
                               top=1 - (1 - padtop) * ratio, hspace=0,)

        tn = subfig.add_subplot(subgs[0])
        tn.set_xlabel("MJD - %.2f" % self.tmin)
        tn.set_ylabel(r"$\Delta \phi_{\rm TN}$")
        tn.set_xlim(0., self.tmax - self.tmin)

        # Subfigure for phase vs. time and pulse profile
        subfig = fig.add_subfigure(gs[1:, 6:])
        subgs = subfig.add_gridspec(3, 2)
        ratio = 3 / 5
        subfig.subplots_adjust(bottom=padbottom * ratio,
                               top=1 - (1 - padtop) * ratio, hspace=0,
                               left=2 * padleft * ratio)

        phase = subfig.add_subplot(subgs[1:, :])
        phase.set_xlabel("pulse phase")
        phase.set_ylabel("MJD - %.2f" % self.tmin)
        phase.set_xlim(0, 1)

        profile = subfig.add_subplot(subgs[0, :], sharex=phase)
        profile.tick_params(which="both", bottom=False, labelbottom=False, left=False, labelleft=False)

        self._figure = fig

    def _plot_spectrum(self):

        n_photons = len(self._data)
        signal = int(n_photons * self.signalToBkg / (self.signalToBkg + 1))
        bkg = n_photons - signal

        n_bins = int(np.log10(self.emax / self.emin) * 10)
        edges = np.logspace(np.log10(self.emin), np.log10(self.emax),
                            n_bins + 1)
        #counts, edges, _ = self._figure.axes[0].hist(self._data["ENERGY"],
        #                                             bins=edges, density=True,
        #                                             histtype="step",
        #                                             color=self.my_datacolor)
        counts, edges = np.histogram(self._data["ENERGY"], bins=edges,  weights=self._data["ENERGY"]**2)
        x = 0.5 * (edges[:-1] + edges[1:])
        counts = counts / (edges[1:] - edges[:-1]) / (bkg + signal)
        self._figure.axes[0].plot(np.repeat(edges, 2)[1:-1], np.repeat(counts, 2), color=self.my_datacolor)

        x = np.logspace(np.log10(self.emin), np.log10(self.emax), 100)
        y = self.bkgModel(x, self.bkgParameters) * bkg / (signal + bkg)
        self._figure.axes[0].plot(x, x**2 * y, color=self.my_modelcolor, ls="--")

        y += self.spectral_model(x, self.spectral_pars) * signal / (
                    signal + bkg)
        self._figure.axes[0].plot(x, x**2 * y, color=self.my_modelcolor)

    def _plot_map(self):

        self._figure.axes[1].hist2d(self._data["RA"], self._data["DEC"],
                                    bins=100, cmap=self.my_cmap,
                                    range=[[self.RAJ - self.roiRadius,
                                            self.RAJ + self.roiRadius],
                                           [self.DECJ - self.roiRadius,
                                            self.DECJ + self.roiRadius]])

        self._figure.axes[2].hist(self._data["RA"], bins=100,
                                  range=[self.RAJ - self.roiRadius,
                                         self.RAJ + self.roiRadius],
                                  histtype="step", color=self.my_datacolor)

        self._figure.axes[3].hist(self._data["DEC"], bins=100,
                                  range=[self.DECJ - self.roiRadius,
                                         self.DECJ + self.roiRadius],
                                  histtype="step", color=self.my_datacolor,
                                  orientation="horizontal")

    def _plot_timing(self):

        x = np.linspace(self.tmin, self.tmax, 1000)
        y, z = self.timing_model(DAY_TO_SECS * (x - self.timing_model.PEPOCH), self.timing_pars)[1:]

        self._figure.axes[4].plot(x - self.tmin, y - z[0] * mjd_to_met(x - self.tmin), color=self.my_datacolor)
        self._figure.axes[5].plot(x - self.tmin, z, color=self.my_datacolor)

        t = np.arange(len(self._noise)) / DAY_TO_SECS
        r = self._noise
        self._figure.axes[6].plot(t, r, color=self.my_datacolor)

    def _plot_phase(self):

        n_phi_bins = 50
        n_photons = len(self._data)
        signal = int(n_photons * self.signalToBkg / (self.signalToBkg + 1))
        bkg = n_photons - signal

        phases = self.timing_model(
            self._data["TIME"].values - mjd_to_met(self.timing_model.PEPOCH),
            self.timing_pars)[0]
        phases = fold_phases(phases)

        self._figure.axes[7].hist2d(phases, met_to_mjd(self._data["TIME"]) - self.tmin,
                                    cmap=self.my_cmap,
                                    bins=(n_phi_bins, int(np.sqrt(n_photons))),
                                    range=[[0, 1], [0., self.tmax - self.tmin]])

        self._figure.axes[8].hist(phases,
                                  bins=np.linspace(0, 1, n_phi_bins + 1),
                                  color=self.my_datacolor, histtype="step",
                                  density=True
                                  )

        x = np.linspace(0, 1, 100)
        y = np.ones(100) / n_photons * bkg
        self._figure.axes[8].plot(x, y, color=self.my_modelcolor, ls="--")

        y += self.profile_model(x, self.profile_pars) * signal / n_photons / self.profile_model.integrate(self.profile_pars)
        self._figure.axes[8].plot(x, y, color=self.my_modelcolor)
