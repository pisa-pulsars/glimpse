import matplotlib.pyplot as plt
import numpy as np
from astropy.time import Time
from scipy.optimize import curve_fit
from scipy.special import factorial
from skimage.filters import gaussian as gaussian_filter

# LAT time constants
MJDRef = 51910.000708148145
LAT_StartMissionMET = 239557417.
LAT_StartMissionMJD = 54682.65599

# Conversion factors
DAY_TO_SECS = 86400.
DEG_TO_RAD = np.pi / 180.
F_1YEAR = 1 / 365. / DAY_TO_SECS

# Statistics
confidence_levels = [0.682689, 0.954500, 0.997300]

# Parameters c0 and c1 are in units of radians
LAT_PSF_ScalingParams = {
    "FRONT": {
        "c0": 6.38e-2,
        "c1": 1.26e-3,
        "beta": 0.8
    },
    "BACK": {
        "c0": 1.23e-1,
        "c1": 2.22e-3,
        "beta": 0.8
    },
    "PSF0": {
        "c0": 1.53e-1,
        "c1": 5.7e-3,
        "beta": 0.8
    },
    "PSF1": {
        "c0": 9.64e-2,
        "c1": 1.78e-3,
        "beta": 0.8
    },
    "PSF2": {
        "c0": 7.02e-2,
        "c1": 1.07e-3,
        "beta": 0.8
    },
    "PSF3": {
        "c0": 4.97e-2,
        "c1": 6.13e-4,
        "beta": 0.8
    }
}


# Conversion functions
def met_to_mjd(t_met):
    """Convert MET seconds to MJD"""
    return MJDRef + t_met / DAY_TO_SECS


def mjd_to_met(t_mjd):
    """Convert MJD to MET seconds"""
    return DAY_TO_SECS * (t_mjd - MJDRef)


def dms_to_decimal(angle):

    dms = np.array(angle.split(":"), dtype=float)
    dec = np.sum(dms / np.array([1., 60., 3600.]))

    return dec


def hours_to_degrees(angle):

    degs = angle * 360. / 24.

    return degs


def cartesian_to_celestial(point):
    r = np.sum(point ** 2) ** 0.5
    dec = np.arcsin(point[2] / r)
    ra = np.arctan(point[1] / point[0]) if point[0] != 0 else 0.
    ra = (ra + np.pi if point[0] < 0 else ra)
    ra = (ra + 2 * np.pi if ra < 0 else ra)
    return np.array([r, ra / DEG_TO_RAD, dec / DEG_TO_RAD])


def celestial_to_cartesian(point):
    ra = point[1] * DEG_TO_RAD
    dec = point[2] * DEG_TO_RAD
    x = point[0] * np.cos(ra) * np.cos(dec)
    y = point[0] * np.sin(ra) * np.cos(dec)
    z = point[0] * np.sin(dec)
    return np.array([x, y, z])


def angular_distance(p1, p2):

    q1 = celestial_to_cartesian(p1)
    q2 = celestial_to_cartesian(p2)

    dq = (q1 - q2)
    dist = np.sum(dq**2)**0.5
    angle = 2 * np.arcsin(0.5 * dist)
    return angle / DEG_TO_RAD


def sigma_psf(E):
    """PSF width in degrees as in Ackermann et al. 2013"""
    s1 = 5.11 * (E/100.)**(-0.76)
    s2 = 0.082
    return (s1**2 + s2**2)**0.5


def load_parfile(filename):

    with open(filename) as parfile:
        pars_dict = {}
        for line in parfile:
            fields = line.strip().split()
            pars_dict[fields[0]] = fields[1]

    return pars_dict


def psf_scaling(energy, evtype):
    pars = LAT_PSF_ScalingParams[evtype]
    scale = np.sqrt(
        (pars["c0"] * (energy / 100.) ** (-pars["beta"])) ** 2
        + pars["c1"] ** 2
    )
    return scale


def taylor(t, x):
    """
    Compute the truncated Taylor series up to order q of a function y(t)
    and its derivatives at n times t.

    Args:
        t (numpy.ndarray): array of size n containing the times at which
                           the series will be computed
        x (np.ndarray): array of size q+1 containing the derivative of y(t)
                        computed in t=0

    Returns:
        numpy.ndarray: 2-d array where each row of index j contains the jth
                       derivative of y(t) computed at the n times t
    """

    t = np.array([t]) if isinstance(t, float) else t
    n, q = len(t), len(x)
    new_shape = q if n == 1 else (n, q)
    t = np.repeat(t, q).reshape(new_shape)
    t = (t ** np.flip(np.arange(q))).T

    matrix = np.zeros((q, q))
    for j in range(q):
        for i in range(q-j):
            matrix[i][-1-j] = x[j+i] / factorial(j)

    return np.matmul(matrix, t)


def fold_phases(phi):

    phi = np.array([phi])
    phi = np.modf(phi)[0][0]
    phi[np.where(phi < 0.)] = phi[np.where(phi < 0.)] + 1.

    return phi


def get_credibility_levels(counts):

    output = np.zeros(len(confidence_levels))
    counts = np.sort(np.ravel(counts))
    n_counts = counts.sum()

    coverage = 1.
    ci = 0
    while coverage >= 0.5:
        level = counts[ci]
        coverage = counts[ci:].sum() / n_counts
        for si in range(len(confidence_levels)):
            if coverage >= confidence_levels[si]:
                output[si] = level
        ci += 1

    return output


def fit_slope(x, y, dy=None):

    def fit_function(data, k, y0):
        return y0 + k * data

    pinit = [(y[-1] - y[0]) / (x[-1] - x[0]), y[0]]

    try:
        popt, covm = curve_fit(fit_function, x, y, sigma=dy, p0=pinit)
    except TypeError:
        popt = [0, 0]

    return popt


def fancy_print(input, indent=0):

    template = " " * indent + "{key:%ds} = {value:16s}\n" % (16 - indent)
    output = ""

    if isinstance(input, dict):
        output += "\n"
        for key in input:
            line = template.format(
                key=fancy_print(key),
                value=fancy_print(input[key], indent=4)
            )
            output += line

    elif isinstance(input, str):
        if input[-4:] in [".csv", ".par"]:
            output += input
        else:
            output += input.replace("_", " ").title()

    else:
        output += str(input)

    return output


def get_quantiles(data, cl=0.682689):

    return np.quantile(data, [0.5 * (1 - cl), 0.5, 0.5 * (1 + cl)])


class Estimator:

    def __init__(self, x0: float, dx: tuple):

        self._x0 = x0
        self._dx = dx
        self._dgt = self._significant_digits

    @property
    def _significant_digits(self):

        dgt_inf = np.floor(np.log10(self._dx[0])).astype(int)
        dgt_sup = np.floor(np.log10(self._dx[1])).astype(int)

        return -np.min([dgt_inf, dgt_sup]).astype(int)

    @property
    def _truncated(self):

        trunc_x0 = np.round(self._x0, self._dgt)
        trunc_dx = (
            np.round(self._dx[0], self._dgt),
            np.round(self._dx[1], self._dgt)
        )

        return trunc_x0, trunc_dx

    def __str__(self):

        return "%f, (%f, %f)" % (self._x0, self._dx[0], self._dx[1])

    def to_latex(self):

        x0, dx = self._truncated
        out_str = r"%s $^{+%s} _{-%s}$" % (
            str(x0), str(dx[0]), str(dx[1])
        )

        return out_str

    @classmethod
    def from_sample(cls, data: np.ndarray):

        sigma = confidence_levels[0]
        q = np.quantile(data,
            [0.5 * (1 - sigma), 0.5, 0.5 * (1 + sigma)])

        return Estimator(q[1], (q[2] - q[1], q[1] - q[0]))
