from os import environ
from os.path import abspath, dirname, join

# Set up paths
BASE_DIR = abspath(join(dirname(__file__), "..", ".."))

LATDATA_DIR = environ.get("LATDATA_DIR", "./")
GLIMPSE_PERIODICITY_DIR = environ.get("GLIMPSE_PERIODICITY_DIR", "./")
GLIMPSE_BAYESIAN_DIR = join("GLIMPSE_BAYESIAN_DIR", "./")
CATALOG_DIR = environ.get("CATALOG_DIR", "./")

PULSAR_INDEX = join(GLIMPSE_PERIODICITY_DIR, "pulsar_index.json")
BAYESIAN_CONFIG = join(GLIMPSE_BAYESIAN_DIR, "glitch_config.yaml")
EVFILE = join(LATDATA_DIR, "FT1_list.txt")
SCFILE = join(LATDATA_DIR, "mission", "spacecraft", "lat_spacecraft_merged.fits")

# Dash conf
HOSTNAME = environ.get("HOSTNAME", "0.0.0.0")
DEBUG = False

# Catalog download path
CATALOGS = {
    "2PC":
        {
            "table_filename": "2PC_catalog_v04.fits",
            "download_path": "https://fermi.gsfc.nasa.gov/ssc/data/access/lat/2nd_PSR_catalog/2PC_auxiliary_files_v04.tgz"
        }
}

# Default datetime format
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
