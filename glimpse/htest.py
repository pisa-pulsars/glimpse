import os

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.gridspec import GridSpec
from scipy.special import erf

from glimpse.utils import DAY_TO_SECS, met_to_mjd, taylor, fold_phases


# TODO: Consider transforming this function to a class HmwScanner
def h_scan(photon_times, pars_start, f0_trials=200, f1_trials=50,
           m=20, c=4, plot_results=False, outdir="./", psr_name="pulsar",
           weights=None, compute_errors=True):
    """Performs an H analysis test with a set of barycentered photon arrival times."""

    test_array = np.empty(shape=(f0_trials, f1_trials, len(photon_times)), dtype=float)
    mem_usage = test_array.nbytes / (1024 ** 2)
    if mem_usage > 2048:
        print("WARNING: This operation will require %d MiB memory." % mem_usage)
    del test_array

    # time window
    tmin_met = np.min(photon_times)
    tmax_met = np.max(photon_times)
    dt_met = tmax_met - tmin_met
    t0_met = 0.5 * (tmin_met + tmax_met)
    tmin_mjd, tmax_mjd, t0_mjd = [met_to_mjd(epoch) for epoch in [tmin_met, tmax_met, t0_met]]

    # accuracies
    pars_accuracy = []
    for fi in range(2):
        # TODO: refine this calculation
        pars_accuracy.append(0.5 / dt_met ** (fi + 1))

    # arrays of frequencies and derivatives
    n_trials = [int(f0_trials), int(f1_trials)]
    pars_values = [np.empty(1), np.empty(1)]
    for fi in range(2):
        pars_values[fi] = np.linspace(pars_start[fi] - pars_accuracy[fi] * (n_trials[fi] // 2),
                                      pars_start[fi] + pars_accuracy[fi] * (n_trials[fi] // 2),
                                      n_trials[fi] + 1)[:-1]
    f2 = pars_start[2] if len(pars_start) > 2 else 0.

    # array of delta times
    deltat = np.array(photon_times) - t0_met
    n_data = len(deltat)
    m_shape = (n_trials[0], n_trials[1])
    n_tot_trials = n_trials[0] * n_trials[1]

    if weights is None:
        weights = np.ones(n_data)
    wi = np.array(weights).reshape((1, 1, n_data)) / np.max(weights)

    # init results dictionary
    results = {"TMIN_MJD": tmin_mjd, "TMAX_MJD": tmax_mjd,
               "PEPOCH": t0_mjd, "H": -1, }
    for fi in range(3):
        results["F%d_UNC" % fi] = 0.

    # init matrix of relative phases, with indices (f0, f1, t)
    # we add terms up to order 3
    # the first term is f0*t
    term_1 = pars_values[0].reshape(m_shape[0], 1, 1) * deltat
    phi = np.ones(n_tot_trials * n_data).reshape(
        m_shape[0], m_shape[1], n_data) * term_1.reshape(m_shape[0], 1, n_data)
    del term_1

    # add term 1/2 *f1*t^2
    term_2 = 0.5 * pars_values[1].reshape(1, m_shape[1], 1) * deltat ** 2
    phi = phi + np.ones(n_tot_trials * n_data).reshape(
        m_shape[0], m_shape[1], n_data) * term_2.reshape(1, m_shape[1], n_data)
    del term_2

    # add term 1/6 *f2*t^3
    term_3 = (f2 * deltat ** 3) / 6.
    phi = phi + np.ones(n_tot_trials * n_data).reshape(
        m_shape[0], m_shape[1], n_data) * term_3.reshape(1, 1, n_data)
    del term_3

    # fractional part of phi, then compute H
    phi = np.modf(phi)[0]
    hmw = Hmw(phi, wi, m=m, c=c)

    # store output in the dictionary
    h_max = np.max(hmw)
    results["H"] = h_max
    results["LOGP"] = H20w_logP_Bruel(h_max, np.sum(wi))
    results["SIGMA"] = Hmw_equivalent_sigma(10**results["LOGP"])

    for fi in range(2):
        index_max = np.argmax(np.max(hmw, axis=1 - fi))
        par_max = pars_values[fi][0] + index_max * pars_accuracy[fi]
        results["F%d_BEST" % fi] = par_max
    results["F2_BEST"] = f2

    # Store info about phases
    ph_best = [
        taylor(x, np.array([0., results["F0_BEST"], results["F1_BEST"]]))[0]
        for x in deltat]
    ph_best = fold_phases(ph_best)
    results["PULSE_PHASE"] = ph_best

    # the following code computes uncertainties via bootstrap resampling
    if compute_errors:
        sample_pars = []
        n_samples = 100
        n_points = n_data
        large_sample = np.random.randint(0, n_data, (n_points - 2) * n_samples)
        large_sample = large_sample.reshape(n_samples, n_points - 2)
        for small_sample in large_sample:
            sample_times = np.append(np.array(photon_times)[small_sample],
                                     [tmin_met, tmax_met])
            sample_weights = np.append(np.array(weights)[small_sample],
                                       [weights[np.argmin(sample_times)],
                                        weights[np.argmax(sample_times)]])
            new_pars = h_scan(sample_times,
                [results["F0_BEST"], results["F1_BEST"], results["F2_BEST"]],
                f0_trials=40, f1_trials=20, m=m, c=c,
                plot_results=False, weights=sample_weights,
                compute_errors=False)
            sample_pars.append([new_pars["F0_BEST"], new_pars["F1_BEST"]])
        sample_pars = np.array(sample_pars).T
        for pi in range(2):
            results["F%d_UNC" % pi] = np.std(sample_pars[pi])
        del large_sample

    # the following code produces an image in the specified output dir
    if plot_results:
        if not os.path.exists(outdir):
            os.mkdir(outdir)

        # init figure
        fig_hmap = plt.figure(2, figsize=(6, 6))
        fig_hmap.clf()
        gs = GridSpec(3, 2, figure=fig_hmap)
        fig_hmap.suptitle("PSR %s \n" % psr_name)

        plttxt = "TSTART MJD %.1f \n" % tmin_mjd + \
                 "TSTOP MJD %.1f \n" % tmax_mjd + \
                 r"F0 = %.9f +/- %.2e Hz" % (results["F0_BEST"], results["F0_UNC"]) + "\n" + \
                 r"F1 = %.2e +/- %.2e Hz s$^{-1}$" % (results["F1_BEST"], results["F1_UNC"]) + "\n" + \
                 r"F2 = %.2e +/- %.2e Hz s$^{-2}$" % (results["F2_BEST"], results["F2_UNC"]) + "\n" + \
                 r"H$_{20}$ = %.2f" % results["H"]

        ax0 = fig_hmap.add_subplot(gs[0, 0])
        ax0.set_axis_off()
        ax0.text(0, 1, plttxt, transform=ax0.transAxes,
                 horizontalalignment='left', verticalalignment='top')

        # H map
        ax1 = fig_hmap.add_subplot(gs[1:3, 0])
        img = ax1.imshow(hmw, extent=[0, 1, 0, 2], interpolation="None", cmap="jet")
        plt.colorbar(img)
        ax1.set_xticks(np.linspace(0, 1, 5))
        ax1.set_yticks(np.linspace(0, 2, 5))
        mags = [np.floor(np.log10(pars_accuracy[0] * f0_trials)),
                np.floor(np.log10(np.abs(pars_values[1][-1])))]
        x_ticks_list = pars_values[1][np.linspace(0, f1_trials - 1, 5, dtype=int)]
        x_ticks_list = np.array(x_ticks_list) / (10 ** mags[1])
        x_label_list = ["%.1f" % item for item in x_ticks_list]
        ax1.set_xticklabels(x_label_list)
        y_ticks_list = pars_values[0][np.linspace(0, f0_trials - 1, 5, dtype=int)]
        y_ticks_list = np.flip((np.array(y_ticks_list) - pars_values[0][0]) / (10 ** mags[0]))
        y_label_list = ["%.2f" % item for item in y_ticks_list]
        ax1.set_yticklabels(y_label_list)
        ax1.invert_yaxis()
        ax1.set_xlabel(r"F1 ($10^{%d}$ Hz s$^{-1})$" % mags[1])
        ax1.set_ylabel(r"F0 ($10^{%d} ~+~ %.10f$ Hz)" % (mags[0], pars_values[0][0]))

        # weighted histogram
        ax2 = fig_hmap.add_subplot(gs[0, 1])
        ph_bins = np.linspace(0., 1., int(np.sum(wi)**0.5)+1)
        counts, edges, _ = ax2.hist(results["PULSE_PHASE"], bins=ph_bins,
                                    color="darkblue", weights=weights, histtype="step",
                                    lw=1)
        ax2.errorbar(0.5 * (edges[1:] + edges[:-1]), counts, np.sqrt(counts),
                     color="darkblue", marker="", capsize=0, ls="", lw=1)
        ax2.set_ylabel("Weighted counts / bin")
        ax2.set_xlim([0., 1.])
        ax2.set_xticks([])

        # phase scatterplot
        ax3 = fig_hmap.add_subplot(gs[1:3, 1])
        ax3.scatter(results["PULSE_PHASE"], (photon_times - tmin_met) / DAY_TO_SECS,
                    marker=".", alpha=weights, color="darkblue")
        ax3.set_xlim([0., 1.])
        ax3.set_ylim([0., np.max((photon_times - tmin_met)) / DAY_TO_SECS])
        ax3.set_xlabel("Pulse phase")
        ax3.set_ylabel("Time since MJD %.2f (days)" % tmin_mjd)

        gs.tight_layout(fig_hmap, rect=[0., 0., 1.0, 1.0])
        gs.update(hspace=0.)

        outfile = os.path.join(outdir, psr_name + "_timing_MJD%d-%d.png") % (tmin_mjd, tmax_mjd)
        plt.savefig(outfile)
        plt.close()

    return results


def Hmw(phi, wi, m=20, c=4):

    Z = 0.
    H = 0.
    for k in range(1, m + 1):
        Z += 2. / np.sum(wi ** 2, axis=-1) * \
             ((wi * np.cos(2 * np.pi * k * phi)).sum(axis=-1) ** 2 +
              (wi * np.sin(2 * np.pi * k * phi)).sum(axis=-1) ** 2)
        H = np.where(Z - c * (k - 1) > H, Z - c * (k - 1), H)

    return H


def H20_logP_DeJager(h):
    """Get the chance probability according to De Jager et al. 2010"""
    return -0.4 * h * np.log10(np.e)


def H20_logP_Kerr(h):
    """Get the chance probability according to Kerr 2011"""
    return -0.398405 * h * np.log10(np.e)


def H20_logP_Bruel(h, N):
    """Get the chance probability according to Bruel 2019"""

    l0 = -0.173025
    l1 = l0 + 0.0525796 * np.exp(-N / 215.170) + 0.086406 * np.exp(-N / 35.5709)

    if 0 <= h < 15:
        logP = l0 * h
    elif 15 <= h < 29:
        logP = 15 * l0 + 0.5 * (l0 + l1) * (h - 15)
    else:
        logP = 22 * l0 + l1 * (h - 22)

    return logP


def H20w_logP_Bruel(h, W):
    """Get the weighted chance probability according to Bruel 2019"""

    return H20_logP_Bruel(h, W + 5)


def Hmw_equivalent_sigma(prob):
    """Return the value of equivalent gaussian sigma"""
    s = 0.0
    p = 10.
    while float(p) > float(prob):
        s = s + 0.01
        p = 1. - erf(float(s) / np.sqrt(2.))

    return s


def Hmw_pulsation_significance(sigma):
    """Get the pulsation probability according to Bruel 2019"""

    pw = -np.log10(1 - erf(sigma / np.sqrt(2.)))

    return pw


def Hmw_test(phi, wi, m=20, c=4):

    wi = wi / np.max(wi)
    hmw = Hmw(phi, wi, m, c)
    logp = H20w_logP_Bruel(hmw, np.sum(wi))
    s = Hmw_equivalent_sigma(10**logp)
    pmw = Hmw_pulsation_significance(s)

    return hmw, pmw
