import os

from astropy.io import fits
import numpy as np

from glimpse.sources import FermiPulsar


class PulsarCatalog:
    """
    This class reads and handles a pulsar catalog (e.g. 2PC, 3PC) passed in FITS format.
    """

    def __init__(self, catalog):
        self.psr_list = []
        if os.path.exists(catalog):
            self.catalog_dir = os.path.dirname(catalog)
            self.catalog_filename = os.path.basename(catalog)
        else:
            raise ValueError("Invalid catalog: please provide catalog name or filename.")
        data_table = fits.getdata(os.path.join(self.catalog_dir, self.catalog_filename))
        for entry in data_table:
            parameters = {}
            for ci in range(len(data_table.columns)):
                key = data_table.columns[ci].name
                parameters[key] = entry[ci]
            pulsar = FermiPulsar(parameters)
            self.add_pulsar(pulsar)

    def add_pulsar(self, pulsar, overwrite=True):
        if isinstance(pulsar, FermiPulsar):
            if pulsar.PSRJ in self.list_pulsars() and not overwrite:
                print(pulsar.PSRJ + " already exists in the catalog. Pulsar not added.")
            else:
                self.psr_list.append(pulsar)
        else:
            raise TypeError("Input is not a FermiPulsar instance.")

    def drop_pulsar(self, psr_name):
        filtered_psr_list = []
        for pulsar in self.psr_list:
            if psr_name != pulsar.PSRJ:
                filtered_psr_list.append(pulsar)
        self.psr_list = filtered_psr_list

    def get_pulsar(self, psr_name):
        for pulsar in self.psr_list:
            if pulsar.PSRJ == psr_name:
                return pulsar
        raise ValueError(psr_name + " not found.")

    def list_parameters(self):
        par_list = []
        for pulsar in self.psr_list:
            for key in pulsar.__dict__:
                if key not in par_list:
                    par_list.append(key)
        return par_list

    def list_pulsars(self):
        name_list = []
        for pulsar in self.psr_list:
            name_list.append(pulsar.PSRJ)
        return name_list

    def select_by_parameter(self, par_name, par_min=None, par_max=None):
        if par_name in self.list_parameters():
            filtered_psr_list = []
            if par_min is not None:
                for pulsar in self.psr_list:
                    if par_name not in pulsar.__dict__:
                        filtered_psr_list.append(pulsar)
                    elif pulsar.__dict__[par_name] >= par_min:
                        filtered_psr_list.append(pulsar)
            self.psr_list = filtered_psr_list
            filtered_psr_list = []
            if par_max is not None:
                for pulsar in self.psr_list:
                    if par_name not in pulsar.__dict__:
                        filtered_psr_list.append(pulsar)
                    elif pulsar.__dict__[par_name] < par_max:
                        filtered_psr_list.append(pulsar)
            self.psr_list = filtered_psr_list
        else:
            raise AttributeError("Invalid parameter.")

    def select_by_type(self, psr_type):
        filtered_psr_list = []
        for pulsar in self.psr_list:
            if pulsar.Type == psr_type:
                filtered_psr_list.append(pulsar)
        self.psr_list = filtered_psr_list

    def select_by_name(self, name_list):
        filtered_psr_list = []
        for pulsar in self.psr_list:
            if pulsar.PSRJ in name_list:
                filtered_psr_list.append(pulsar)
        self.psr_list = filtered_psr_list

    def select_region(self, ra_center, dec_center, radius):
        if 0 < ra_center < 360 and abs(dec_center) < 90:
            filtered_psr_list = []
            for pulsar in self.psr_list:
                ra_psr = pulsar.RAJ2000
                dec_psr = pulsar.DEJ2000
                if np.abs(ra_psr - ra_center) < 180:
                    distance = np.sqrt((ra_psr - ra_center)**2 + (dec_psr - dec_center)**2)
                else:
                    distance = np.sqrt((360 - np.abs(ra_psr - ra_center)) ** 2 + (dec_psr - dec_center) ** 2)
                if distance < radius:
                    filtered_psr_list.append(pulsar)
            self.psr_list = filtered_psr_list
        else:
            raise ValueError("Parameters out of range.")
