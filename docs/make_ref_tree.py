#!/usr/bin/env python

import os


class DocsMaker:

    def __init__(self, outdir="./"):

        self.tree = []

    def get_tree(self, dirname):

        children = os.listdir(dirname)
        for child in children:
            if child not in ["__init__.py", "__pycache__"]:
                subpath = os.path.join(dirname, child)
                if os.path.isdir(subpath):
                    self.get_tree(subpath)
                else:
                    self.tree.append(os.path.join(dirname, child))

    def make(self, outdir):

        if not os.path.exists(outdir):
            os.mkdir(outdir)

        for item in self.tree:
            filename = os.path.splitext(item)[0].replace("/", ".")
            print(filename)
            content = "::: %s" % filename
            text_file = open(os.path.join(outdir, filename + ".md"), "w")
            text_file.write(content)
            text_file.close()


if __name__ == "__main__":

    dm = DocsMaker()
    dm.get_tree("glimpse")
    dm.make("docs/modules")


