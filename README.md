# Glimpse

Glimpse is a Python 3 package providing tools for the analysis of _Fermi_-LAT pulsars. The main modules contain classes that allow to simulate photon data sets, search for pulsations with the H test statistics and characterize glitches via Bayesian parameter estimation.

For the documentation, click [here](https://glimpse-pisa-pulsars-9aba0b90aa50c69e02c66d1f4ad98347676fd23c4e.gitlab.io).

For examples, click [here](https://gitlab.com/pisa-pulsars/glimpse/-/tree/main/examples?ref_type=heads).
